<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('last_name_paternal')->nullable();
            $table->string('last_name_maternal')->nullable();
            $table->date('date_of_birth')->nullable();
            $table->string('phone')->nullable();
            $table->string('cell_phone')->nullable();
            $table->string('ine_number')->nullable();
            $table->integer('housewife')->nullable();
            $table->integer('out_of_home')->nullable();
            $table->string('address')->nullable();
            $table->string('facebook')->nullable();
            $table->string('commentary')->nullable();
            $table->string('email')->unique();
            $table->integer('status')->default('0');
            $table->time('time_active')->nullable();
            $table->dateTime('activation_date')->nullable();
            $table->string('ip')->nullable();
            $table->string('password');
            $table->string('photo')->nullable();
            $table->date('frecuencia_participacion')->nullable();
            $table->string('photo_personal')->nullable();
            $table->string('photo_limpieza')->nullable();
            $table->string('photo_lavado')->nullable();
            $table->integer('estado_id')->nullable();
            $table->integer('municipio_id')->nullable();
            $table->integer('localidad_id')->nullable();
            $table->longText('calle')->nullable();
            $table->integer( 'codigo_postal')->nullable();
            $table->integer( 'complete_information')->nullable();
            $table->integer( 'seen_by_boss')->nullable();
            $table->integer( 'user_id')->nullable();
            $table->longText( 'boss_message')->nullable();

            $table->rememberToken();
            $table->timestamps();
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
