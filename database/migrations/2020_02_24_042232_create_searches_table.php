<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSearchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('searchs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('range')->nullable();
            $table->string('housewife')->nullable();
            $table->string('out_of_home')->nullable();
            $table->string('estado_id')->nullable();
            $table->string('municipio_id')->nullable();
            $table->string('localidad_id')->nullable();
            $table->string('cp')->nullable();
            $table->string('user_id')->nullable();
            $table->string('last_participation')->nullable();
            $table->string('cat32_new')->nullable();
            $table->string('cat33_new')->nullable();
            $table->string('cat34_new')->nullable();
            $table->string('cat32_old')->nullable();
            $table->string('cat33_old')->nullable();
            $table->string('cat34_old')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('searches');
    }
}
