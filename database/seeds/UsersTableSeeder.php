<?php
use App\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;



class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        // Limpiar tablas
        Permission::truncate();
        Role::truncate();
        User::truncate();

        $adminRole = Role::create(['name' => 'Administrador']);
        $jefeRole = Role::create(['name' => 'Jefe_capturista']);
        $capturistaRole = Role::create(['name' => 'Capturista']);
        $candidatoRole = Role::create(['name' => 'Candidato']);

        $ver_admin = Permission::create(['name' => 'Ver Administradores']);
        $editar_admin = Permission::create(['name' => 'Editar Administradores']);
        $eliminar_admin = Permission::create(['name' => 'Eliminar Administradores']);

        $editar_reles = Permission::create(['name' => 'Editar roles']);

        $adminRole->givePermissionTo('Editar roles');
        $adminRole->givePermissionTo('Ver Administradores');
        $adminRole->givePermissionTo('Editar Administradores');
        $adminRole->givePermissionTo('Eliminar Administradores');

        $ver_jefe_capturista = Permission::create(['name' => 'Ver Jefe Capturista']);
        $editar_jefe_capturista = Permission::create(['name' => 'Editar Jefe Capturista']);
        $eliminar_jefe_capturista = Permission::create(['name' => 'Eliminar Jefe Capturista']);

        $adminRole->givePermissionTo('Ver Jefe Capturista');
        $adminRole->givePermissionTo('Editar Jefe Capturista');
        $adminRole->givePermissionTo('Eliminar Jefe Capturista');


        $ver_capturista = Permission::create(['name' => 'Ver Capturista']);
        $editar_capturista = Permission::create(['name' => 'Editar Capturista']);
        $eliminar_capturista = Permission::create(['name' => 'Eliminar Capturista']);

        $adminRole->givePermissionTo('Ver Capturista');
        $adminRole->givePermissionTo('Editar Capturista');
        $adminRole->givePermissionTo('Eliminar Capturista');

        $jefeRole->givePermissionTo('Ver Capturista');
        $jefeRole->givePermissionTo('Editar Capturista');
        $jefeRole->givePermissionTo('Eliminar Capturista');


        $ver_candidato = Permission::create(['name' => 'Ver Cantidato']);
        $editar_candidato = Permission::create(['name' => 'Editar Cantidato']);
        $eliminar_candidato = Permission::create(['name' => 'Eliminar Cantidato']);

        $adminRole->givePermissionTo('Ver Cantidato');
        $adminRole->givePermissionTo('Editar Cantidato');
        $adminRole->givePermissionTo('Eliminar Cantidato');

        $jefeRole->givePermissionTo('Ver Cantidato');
        $jefeRole->givePermissionTo('Editar Cantidato');
        $jefeRole->givePermissionTo('Eliminar Cantidato');

        $capturistaRole->givePermissionTo('Ver Cantidato');
        $capturistaRole->givePermissionTo('Editar Cantidato');
        $capturistaRole->givePermissionTo('Eliminar Cantidato');



        // Crear Usuario
        $admin = new User;
        $admin->name = 'Victor Hugo';
        $admin->email = 'vic.hug.urib@gmail.com';
        $admin->password = '12345678';
        $admin->status = '1';
        $admin->save();
        // Asignar Rol
        $admin->assignRole($adminRole);

        // Crear Usuario
        $capturista = new User;
        $capturista->name = 'Victor Uribe';
        $capturista->email = 'fu-sio@hotmail.com';
        $capturista->password = '12345678';
        $capturista->save();
        // Asignar Rol
        $capturista->assignRole($jefeRole);

        // Crear Usuario
        $capturista = new User;
        $capturista->name = 'Victor Hernandez';
        $capturista->email = 'vic.hug.urib1@gmail.com';
        $capturista->password = '12345678';
        $capturista->time_active = '00:05:00';
        $capturista->activation_date = date("Y-m-d H:i:s");
        $capturista->ip = '201.141.173.169';
        $capturista->save();
        // Asignar Rol
        $capturista->assignRole($capturistaRole);

        // Crear Usuario
        $capturista = new User;
        $capturista->name = 'Hugo Uribe';
        $capturista->email = 'fu-sio1@hotmail.com';
        $capturista->password = '12345678';
        $capturista->complete_information = 2;
        $capturista->user_id = 1;
        $capturista->save();
        // Asignar Rol
        $capturista->assignRole($candidatoRole);

    }
}
