<?php

use App\Brand;
use Illuminate\Database\Seeder;

class BrandTableSeeder extends Seeder
{
     /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
     {
          Brand::truncate();
          // Category::create(['name' => 'DETERGENTE POLVO']);
          //DETERGENTE POLVO
          Brand::create(['name' => '123', 'category_id' => 1 ]);
          Brand::create(['name' => 'ACE', 'category_id' => 1 ]);
          Brand::create(['name' => 'ARCOIRIS', 'category_id' => 1 ]);
          Brand::create(['name' => 'ARIEL', 'category_id' => 1 ]);
          Brand::create(['name' => 'ARM & HAMMER', 'category_id' => 1 ]);
          Brand::create(['name' => 'AURRERA MULTIUSOS', 'category_id' => 1 ]);
          Brand::create(['name' => 'AUXY', 'category_id' => 1 ]);
          Brand::create(['name' => 'BLANCA NIEVES', 'category_id' => 1 ]);
          Brand::create(['name' => 'BLANCATEL MULTIUSOS', 'category_id' => 1 ]);
          Brand::create(['name' => 'BOLD 3', 'category_id' => 1 ]);
          Brand::create(['name' => 'CARISMA', 'category_id' => 1 ]);
          Brand::create(['name' => 'CLORALEX BLANCOS', 'category_id' => 1 ]);
          Brand::create(['name' => 'FOCA', 'category_id' => 1 ]);
          Brand::create(['name' => 'GREAT VALUE', 'category_id' => 1 ]);
          Brand::create(['name' => 'HOUSE CARE', 'category_id' => 1 ]);
          Brand::create(['name' => 'MAESTRO LIMPIO', 'category_id' => 1 ]);
          Brand::create(['name' => 'PERSIL', 'category_id' => 1 ]);
          Brand::create(['name' => 'PINOL', 'category_id' => 1 ]);
          Brand::create(['name' => 'ROMA', 'category_id' => 1 ]);
          Brand::create(['name' => 'ULTRA CLEAN', 'category_id' => 1 ]);
          Brand::create(['name' => 'VIVA', 'category_id' => 1 ]);
          //DETERGENTE LIQUIDO
          Brand::create(['name' => '123', 'category_id' => 2 ]);
          Brand::create(['name' => 'ACE', 'category_id' => 2 ]);
          Brand::create(['name' => 'ARCOIRIS', 'category_id' => 2 ]);
          Brand::create(['name' => 'ARIEL', 'category_id' => 2 ]);
          Brand::create(['name' => 'ARM & HAMMER', 'category_id' => 2 ]);
          Brand::create(['name' => 'AURRERA ROPA OSCURA', 'category_id' => 2 ]);
          Brand::create(['name' => 'AUXY', 'category_id' => 2 ]);
          Brand::create(['name' => 'BLANCA NIEVES', 'category_id' => 2 ]);
          Brand::create(['name' => 'BOLD 3', 'category_id' => 2 ]);
          Brand::create(['name' => 'CARISMA', 'category_id' => 2 ]);
          Brand::create(['name' => 'CLOROX COLORES', 'category_id' => 2 ]);
          Brand::create(['name' => 'FOCA', 'category_id' => 2 ]);
          Brand::create(['name' => 'GREAT VALUE', 'category_id' => 2 ]);
          Brand::create(['name' => 'MAS COLOR', 'category_id' => 2 ]);
          Brand::create(['name' => 'MEMBERS MARK', 'category_id' => 2 ]);
          Brand::create(['name' => 'PERSIL', 'category_id' => 2 ]);
          Brand::create(['name' => 'PINOL', 'category_id' => 2 ]);
          Brand::create(['name' => 'PRECISIMO', 'category_id' => 2 ]);
          Brand::create(['name' => 'QUALITY DAY', 'category_id' => 2 ]);
          Brand::create(['name' => 'ROMA', 'category_id' => 2 ]);
          Brand::create(['name' => 'TIDE', 'category_id' => 2 ]);
          Brand::create(['name' => 'VEL ROSITA', 'category_id' => 2 ]);
          Brand::create(['name' => 'VIVA', 'category_id' => 2 ]);
          Brand::create(['name' => 'SAVIA', 'category_id' => 2 ]);
          //SUAVIZANTES PARA ROPA
          Brand::create(['name' => 'AURRERA', 'category_id' => 3 ]);
          Brand::create(['name' => 'CARISMA', 'category_id' => 3 ]);
          Brand::create(['name' => 'DOWNY', 'category_id' => 3 ]);
          Brand::create(['name' => 'ENSUEÑO', 'category_id' => 3 ]);
          Brand::create(['name' => 'GOLDEN HILLS', 'category_id' => 3 ]);
          Brand::create(['name' => 'KLY', 'category_id' => 3 ]);
          Brand::create(['name' => 'MEMBERS MARK', 'category_id' => 3 ]);
          Brand::create(['name' => 'NUBECITA', 'category_id' => 3 ]);
          Brand::create(['name' => 'PRECISSIMO', 'category_id' => 3 ]);
          Brand::create(['name' => 'REAL CLEAN', 'category_id' => 3 ]);
          Brand::create(['name' => 'SAVIA', 'category_id' => 3 ]);
          Brand::create(['name' => 'SUAVITEL', 'category_id' => 3 ]);
          Brand::create(['name' => 'KIRK LAND', 'category_id' => 3 ]);
          //JABON DE BARRA
          Brand::create(['name' => '123', 'category_id' => 4 ]);
          Brand::create(['name' => 'AURRERA', 'category_id' => 4 ]);
          Brand::create(['name' => 'BLANCATEL', 'category_id' => 4 ]);
          Brand::create(['name' => 'DAROMA', 'category_id' => 4 ]);
          Brand::create(['name' => 'FLORAL', 'category_id' => 4 ]);
          Brand::create(['name' => 'LIRIO', 'category_id' => 4 ]);
          Brand::create(['name' => 'MORO', 'category_id' => 4 ]);
          Brand::create(['name' => 'PRINCESA', 'category_id' => 4 ]);
          Brand::create(['name' => 'ROMA', 'category_id' => 4 ]);
          Brand::create(['name' => 'TEPEYAC', 'category_id' => 4 ]);
          Brand::create(['name' => 'VEL ROSITA', 'category_id' => 4 ]);
          Brand::create(['name' => 'VIVA', 'category_id' => 4 ]);
          Brand::create(['name' => 'WIXO', 'category_id' => 4 ]);
          Brand::create(['name' => 'ZOTE', 'category_id' => 4 ]);
          //CLORO/BLANQUEADOR DE ROPA
          Brand::create(['name' => 'AURRERA', 'category_id' => 5 ]);
          Brand::create(['name' => 'BLANCATEL', 'category_id' => 5 ]);
          Brand::create(['name' => 'CLORALEX', 'category_id' => 5 ]);
          Brand::create(['name' => 'CLOROX', 'category_id' => 5 ]);
          Brand::create(['name' => 'DON CLORITO', 'category_id' => 5 ]);
          Brand::create(['name' => 'LOS PATITOS', 'category_id' => 5 ]);
          Brand::create(['name' => 'QUALITY DAY', 'category_id' => 5 ]);
          Brand::create(['name' => 'STAN HOME', 'category_id' => 5 ]);
          Brand::create(['name' => 'STROMO', 'category_id' => 5 ]);
          Brand::create(['name' => 'WHITEX', 'category_id' => 5 ]);
          //PRETRATADORES DE MANCHAS PARA ROPA
          Brand::create(['name' => 'CLORALEX', 'category_id' => 6 ]);
          Brand::create(['name' => 'CLOROX', 'category_id' => 6 ]);
          Brand::create(['name' => 'DR BECKMANN', 'category_id' => 6 ]);
          Brand::create(['name' => 'EL MAGO', 'category_id' => 6 ]);
          Brand::create(['name' => 'MEMBERS MARK', 'category_id' => 6 ]);
          Brand::create(['name' => 'OXY CLEAN', 'category_id' => 6 ]);
          Brand::create(['name' => 'VANISH', 'category_id' => 6 ]);
          //LAVATRASTES
          Brand::create(['name' => 'AXION', 'category_id' => 7 ]);
          Brand::create(['name' => 'EFICAZ', 'category_id' => 7 ]);
          Brand::create(['name' => 'SALVO', 'category_id' => 7 ]);
          Brand::create(['name' => 'SAVIA', 'category_id' => 7 ]);
          //PRODUCTOS QUITAGRASA
          Brand::create(['name' => 'AJAX BICLORO', 'category_id' => 8 ]);
          Brand::create(['name' => 'BRASSO', 'category_id' => 8 ]);
          Brand::create(['name' => 'EASY OFF', 'category_id' => 8 ]);
          Brand::create(['name' => 'KLAR', 'category_id' => 8 ]);
          Brand::create(['name' => 'MR MUSCULO', 'category_id' => 8 ]);
          Brand::create(['name' => 'NOW', 'category_id' => 8 ]);
          Brand::create(['name' => 'PINOL', 'category_id' => 8 ]);
          Brand::create(['name' => 'SCOTCH BRITE', 'category_id' => 8 ]);
          Brand::create(['name' => 'STAN HOME', 'category_id' => 8 ]);
          Brand::create(['name' => 'VERT', 'category_id' => 8 ]);
          //PRODUCTOS DESINFECTANTES
          Brand::create(['name' => 'AJAX', 'category_id' => 9 ]);
          Brand::create(['name' => 'CLOROX', 'category_id' => 9 ]);
          Brand::create(['name' => 'LYSOL', 'category_id' => 9 ]);

          //PRODUCTOS LIMPIAPISOS
          Brand::create(['name' => 'AJAX', 'category_id' => 10 ]);
          Brand::create(['name' => 'BRASSO', 'category_id' => 10 ]);
          Brand::create(['name' => 'CHEDRAUI', 'category_id' => 10 ]);
          Brand::create(['name' => 'FABULOSO', 'category_id' => 10 ]);
          Brand::create(['name' => 'FLASH', 'category_id' => 10 ]);
          Brand::create(['name' => 'LOS PATITOS', 'category_id' => 10 ]);
          Brand::create(['name' => 'LYSOL', 'category_id' => 10 ]);
          Brand::create(['name' => 'MAXI HOGAR', 'category_id' => 10 ]);
          Brand::create(['name' => 'PINOL', 'category_id' => 10 ]);
          Brand::create(['name' => 'POETT', 'category_id' => 10 ]);

          //PRODUCTOS LIMPIA VIDRIOS
          Brand::create(['name' => 'BRASSO', 'category_id' => 11 ]);
          Brand::create(['name' => 'KLAR', 'category_id' => 11 ]);
          Brand::create(['name' => 'MR MUSCULO', 'category_id' => 11 ]);
          Brand::create(['name' => 'PINOL', 'category_id' => 11 ]);
          Brand::create(['name' => 'STAN HOME', 'category_id' => 11 ]);
          Brand::create(['name' => 'WINDEX', 'category_id' => 11 ]);

          //LIMPIADOR DE BAÑOS', 'category_id' => 8 ]);
          Brand::create(['name' => 'AJAX', 'category_id' => 12 ]);
          Brand::create(['name' => 'HARPIC', 'category_id' => 12 ]);
          Brand::create(['name' => 'MR MUSCULO', 'category_id' => 12 ]);
          Brand::create(['name' => 'WINDEX', 'category_id' => 12 ]);

          //PRODUCTOS MULTIUSOS', 'category_id' => 8 ]);
          Brand::create(['name' => 'ARM & HAMMER', 'category_id' => 13 ]);
          Brand::create(['name' => 'BREF', 'category_id' => 13 ]);
          Brand::create(['name' => 'POETT', 'category_id' => 13 ]);

          //SHAMPOO PARA EL CABELLO', 'category_id' => 8 ]);
          Brand::create(['name' => 'ALERT', 'category_id' => 14 ]);
          Brand::create(['name' => 'ALFAPARF', 'category_id' => 14 ]);
          Brand::create(['name' => 'BIO EXPERT', 'category_id' => 14 ]);
          Brand::create(['name' => 'BIO VITAL', 'category_id' => 14 ]);
          Brand::create(['name' => 'BOTANIST', 'category_id' => 14 ]);
          Brand::create(['name' => 'CAPRICE', 'category_id' => 14 ]);
          Brand::create(['name' => 'CRE C', 'category_id' => 14 ]);
          Brand::create(['name' => 'DOVE', 'category_id' => 14 ]);
          Brand::create(['name' => 'EGO', 'category_id' => 14 ]);
          Brand::create(['name' => 'ELVIVE', 'category_id' => 14 ]);
          Brand::create(['name' => 'FERMODYL', 'category_id' => 14 ]);
          Brand::create(['name' => 'FOLICURE', 'category_id' => 14 ]);
          Brand::create(['name' => 'FRUCTIS', 'category_id' => 14 ]);
          Brand::create(['name' => 'GRISI', 'category_id' => 14 ]);
          Brand::create(['name' => 'HEAD & SHOULDERS', 'category_id' => 14 ]);
          Brand::create(['name' => 'HERBAL ESSENCES', 'category_id' => 14 ]);
          Brand::create(['name' => 'HUGGIES', 'category_id' => 14 ]);
          Brand::create(['name' => 'JOHNSON`S', 'category_id' => 14 ]);
          Brand::create(['name' => 'JUST FOR MEN', 'category_id' => 14 ]);
          Brand::create(['name' => 'LOREAL', 'category_id' => 14 ]);
          Brand::create(['name' => 'MARC ANTOHONY', 'category_id' => 14 ]);
          Brand::create(['name' => 'MEDICASP', 'category_id' => 14 ]);
          Brand::create(['name' => 'MENNEN', 'category_id' => 14 ]);
          Brand::create(['name' => 'MUSTELA', 'category_id' => 14 ]);
          Brand::create(['name' => 'OGX', 'category_id' => 14 ]);
          Brand::create(['name' => 'OLD SPICE', 'category_id' => 14 ]);
          Brand::create(['name' => 'ORGANOGAL', 'category_id' => 14 ]);
          Brand::create(['name' => 'PALMOLIVE OPTIMS', 'category_id' => 14 ]);
          Brand::create(['name' => 'PANTENE', 'category_id' => 14 ]);
          Brand::create(['name' => 'PERT', 'category_id' => 14 ]);
          Brand::create(['name' => 'RISITOS DE ORO', 'category_id' => 14 ]);
          Brand::create(['name' => 'SALERM', 'category_id' => 14 ]);
          Brand::create(['name' => 'SAVILE', 'category_id' => 14 ]);
          Brand::create(['name' => 'SCHWARZKOP', 'category_id' => 14 ]);
          Brand::create(['name' => 'SEDAL', 'category_id' => 14 ]);
          Brand::create(['name' => 'TIO NACHO', 'category_id' => 14 ]);
          Brand::create(['name' => 'TRESEMME', 'category_id' => 14 ]);
          Brand::create(['name' => 'VANART', 'category_id' => 14 ]);
          Brand::create(['name' => 'VITANOL', 'category_id' => 14 ]);

          //ACONDICIONADOR PARA EL CABELLO', 'category_id' => 13 ]);
          Brand::create(['name' => 'BIO EXPERT', 'category_id' => 15 ]);
          Brand::create(['name' => 'CAPRICE', 'category_id' => 15 ]);
          Brand::create(['name' => 'DOVE', 'category_id' => 15 ]);
          Brand::create(['name' => 'ELVIVE', 'category_id' => 15 ]);
          Brand::create(['name' => 'FRUCTIS', 'category_id' => 15 ]);
          Brand::create(['name' => 'HERBAL ESSENCES', 'category_id' => 15 ]);
          Brand::create(['name' => 'OGX', 'category_id' => 15 ]);
          Brand::create(['name' => 'PALMOLIVE OPTIMS', 'category_id' => 15 ]);
          Brand::create(['name' => 'PANTENE', 'category_id' => 15 ]);
          Brand::create(['name' => 'PERT', 'category_id' => 15 ]);
          Brand::create(['name' => 'SAVILE', 'category_id' => 15 ]);
          Brand::create(['name' => 'SEDAL', 'category_id' => 15 ]);
          Brand::create(['name' => 'TRESEME', 'category_id' => 15 ]);
          Brand::create(['name' => 'VANART', 'category_id' => 15 ]);

          //GEL PARA EL CABELLO', 'category_id' => 8 ]);
          Brand::create(['name' => 'ATM', 'category_id' => 16 ]);
          Brand::create(['name' => 'CAPRICE', 'category_id' => 16 ]);
          Brand::create(['name' => 'CERILLA DE GORILA', 'category_id' => 16 ]);
          Brand::create(['name' => 'EGO', 'category_id' => 16 ]);
          Brand::create(['name' => 'FRUCTIS', 'category_id' => 16 ]);
          Brand::create(['name' => 'XIOMARA', 'category_id' => 16 ]);
          Brand::create(['name' => 'XTREME', 'category_id' => 16 ]);

          //SPRAY PARA EL CABELLO', 'category_id' => 15 ]);
          Brand::create(['name' => 'AQUANET', 'category_id' => 17 ]);
          Brand::create(['name' => 'CAPRICE', 'category_id' => 17 ]);
          Brand::create(['name' => 'JOHNSONS', 'category_id' => 17 ]);
          Brand::create(['name' => 'TRESEMME', 'category_id' => 17 ]);

          //MOUSSE PARA EL CABELLO', 'category_id' => 8 ]);
          Brand::create(['name' => 'CAPRICE', 'category_id' => 18 ]);
          Brand::create(['name' => 'HERBAL ESSENCES', 'category_id' => 18 ]);
          Brand::create(['name' => 'PALMOLIVE NEUTRO BALANCE', 'category_id' => 18 ]);
          Brand::create(['name' => 'SCHWARZKOP', 'category_id' => 18 ]);

          //CREMA PARA PEINAR PARA EL CABELLO', 'category_id' => 8 ]);
          Brand::create(['name' => 'ELVIVE', 'category_id' => 19 ]);
          Brand::create(['name' => 'FRUCTIS', 'category_id' => 19 ]);
          Brand::create(['name' => 'HERBAL ESSENCES', 'category_id' => 19 ]);
          Brand::create(['name' => 'LOREAL', 'category_id' => 19 ]);
          Brand::create(['name' => 'PANTENE', 'category_id' => 19 ]);
          Brand::create(['name' => 'PERT', 'category_id' => 19 ]);
          Brand::create(['name' => 'SAVILE', 'category_id' => 19 ]);
          Brand::create(['name' => 'SEDAL', 'category_id' => 19 ]);
          Brand::create(['name' => 'TRESEMME', 'category_id' => 19 ]);

          //TINTE PARA EL CABELLO', 'category_id' => 8 ]);
          Brand::create(['name' => 'COLOR TECH', 'category_id' => 20 ]);
          Brand::create(['name' => 'GARNIER NUTRISSE', 'category_id' => 20 ]);
          Brand::create(['name' => 'INMEDIA EXCELLENCE', 'category_id' => 20 ]);
          Brand::create(['name' => 'JUST FOR MEN', 'category_id' => 20 ]);
          Brand::create(['name' => 'KOLESTON', 'category_id' => 20 ]);
          Brand::create(['name' => 'LOREAL CASTING', 'category_id' => 20 ]);
          Brand::create(['name' => 'MISS CLAIROL', 'category_id' => 20 ]);
          Brand::create(['name' => 'PALETTE', 'category_id' => 20 ]);
          Brand::create(['name' => 'PREFERENCE', 'category_id' => 20 ]);
          Brand::create(['name' => 'REVLON', 'category_id' => 20 ]);
          Brand::create(['name' => 'XIOMARA', 'category_id' => 20 ]);

          //DESODORANTES DAMA', 'category_id' => 8 ]);
          Brand::create(['name' => 'ADIDAS', 'category_id' => 21 ]);
          Brand::create(['name' => 'AXE', 'category_id' => 21 ]);
          Brand::create(['name' => 'BAN', 'category_id' => 21 ]);
          Brand::create(['name' => 'DOVE', 'category_id' => 21 ]);
          Brand::create(['name' => 'ENGLISH LADY', 'category_id' => 21 ]);
          Brand::create(['name' => 'GARNIER BIO', 'category_id' => 21 ]);
          Brand::create(['name' => 'LACTOVIT', 'category_id' => 21 ]);
          Brand::create(['name' => 'LADY SPEED STICK', 'category_id' => 21 ]);
          Brand::create(['name' => 'MITCHUM', 'category_id' => 21 ]);
          Brand::create(['name' => 'NATURA', 'category_id' => 21 ]);
          Brand::create(['name' => 'NIVEA', 'category_id' => 21 ]);
          Brand::create(['name' => 'NUVEL', 'category_id' => 21 ]);
          Brand::create(['name' => 'OBAO', 'category_id' => 21 ]);
          Brand::create(['name' => 'PALMOLIVE', 'category_id' => 21 ]);
          Brand::create(['name' => 'REXONA', 'category_id' => 21 ]);
          Brand::create(['name' => 'SAVILE', 'category_id' => 21 ]);
          Brand::create(['name' => 'SECRET', 'category_id' => 21 ]);
          Brand::create(['name' => 'SUAVE', 'category_id' => 21 ]);

          //DESODORANTES HOMBRES', 'category_id' => 8 ]);
          Brand::create(['name' => 'ADIDAS', 'category_id' => 22 ]);
          Brand::create(['name' => 'ARM & HAMMER', 'category_id' => 22 ]);
          Brand::create(['name' => 'AVON', 'category_id' => 22 ]);
          Brand::create(['name' => 'AXE', 'category_id' => 22 ]);
          Brand::create(['name' => 'BRUT', 'category_id' => 22 ]);
          Brand::create(['name' => 'DOVE', 'category_id' => 22 ]);
          Brand::create(['name' => 'GILLETTE', 'category_id' => 22 ]);
          Brand::create(['name' => 'JOVAN', 'category_id' => 22 ]);
          Brand::create(['name' => 'NIVEA', 'category_id' => 22 ]);
          Brand::create(['name' => 'OBAO', 'category_id' => 22 ]);
          Brand::create(['name' => 'OLD SPICE', 'category_id' => 22 ]);
          Brand::create(['name' => 'PLAY BOY', 'category_id' => 22 ]);
          Brand::create(['name' => 'REXONA', 'category_id' => 22 ]);
          Brand::create(['name' => 'SPEED STICK', 'category_id' => 22 ]);
          Brand::create(['name' => 'STEFANO', 'category_id' => 22 ]);

          //PASTA DENTAL', 'category_id' => 8 ]);
          Brand::create(['name' => 'ARM & HAMMER', 'category_id' => 23 ]);
          Brand::create(['name' => 'COLGATE', 'category_id' => 23 ]);
          Brand::create(['name' => 'CREST', 'category_id' => 23 ]);
          Brand::create(['name' => 'DENSIBLEND', 'category_id' => 23 ]);
          Brand::create(['name' => 'ORAL B', 'category_id' => 23 ]);
          Brand::create(['name' => 'ORIFLAME', 'category_id' => 23 ]);
          Brand::create(['name' => 'SENSODYME', 'category_id' => 23 ]);

          //ENJUAGUE BUCAL', 'category_id' => 8 ]);
          Brand::create(['name' => 'ASTRINGOSOL', 'category_id' => 24 ]);
          Brand::create(['name' => 'COLGATE', 'category_id' => 24 ]);
          Brand::create(['name' => 'CRET', 'category_id' => 24 ]);
          Brand::create(['name' => 'EQUATE', 'category_id' => 24 ]);
          Brand::create(['name' => 'LISTERINE', 'category_id' => 24 ]);
          Brand::create(['name' => 'ORAL B', 'category_id' => 24 ]);
          Brand::create(['name' => 'KIRKLAND', 'category_id' => 24 ]);

          //TOALLAS FEMENINAS SANITARIAS', 'category_id' => 8 ]);
          Brand::create(['name' => 'ALWAYS', 'category_id' => 25 ]);
          Brand::create(['name' => 'KOTEX', 'category_id' => 25 ]);
          Brand::create(['name' => 'NATURELLA', 'category_id' => 25 ]);
          Brand::create(['name' => 'SABA', 'category_id' => 25 ]);

          //PANTIPROTECTORES  FEMENINOS', 'category_id' => 8 ]);
          Brand::create(['name' => 'DEPEND', 'category_id' => 26 ]);
          Brand::create(['name' => 'KOTEX', 'category_id' => 26 ]);
          Brand::create(['name' => 'NATURELLA', 'category_id' => 26 ]);
          Brand::create(['name' => 'SABA', 'category_id' => 26 ]);

          //TAMPONES', 'category_id' => 8 ]);
          Brand::create(['name' => 'PLAYTEX', 'category_id' => 27 ]);
          Brand::create(['name' => 'SABA', 'category_id' => 27 ]);
          Brand::create(['name' => 'KOTEX', 'category_id' => 27 ]);

          //JABON INTIMO', 'category_id' => 8 ]);
          Brand::create(['name' => 'BENZAL', 'category_id' => 28 ]);
          Brand::create(['name' => 'LACTACYD', 'category_id' => 28 ]);
          Brand::create(['name' => 'LOMECAN', 'category_id' => 28 ]);

          //CREMA FACIAL', 'category_id' => 8 ]);
          Brand::create(['name' => 'AVON', 'category_id' => 29 ]);
          Brand::create(['name' => 'EUCERIN', 'category_id' => 29 ]);
          Brand::create(['name' => 'GARNIER', 'category_id' => 29 ]);
          Brand::create(['name' => 'LOREAL', 'category_id' => 29 ]);
          Brand::create(['name' => 'NIVEA', 'category_id' => 29 ]);
          Brand::create(['name' => 'PONDS', 'category_id' => 29 ]);
          Brand::create(['name' => 'TEATRICAL', 'category_id' => 29 ]);

          //CREMA CORPORAL', 'category_id' => 8 ]);
          Brand::create(['name' => 'CETAPHIL', 'category_id' => 30 ]);
          Brand::create(['name' => 'DOVE', 'category_id' => 30 ]);
          Brand::create(['name' => 'GOICOECHEA', 'category_id' => 30 ]);
          Brand::create(['name' => 'GRISI', 'category_id' => 30 ]);
          Brand::create(['name' => 'HINDS', 'category_id' => 30 ]);
          Brand::create(['name' => 'KLEEN BEBE', 'category_id' => 30 ]);
          Brand::create(['name' => 'LUBRIDERM', 'category_id' => 30 ]);
          Brand::create(['name' => 'MEMBERS MARK', 'category_id' => 30 ]);
          Brand::create(['name' => 'NIVEA', 'category_id' => 30 ]);
          Brand::create(['name' => 'NUVEL', 'category_id' => 30 ]);
          Brand::create(['name' => 'PALMOLIVE', 'category_id' => 30 ]);
          Brand::create(['name' => 'ST YVES', 'category_id' => 30 ]);
          Brand::create(['name' => 'TEATRICAL', 'category_id' => 30 ]);

          //JABON CORPORA', 'category_id' => 8 ]);
          Brand::create(['name' => 'AXE', 'category_id' => 31 ]);
          Brand::create(['name' => 'CAMAY', 'category_id' => 31 ]);
          Brand::create(['name' => 'DIAL', 'category_id' => 31 ]);
          Brand::create(['name' => 'DOVE', 'category_id' => 31 ]);
          Brand::create(['name' => 'ESCUDO', 'category_id' => 31 ]);
          Brand::create(['name' => 'GRISI', 'category_id' => 31 ]);
          Brand::create(['name' => 'HENO', 'category_id' => 31 ]);
          Brand::create(['name' => 'KLEENEX', 'category_id' => 31 ]);
          Brand::create(['name' => 'LIRIO', 'category_id' => 31 ]);
          Brand::create(['name' => 'MEMBERS MARK', 'category_id' => 31 ]);
          Brand::create(['name' => 'NATURA', 'category_id' => 31 ]);
          Brand::create(['name' => 'NIVEA', 'category_id' => 31 ]);
          Brand::create(['name' => 'OLD SPICE', 'category_id' => 31 ]);
          Brand::create(['name' => 'PALMOLIVE', 'category_id' => 31 ]);
          Brand::create(['name' => 'ROSA VENUS', 'category_id' => 31 ]);
          Brand::create(['name' => 'SAVILE', 'category_id' => 31 ]);
          Brand::create(['name' => 'TERSSO', 'category_id' => 31 ]);
          Brand::create(['name' => 'ZEST', 'category_id' => 31 ]);

     }
}
