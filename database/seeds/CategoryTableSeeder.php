<?php

use App\Category;
use App\MainCategory;
use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         Category::truncate();




         Category::create(['name' => 'DETERGENTE POLVO',  'category_id' => 32 ]);
         Category::create(['name' => 'DETERGENTE LIQUIDO',  'category_id' => 32 ]);
         Category::create(['name' => 'SUAVIZANTES PARA ROPA',  'category_id' => 32 ]);
         Category::create(['name' => 'JABON DE BARRA',  'category_id' => 32 ]);
         Category::create(['name' => 'CLORO/BLANQUEADOR DE ROPA',  'category_id' => 32 ]);
         Category::create(['name' => 'PRETRATADORES DE MANCHAS PARA ROPA',  'category_id' => 32 ]);
         Category::create(['name' => 'LAVATRASTES',  'category_id' => 33 ]);
         Category::create(['name' => 'PRODUCTOS QUITAGRASA',  'category_id' => 33 ]);
         Category::create(['name' => 'PRODUCTOS DESINFECTANTES',  'category_id' => 33 ]);
         Category::create(['name' => 'PRODUCTOS LIMPIAPISOS',  'category_id' => 33 ]);
         Category::create(['name' => 'PRODUCTOS LIMPIA VIDRIOS',  'category_id' => 33 ]);
         Category::create(['name' => 'LIMPIADOR DE BAÑOS',  'category_id' => 33 ]);
         Category::create(['name' => 'PRODUCTOS MULTIUSOS',  'category_id' => 33 ]);
         Category::create(['name' => 'SHAMPOO PARA EL CABELLO',  'category_id' => 34 ]);
         Category::create(['name' => 'ACONDICIONADOR PARA EL CABELLO',  'category_id' => 34 ]);
         Category::create(['name' => 'GEL PARA EL CABELLO',  'category_id' => 34 ]);
         Category::create(['name' => 'SPRAY PARA EL CABELL',  'category_id' => 34 ]);
         Category::create(['name' => 'MOUSSE PARA EL CABELLO',  'category_id' => 34 ]);
         Category::create(['name' => 'CREMA PARA PEINAR PARA EL CABELLO',  'category_id' => 34 ]);
         Category::create(['name' => 'TINTE PARA EL CABELLO',  'category_id' => 34 ]);
         Category::create(['name' => 'DESODORANTES DAMA',  'category_id' => 34 ]);
         Category::create(['name' => 'DESODORANTES HOMBRES',  'category_id' => 34 ]);
         Category::create(['name' => 'PASTA DENTAL',  'category_id' => 34 ]);
         Category::create(['name' => 'ENJUAGUE BUCAL',  'category_id' => 34 ]);
         Category::create(['name' => 'TOALLAS FEMENINAS SANITARIAS',  'category_id' => 34 ]);
         Category::create(['name' => 'PANTIPROTECTORES  FEMENINOS',  'category_id' => 34 ]);
         Category::create(['name' => 'TAMPONES',  'category_id' => 34 ]);
         Category::create(['name' => 'JABON INTIMO',  'category_id' => 34 ]);
         Category::create(['name' => 'CREMA FACIAL',  'category_id' => 34 ]);
         Category::create(['name' => 'CREMA CORPORAL',  'category_id' => 34 ]);
         Category::create(['name' => 'JABON CORPORA',  'category_id' => 34 ]);


         Category::create(['name' => 'PRODUCTOS DE LAVADO', 'order' => '3', 'logo' => 'https://image.flaticon.com/icons/png/512/115/115513.png']);
         Category::create(['name' => 'PRODUCTOS PARA LA LIMPIEZA DEL HOGAR', 'order' => '2', 'logo' => 'https://www.kindpng.com/picc/m/275-2757623_presupuesto-de-limpieza-icono-servicio-de-limpieza-hd.png']);
         Category::create(['name' => 'PRODUCTOS PARA EL CUIDADO PERSONAL', 'order' => '1' ,'logo' => 'https://image.flaticon.com/icons/svg/2584/2584855.svg']);
    }
}
