@extends('admin.layout')

@section('content')
<div class="col-md-12">
     <form class="form" method="post" action="{{ route('answer.update', ['user_id' => $user->id, 'answer' => $survey]) }}">
          {{ csrf_field() }} {{ method_field('PUT') }}
          <input type="hidden" name="survey_id" value="{{ $survey->id }}">
          <div class="card">
               <div class="card-header ">
                    <div class="card-header">
                         <h4 class="card-title">Encuetas</h4>
                    </div>
               </div>
               <div class="card-body ">
                    <div class="row">
                         <div class="col-md-12">
                              @if($errors->any())
                              <ul class="list-group">
                                   @foreach($errors->all() as $key => $error)
                                   <li class="list-group-item list-group-item-danger"> {{ $error }}</li>
                                   @endforeach
                              </ul>
                              @endif
                         </div>
                         <div class="col-md-12">
                              <div class="row">
                                   <div class="col-md-12">
                                        <div class="form-group">
                                             <label for="name">Titulo</label>
                                             <p>{{ $survey->title }}</p>
                                        </div>
                                   </div>
                                   <div class="col-md-12">
                                        <div class="form-group">
                                             <label for="study_id">Estudio</label>
                                             <p>{{ $survey->study->name }}</p>
                                        </div>
                                   </div>
                                   <div class="col-md-12">
                                        <div class="form-group">
                                             <label for="email">Descripción</label>
                                             <p>{{ $survey->description }}</p>
                                        </div>
                                   </div>
                                   <div class="col-md-12">
                                        <hr>
                                   </div>

                                   <div class="col-md-12">
                                        <?php foreach ($survey->question as $key => $pregunta): ?>
                                             <div class="jumbotron">
                                                  <div class="col-md-12">
                                                       <div class="form-group">
                                                            <div class="row">
                                                                 <div class="col-md-12">
                                                                      <div class="form-group">
                                                                           <strong>Pregunta</strong><br>
                                                                           <p><?=$pregunta->questions?></p>
                                                                      </div>
                                                                 </div>
                                                            </div>
                                                            <hr>
                                                            <div class="row">
                                                                 <div class="col-sm-12">
                                                                      <strong >Repuestas</strong>
                                                                 </div>
                                                                 <?php if($pregunta->catalog_question_id == 1): ?>
                                                                      <?php foreach ($pregunta->answers as $key => $respueta): ?>
                                                                           <div class="col-md-4">
                                                                                <input type="radio"  name="pregunta[<?=$pregunta->id?>]"
                                                                                     {{ collect($user->answer->pluck('id'))->contains($respueta->id) ? 'checked':'' }}

                                                                                 value="<?=$respueta->id?>">
                                                                                <label style="font-size:16px;"><?=$respueta->answer?></label>
                                                                           </div>
                                                                      <?php endforeach; ?>
                                                                 <?php else: ?>
                                                                      <div class="col-md-12">
                                                                           <textarea name="pregunta[<?=$pregunta->id?>]" style="height:100px;" rows="8" cols="80" class="form-control"><?=$pregunta->open_answer($pregunta->id, $user->id)->open_answer?></textarea>
                                                                      </div>
                                                                 <?php endif;?>
                                                            </div>
                                                       </div>
                                                  </div>
                                             </div>
                                   <?php endforeach; ?>
                              </div>
                              <div class="col-md-12">
                                   <hr>
                              </div>
                         </div>
                    </div>
               </div>
               <a href="{{ route('answer.index', ['user_id' => $user->id]) }}" class="btn btn-default btn-fill pull-left">Cancelar</a>
               <button type="submit" class="btn btn-info btn-fill pull-right">Guardar</button>
               <div class="clearfix"></div>
          </div>
     </div>

</form>
</div>
<!-- @include('admin.survey.modal_question') -->

@stop
