@extends('admin.layout')

@section('content')
<div class="col-md-12">
     <div class="card strpied-tabled-with-hover">
          <div class="card-header ">
               <h4 class="card-title">Administradores</h4>
               <p class="card-category">Lista de administradores</p>
               <a href="{{ route('admin.user.create') }}" class="btn btn-success btn-wd" style="float: right; margin-top: -50px;">
                    <i class="fa fa-plus"></i> Nuevo
               </a>
          </div>
          <div class="card-body table-responsive">
               <table class="table table-hover table-striped table_js">
                    <thead>
                         <tr>
                              <th>ID</th>
                              <th>Nombre</th>
                              <th>Correo</th>
                              <th class="text-center">Estatus</th>
                              <th class="text-center"><i class="nc-icon nc-preferences-circle-rotate"></i></th>
                         </tr>
                    </thead>
                    <tbody>
                         @foreach ($users as $user)
                         <tr>
                              <td>{{ $user->id }}</td>
                              <td>{{ $user->name }}</td>
                              <td>{{ $user->email }}</td>
                              <td class="text-center">
                                   @if($user->status == 1)
                                        <h6><span class="badge badge-success">Activo</span></h6>
                                   @else
                                        <h6><span class="badge badge-danger">Inactivo</span></h6>
                                   @endif
                              </td>
                              <td class="text-center">
                                   @can('Ver Administradores')
                                   <a href="{{ route('admin.user.show', $user) }}"
                                        class="btn btn-sm btn-default btn-outline"
                                   > <i class="fa fa-eye"></i> </a>
                                   @endcan
                                   @can('Editar Administradores')
                                   <a href="{{ route('admin.user.edit', $user) }}"
                                        class="btn btn-sm btn-info btn-outline"
                                   > <i class="fa fa-pencil"></i> </a>
                                   @endcan
                                   @can('Eliminar Administradores')
                                   <form method="POST"
                                        action="{{ route('admin.user.destroy', $user) }}"
                                        style="display:inline">
                                        {{ csrf_field() }} {{ method_field('DELETE') }}
                                        <button class="btn btn-sm btn-danger btn-outline"
                                             onclick="return confirm('¿ Estás seguro de querer eliminar este administrador ?')"
                                         >
                                         <i class="fa fa-trash"></i>
                                        </button>
                                   </form>
                                   @endcan
                              </td>
                         </tr>
                         @endforeach
                    </tbody>
               </table>
          </div>
     </div>
</div>
@stop
