@extends('admin.layout')

@section('content')
@if(auth()->user()->can('Editar roles'))
<div class="col-md-6">
@else
<div class="col-md-6 ml-auto mr-auto">
@endif
     <form class="form" method="post" action="{{ route('admin.capturist.update', $capturist) }}">
          <div class="card ">
               <div class="card-header ">
                    <div class="card-header">
                         <h4 class="card-title">Datos personales</h4>
                    </div>
               </div>
               <div class="card-body ">
                    <div class="row">
                         <div class="col-md-12">
                              @if($errors->any())
                                   <ul class="list-group">
                                        @foreach($errors->all() as $key => $error)
                                             <li class="list-group-item list-group-item-danger"> {{ $error }}</li>
                                        @endforeach
                                   </ul>
                              @endif
                         </div>
                    </div>
                    <div class="row">
                         {{ csrf_field() }} {{ method_field('PUT') }}
                         <div class="col-md-12">
                              <div class="form-group">
                                   <label for="name">Nombre</label>
                                   <input type="text" name="name" class="form-control" value="{{ old('name', $capturist->name) }}" placeholder="Nombre">
                              </div>
                         </div>
                    </div>
                    <div class="row">
                         <div class="col-md-12">
                              <div class="form-group">
                                   <label for="email">Correo</label>
                                   <input type="email" name="email" class="form-control" value="{{ old('email', $capturist->email) }}" placeholder="Correo">
                              </div>
                         </div>
                    </div>
                    <div class="row">
                         <div class="col-md-12">
                              <div class="form-group">
                                   <label for="status">Estatus</label><br>
                                   <div class="onoffswitch">
                                       <input type="checkbox"
                                        name="status"
                                        value="1"
                                        @if( old('status', $capturist->status) == 1 ) checked @endif
                                        class="onoffswitch-checkbox"
                                        id="myonoffswitch" >
                                       <label class="onoffswitch-label" for="myonoffswitch">
                                           <span class="onoffswitch-inner"></span>
                                           <span class="onoffswitch-switch"></span>
                                       </label>
                                   </div>
                              </div>
                         </div>
                    </div>
                    <div class="row">
                         <div class="col-md-12">
                              <div class="form-group">
                                   <label for="password">Contraseña</label>
                                   <input type="password" name="password" class="form-control"  placeholder="Contraseña">
                                   <span class="help-block">Dejar en blanco para no cambiar la contraseña</span>
                              </div>
                         </div>
                    </div>
                    <div class="row">
                         <div class="col-md-12">
                              <div class="form-group">
                                   <label for="password_confirmation">Repite la contraseña</label>
                                   <input type="password" name="password_confirmation" class="form-control"  placeholder="Repite la contraseña">
                              </div>
                         </div>
                    </div>
                    <hr>
                    <button type="submit" class="btn btn-info btn-fill pull-right">Actualizar usuario</button>
                    <a href="{{ route('admin.leader.index') }}" class="btn btn-default btn-fill pull-left">Cancelar</a>
                    <div class="clearfix"></div>
               </div>
          </div>
     </form>
</div>
@can('Editar roles')
<div class="col-md-6 col-sm-6">
     <form class="form" method="post" action="{{ route('admin.leader.permissions.update', $capturist) }}">
          <div class="card ">
               <div class="card-header ">
                    <div class="card-header">
                         <h4 class="card-title">Permisos</h4>
                    </div>
               </div>
               <div class="card-body ">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                    @include('admin.permissions-checkboxes')

                    <hr>
                    <button type="submit" class="btn btn-info btn-fill pull-right">Actualizar usuario</button>

               </div>
          </div>
     </form>
</div>
@endcan
@stop
