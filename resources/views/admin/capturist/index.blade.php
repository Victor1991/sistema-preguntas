@extends('admin.layout')

@section('content')
<div class="col-md-12">
     <div class="card strpied-tabled-with-hover">
          <div class="card-header ">
               <h4 class="card-title">Capturistas</h4>
               <p class="card-category">Lista de capturistas</p>
               <a href="{{ route('admin.capturist.create') }}" class="btn btn-success btn-wd" style="float: right; margin-top: -50px;">
                    <i class="fa fa-plus"></i> Nuevo
               </a>
          </div>
          <div class="card-body table-responsive">
               <table class="table table-hover table-striped table_js">
                    <thead>
                         <tr>
                              <th>ID</th>
                              <th>Nombre</th>
                              <th>Correo</th>
                              <th>Estatus</th>
                              <th class="text-center">Activar</th>
                              <th class="text-center"><i class="nc-icon nc-preferences-circle-rotate"></i></th>
                         </tr>
                    </thead>

                    <tbody>
                         @foreach ($users as $capturist)
                         <tr>
                              <td>{{ $capturist->id }}</td>
                              <td>{{ $capturist->name }}</td>
                              <td>{{ $capturist->email }}</td>
                              <td class="text-center">
                                   @if($capturist->status == 1)
                                   <h6><span class="badge badge-success">Activo</span></h6>
                                   @else
                                   <h6><span class="badge badge-danger">Inactivo</span></h6>
                                   @endif
                              </td>
                              <td class="text-center">
                                   <input type="checkbox" class="l_capturista" value="{{ $capturist->id }}">
                              </td>
                              <td class="text-center">
                                   @can('Ver Capturista')
                                   <a href="{{ route('admin.capturist.show', $capturist) }}"
                                   class="btn btn-sm btn-default btn-outline"
                                   > <i class="fa fa-eye"></i> </a>
                                   @endcan
                                   @can('Editar Capturista')
                                   <a href="{{ route('admin.capturist.edit', $capturist) }}"
                                   class="btn btn-sm btn-info btn-outline"
                                   > <i class="fa fa-pencil"></i> </a>
                                   @endcan
                                   @can('Eliminar Capturista')
                                   <form method="POST"
                                   action="{{ route('admin.capturist.destroy', $capturist) }}"
                                   style="display:inline">
                                   {{ csrf_field() }} {{ method_field('DELETE') }}
                                   <button class="btn btn-sm btn-danger btn-outline"
                                   onclick="return confirm('¿ Estás seguro de querer eliminar este capturista ?')"
                                   >
                                   <i class="fa fa-trash"></i>
                              </button>
                         </form>
                         @endcan

                    </td>
               </tr>
               @endforeach
          </tbody>
          <tfoot>
               <tr>
                    <td colspan="6">
                         <button style="display:none; float:right;" id="btn_activar"  type="button" data-toggle="modal" data-target="#activar_capturistas" class="btn btn-success pull-left">Activar usuarios</button>
                    </td>
               </tr>
          </tfoot>
     </table>
</div>
</div>
</div>



<div class="modal fade modal-mini modal-primary " id="activar_capturistas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
     <form method="POST" action="{{ route('admin.capturista.activar_capturistas') }}">
          {{ csrf_field() }}
          <div class="modal-dialog">
               <div class="modal-content">
                    <div class="modal-header justify-content-center">
                         <div class="modal-profile">
                              <i class="nc-icon nc-time-alarm"></i>
                         </div>
                    </div>
                    <div class="modal-body text-center">
                         <p> Tiempo actividad : </p>
                         <input type="text" style="text-align:center;" name="tiempo" required class="form-contorl clockpicker" value="">
                         <input type="hidden" name="ip" id="ip" value="">
                         <div id="list_user_active">
                         </div>
                    </div>
                    <div class="modal-footer">
                         <div class="col-md-12">
                              <button type="button" class="btn btn-default btn-simple" data-dismiss="modal">Cancelar</button>
                              <button type="submit" class="btn btn-primary">Activar</button>
                         </div>
                    </div>
               </div>
          </div>
     </form>
</div>

@stop
<!-- Estilos pasarlos al layout -->
@push('style')
<style media="screen">
.popover.clockpicker-popover.bottom.clockpicker-align-left{
    z-index: 9999;
}
</style>

@endpush
<!-- Escripts pasarlos al layout -->
@push('scripts')
<script>
$( document ).ready(function() {

     $.getJSON('https://api.ipify.org?format=json', function(data){
          $('#ip').val(data.ip);
     });

     $('.l_capturista').change(function() {
          var ver_boton = false;
          $('#btn_activar').hide();
          $('#list_user_active').html('');
          $('.l_capturista').each(function () {
               if (this.checked) {
                    $('#btn_activar').show();
                    $('#list_user_active').append('<input type="hidden" name="user[]" value="'+this.value+'" />');
               }
          });

     });

});
</script>
@endpush
