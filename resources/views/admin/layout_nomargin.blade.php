<!DOCTYPE html>
<html lang="en">

<head>
     <meta charset="utf-8" />
     <link rel="apple-touch-icon" sizes="76x76" href="/admin_theme/img/apple-icon.png">
     <link rel="icon" type="image/png" href="https://upload.wikimedia.org/wikipedia/commons/6/63/Ingress_Logo.png">
     <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
     <title>{{ config('app.name') }}</title>
     <meta name="csrf-token" content="{{ csrf_token() }}">

     <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
     <!--     Fonts and icons     -->
     <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
     <!-- CSS Files -->
     <link href="/admin_theme/css/bootstrap.min.css" rel="stylesheet" />
     <link href="/admin_theme/css/light-bootstrap-dashboard.css?v=2.0.0 " rel="stylesheet" />
     <!-- CSS Just for demo purpose, don't include it in your project -->
     <link href="/admin_theme/css/demo.css" rel="stylesheet" />

     <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.20/datatables.min.css"/>
     <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/clockpicker/0.0.7/jquery-clockpicker.min.css"/>
     <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />

     @stack('style')

</head>
<body>
     <div class="wrapper">
          <div class="sidebar" data-color="" style=" background-color: #001E38" >
               <!--
               Tip 1: You can change the color of the sidebar using: data-color="purple | blue | green | orange | red"

               Tip 2: you can also add an image using data-image tag
          -->
          <div class="sidebar-wrapper">
               <div class="logo">
                    <center>
                    <a href="/" >
                         <img src="/admin_theme/img/logb.png" alt="sistemaintermerk.com.mx" style="width: 90%">
                    </a>
               </center>
                    <!--
                    <a href="/" class="simple-text logo-normal">
                         {{ config('app.name') }}
                    </a>
               -->
               </div>
               <div class="user">

                    <div class="info ">
                         <a data-toggle="collapse" href="#collapseExample" class="collapsed">
                              <span>
                                   {{ auth()->user()->name }}
                              </span>
                         </a>

                    </div>
               </div>
               @include('admin.partials.nav')
          </div>
     </div>
     <div class="main-panel">
          <!-- Navbar -->
          <nav class="navbar navbar-expand-lg ">
               <div class="container-fluid">
                    <div class="navbar-wrapper">
                         <div class="navbar-minimize">
                              <button id="minimizeSidebar" class="btn btn-warning btn-fill btn-round btn-icon d-none d-lg-block">
                                   <i class="fa fa-ellipsis-v visible-on-sidebar-regular"></i>
                                   <i class="fa fa-navicon visible-on-sidebar-mini"></i>
                              </button>
                         </div>
                         <a class="navbar-brand" >  </a>
                    </div>
                    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                         <span class="navbar-toggler-bar burger-lines"></span>
                         <span class="navbar-toggler-bar burger-lines"></span>
                         <span class="navbar-toggler-bar burger-lines"></span>
                    </button>
                    <div class="collapse navbar-collapse justify-content-end">


                         <ul class="navbar-nav">

                              <li class="dropdown nav-item">
                                   @if(auth()->user()->hasRole('Jefe_capturista'))
                                        @include('admin.partials.notification_candidate')
                                   @endif
                              </li>

                              <li class="nav-item dropdown">
                                   <a class="nav-link dropdown-toggle" href="https://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="nc-icon nc-bullet-list-67"></i>
                                   </a>
                                   <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">

                                        <a href="{{ url('/logout') }}" class="dropdown-item text-danger">
                                             <i class="nc-icon nc-button-power"></i> Cerrar sesión
                                        </a>
                                   </div>
                              </li>
                         </ul>
                    </div>
               </div>
          </nav>
          <!-- End Navbar -->
          <div class="content p-0">
               <div class="container-fluid p-0">
                    <div class="row">
                         <div class="col-md-12">
                              @include('admin.partials.flash-message')

                         </div>
                    </div>
                    <div class="row">
                         @yield('content')
                    </div>

               </div>
          </div>
          <footer class="footer">
               <div class="container">
                    <nav>

                         <p class="copyright text-center">
                              ©
                              <script>
                              document.write(new Date().getFullYear())
                              </script>

                         </p>
                    </nav>
               </div>
          </footer>
     </div>
</div>

</body>
<!--   Core JS Files   -->
<script src="/admin_theme/js/core/jquery.3.2.1.min.js" type="text/javascript"></script>
<script src="/admin_theme/js/core/popper.min.js" type="text/javascript"></script>
<script src="/admin_theme/js/core/bootstrap.min.js" type="text/javascript"></script>
<!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
<script src="/admin_theme/js/plugins/bootstrap-switch.js"></script>
<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
<!--  Chartist Plugin  -->
<script src="/admin_theme/js/plugins/chartist.min.js"></script>
<!--  Notifications Plugin    -->
<script src="/admin_theme/js/plugins/bootstrap-notify.js"></script>
<!-- Control Center for Light Bootstrap Dashboard: scripts for the example pages etc -->
<script src="/admin_theme/js/light-bootstrap-dashboard.js?v=2.0.0 " type="text/javascript"></script>

<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.20/datatables.min.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/clockpicker/0.0.7/jquery-clockpicker.js"></script>


<script src="/admin_theme/js/demo.js"></script>

<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>


@stack('scripts')


</html>
