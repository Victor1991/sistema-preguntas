@extends('admin.layout')

@section('content')
<div class="col-md-12">
     <form class="form" method="post" action="{{ url('admin/'.$category_id.'/category') }}">
          {{ csrf_field() }}
          <div class="card">
               <div class="card-header ">
                    <div class="card-header">
                         <h4 class="card-title">Datos categoría</h4>
                    </div>
               </div>
               <div class="card-body ">
                    <div class="row">
                         <div class="col-md-12">
                              @if($errors->any())
                                   <ul class="list-group">
                                        @foreach($errors->all() as $key => $error)
                                             <li class="list-group-item list-group-item-danger"> {{ $error }}</li>
                                        @endforeach
                                   </ul>
                              @endif
                         </div>

                         <div class="col-md-12">
                              <div class="row">
                                   <div class="col-md-9">
                                        <div class="form-group">
                                             <label for="name">Nombre</label>
                                             <input type="text" name="name" class="form-control" value="{{ old('name') }}" placeholder="Nombre">
                                        </div>
                                   </div>


                                   <div class="col-md-3">
                                        <div class="form-group">
                                             <label for="status">Estatus</label><br>
                                             <div class="onoffswitch">
                                                 <input type="checkbox" name="status" value="1"  @if( old('status') == 1 ) checked @endif class="onoffswitch-checkbox" id="myonoffswitch">
                                                 <label class="onoffswitch-label" for="myonoffswitch">
                                                     <span class="onoffswitch-inner"></span>
                                                     <span class="onoffswitch-switch"></span>
                                                 </label>
                                             </div>
                                        </div>
                                   </div>
                              </div>
                         </div>
                         <div class="col-md-12">
                              <hr>
                         </div>
                    </div>

                    <button type="submit" class="btn btn-info btn-fill pull-right">Crear categoría</button>
                    <a href="{{ route('admin.category.index') }}" class="btn btn-default btn-fill pull-left">Cancelar</a>
                    <div class="clearfix"></div>
               </div>
          </div>

     </form>
</div>
@stop
