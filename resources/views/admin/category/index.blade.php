@extends('admin.layout')

@section('content')
<div class="col-md-12">
     <ul class="nav nav-tabs" style="font-size: 12px;">
          @foreach ($categorys as $key => $category)
          <li class="nav-item">
               <a class="nav-link @if($key == 0) active @endif" data-toggle="tab" href="#tab{{$key}}">{{ $category->name }}</a>
          </li>
          @endforeach
     </ul>
     <div class="row">
          <div class="col-md-12">
               <div class="tab-content">
                    @foreach ($categorys as $key => $category)
                    <div class="tab-pane @if($key == 0) active @else fade @endif" id="tab{{$key}}" style="padding-top: 0px;">
                         <div class="card strpied-tabled-with-hover">
                              <div class="card-header ">
                                   <h4 class="card-title"> {{ $category->name }} </h4>
                                   <p class="card-category">Lista de categorías</p>
                                   <a href="{{ url('admin/'.$category->id.'/category/create') }}" class="btn btn-success btn-wd" style="float: right; margin-top: -50px;">
                                        <i class="fa fa-plus"></i> Nuevo
                                   </a>
                              </div>
                              <div class="card-body table-responsive">
                                   <table class="table table-hover table-striped table_js">
                                        <thead>
                                             <tr>
                                                  <th>ID</th>
                                                  <th>Nombre</th>
                                                  <th class="text-center">Estatus</th>
                                                  <th class="text-center">Número de marcas</th>
                                                  <th class="text-center"><i class="nc-icon nc-preferences-circle-rotate"></i></th>
                                             </tr>
                                        </thead>
                                        <tbody>
                                             @foreach ($category->category as $key => $subcat)
                                             <tr>
                                                  <td>{{ $subcat->id }}</td>
                                                  <td>{{ $subcat->name }}</td>
                                                  <td class="text-center">
                                                       @if($subcat->status == 1)
                                                       <span class="badge badge-success">Activo</span>
                                                       @else
                                                       <span class="badge badge-danger">Inactivo</span>
                                                       @endif
                                                  </td>
                                                  <td class="text-center">{{ $subcat->brands->count() }}</td>
                                                  <td class="text-center">
                                                       <a href="{{ route('brand.index', ['category_id' => $subcat->id ]) }}"
                                                            class="btn btn-sm btn-default btn-outline"
                                                            >
                                                            <i class="fa fa-cubes" aria-hidden="true"></i> Marcas
                                                       </a>

                                                       <a href="{{ route('category.edit' , ['category_id' => $category->id, 'category' => $subcat]) }}"
                                                            class="btn btn-sm btn-info btn-outline"
                                                            >
                                                            <i class="fa fa-edit" aria-hidden="true"></i> Editar
                                                       </a>
                                                  </td>
                                             </tr>
                                             @endforeach
                                        </tbody>
                                   </table>
                              </div>
                         </div>
                    </div>
                    @endforeach
               </div>
          </div>
     </div>
</div>

@stop
