@extends('admin.layout')

@section('content')
<div class="col-md-6">
     <div class="card card-user">
          <div class="card-header no-padding">
               <div class="card-image" style="background-color: #004b8d;">
                    <!-- <img src="/img/background-perfil.jpg" alt="..."> -->
               </div>
          </div>
          <div class="card-body ">
               <div class="author">
                    <a href="#">
                         <img class="avatar border-gray" src="https://us.123rf.com/450wm/tuktukdesign/tuktukdesign1608/tuktukdesign160800042/61010829-icono-de-usuario-hombre-perfil-hombre-de-negocios-avatar-ilustraci%C3%B3n-vectorial-persona-glifo.jpg?ver=6" alt="...">
                    </a>
                    <p class="card-description">
                         <h6 class="card-title">Número identificador</h6>
                         {{ $leader->id }}
                         <hr>
                         <h6 class="card-title">Nombre completo</h6>
                         {{ $leader->name }}
                         <hr>
                         <h6 class="card-title">Correo</h6>
                         {{ $leader->email }}
                         <hr>

                         <h6 class="card-title">Estatus</h6>
                         @if($leader->status == 1)
                              <h6><span class="badge badge-success">Activo</span></h6>
                         @else
                              <h6><span class="badge badge-danger">Inactivo</span></h6>
                         @endif

                         <hr>

                         <h6 class="card-title">Fecha registro</h6>
                         {{ $leader->created_at }}
                    </p>
               </div>
          </div>

     </div>
</div>
<div class="col-md-6">
     <div class="card card-user">
          <div class="card-header">
               <h4 class="card-title">Permiso adicionales</h4>
               <p class="card-category">permisos extras asignados </p>
          </div>
          <div class="card-body ">
                    <p class="card-description">
                         @forelse ($leader->permissions as $permission)
                              <h6 class="card-title">{{ $permission->name }}</h6>
                              @unless($loop>last)
                                   <hr>
                              @endunless
                         @empty
                              <h6 class="card-title">No tiene permisos adicionales</h6>
                         @endforelse
                    </p>
          </div>

     </div>
</div>
@stop
