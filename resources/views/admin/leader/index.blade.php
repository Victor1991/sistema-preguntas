@extends('admin.layout')

@section('content')
<div class="col-md-12">
     <div class="card strpied-tabled-with-hover">
          <div class="card-header ">
               <h4 class="card-title">Jefes Capturistas</h4>
               <p class="card-category">Lista de jefes capturistas</p>
               <a href="{{ route('admin.leader.create') }}" class="btn btn-success btn-wd" style="float: right; margin-top: -50px;">
                    <i class="fa fa-plus"></i> Nuevo
               </a>
          </div>
          <div class="card-body table-responsive">
               <table class="table table-hover table-striped table_js">
                    <thead>
                         <tr>
                              <th>ID</th>
                              <th>Nombre</th>
                              <th>Correo</th>
                              <th class="text-center">Estatus</th>
                              <th class="text-center"><i class="nc-icon nc-preferences-circle-rotate"></i></th>
                         </tr>
                    </thead>
                    <tbody>
                         @foreach ($users as $leader)
                         <tr>
                              <td>{{ $leader->id }}</td>
                              <td>{{ $leader->name }}</td>
                              <td>{{ $leader->email }}</td>
                              <td class="text-center">
                                   @if($leader->status == 1)
                                        <h6><span class="badge badge-success">Activo</span></h6>
                                   @else
                                        <h6><span class="badge badge-danger">Inactivo</span></h6>
                                   @endif
                              </td>
                              <td class="text-center">
                                   @can('Ver Jefe Capturista')
                                   <a href="{{ route('admin.leader.show', $leader) }}"
                                        class="btn btn-sm btn-default btn-outline"
                                   > <i class="fa fa-eye"></i> </a>
                                   @endcan
                                   @can('Editar Jefe Capturista')
                                   <a href="{{ route('admin.leader.edit', $leader) }}"
                                        class="btn btn-sm btn-info btn-outline"
                                   > <i class="fa fa-pencil"></i> </a>
                                   @endcan
                                   @can('Eliminar Jefe Capturista')
                                   <form method="POST"
                                        action="{{ route('admin.leader.destroy', $leader) }}"
                                        style="display:inline">
                                        {{ csrf_field() }} {{ method_field('DELETE') }}
                                        <button class="btn btn-sm btn-danger btn-outline"
                                             onclick="return confirm('¿ Estás seguro de querer eliminar este Jefe de Capturistas ?')"
                                         >
                                         <i class="fa fa-trash"></i>
                                        </button>
                                   </form>
                                    @endcan
                              </td>
                         </tr>
                         @endforeach
                    </tbody>
               </table>
          </div>
     </div>
</div>
@stop
