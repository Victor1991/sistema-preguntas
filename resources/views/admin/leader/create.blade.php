@extends('admin.layout')

@section('content')

@if(auth()->user()->can('Editar roles'))
<div class="col-md-12">
@else
<div class="col-md-6 ml-auto mr-auto">
@endif
<div class="col-md-12">
     <form class="form" method="post" action="{{ route('admin.leader.store') }}">
          {{ csrf_field() }}

          <div class="card">
               <div class="card-header ">
                    <div class="card-header">
                         <h4 class="card-title">Datos personales</h4>
                    </div>
               </div>
               <div class="card-body ">
                    <div class="row">
                         <div class="col-md-12">
                              @if($errors->any())
                                   <ul class="list-group">
                                        @foreach($errors->all() as $key => $error)
                                             <li class="list-group-item list-group-item-danger"> {{ $error }}</li>
                                        @endforeach
                                   </ul>
                              @endif
                         </div>
                         @if(auth()->user()->can('Editar roles'))
                         <div class="col-md-6">
                         @else
                         <div class="col-md-12">
                         @endif
                              <div class="row">
                                   <div class="col-md-12">
                                        <div class="form-group">
                                             <label for="name">Nombre</label>
                                             <input type="text" name="name" class="form-control" value="{{ old('name') }}" placeholder="Nombre">
                                        </div>
                                   </div>
                              </div>
                              <div class="row">
                                   <div class="col-md-12">
                                        <div class="form-group">
                                             <label for="email">Correo</label>
                                             <input type="email" name="email" class="form-control" value="{{ old('email') }}" placeholder="Correo">
                                        </div>
                                   </div>
                              </div>
                              <div class="row">
                                   <div class="col-md-12">
                                        <div class="form-group">
                                             <label for="status">Estatus</label><br>
                                             <div class="onoffswitch">
                                                 <input type="checkbox" name="status" value="1"  @if( old('status') == 1 ) checked @endif class="onoffswitch-checkbox" id="myonoffswitch">
                                                 <label class="onoffswitch-label" for="myonoffswitch">
                                                     <span class="onoffswitch-inner"></span>
                                                     <span class="onoffswitch-switch"></span>
                                                 </label>
                                             </div>
                                        </div>
                                   </div>
                              </div>
                         </div>
                         @can('Editar roles')
                              <div class="col-md-6">
                                   @include('admin.permissions-checkboxes')
                              </div>
                         @endcan
                         <div class="col-md-12">
                              <hr>
                         </div>
                         <div class="col-md-12">
                              <span class="help-block"> La contraseña será generada y enviada al nuevo usuario vía correo.</span>
                         </div>
                         <div class="col-md-12">
                              <hr>
                         </div>
                    </div>

                    <button type="submit" class="btn btn-info btn-fill pull-right">Crear usuario</button>
                    <a href="{{ route('admin.leader.index') }}" class="btn btn-default btn-fill pull-left">Cancelar</a>
                    <div class="clearfix"></div>
               </div>
          </div>

     </form>
</div>
@stop
