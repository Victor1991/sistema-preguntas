@extends('admin.layout')

@section('content')
@if(auth()->user()->can('Editar roles'))
<div class="col-md-12">
@else
<div class="col-md-12 ml-auto mr-auto">
@endif
     <form class="form" method="post" action="{{ route('admin.study.store') }}">
          {{ csrf_field() }}

          <div class="card">
               <div class="card-header ">
                    <div class="card-header">
                         <h4 class="card-title">Datos del estudio</h4>
                    </div>
               </div>
               <div class="card-body ">
                    <div class="row">
                         <div class="col-md-12">
                              @if($errors->any())
                                   <ul class="list-group">
                                        @foreach($errors->all() as $key => $error)
                                             <li class="list-group-item list-group-item-danger"> {{ $error }}</li>
                                        @endforeach
                                   </ul>
                              @endif
                         </div>
                         <div class="col-md-12">
                              <div class="row">
                                   <div class="col-md-8">
                                        <div class="form-group">
                                             <label for="name">Nombre</label>
                                             <input type="text" name="name" class="form-control" value="{{ old('name') }}" placeholder="Nombre">
                                        </div>
                                   </div>
                                   <div class="col-md-4">
                                        <div class="form-group">
                                             <label for="email">Fecha estudio</label>
                                             <input type="text" name="study_date" autocomplete="off" class="form-control datetimepicker" value="{{ old('study_date') }}" placeholder="Fecha estudio">
                                        </div>
                                   </div>

                              </div>
                              <div class="row">
                                   <div class="col-md-8">
                                        <div class="form-group">
                                             <label for="email">Cliente / Empresa</label>
                                             <select class="form-control" name="company_id"  >
                                                  <option value="" hidden> -- Seleccione una opción</option>
                                                  @foreach ($companys as $key => $company)
                                                       <option value="{{ $company->id }}">{{ $company->name }}</option>
                                                  @endforeach
                                             </select>
                                        </div>
                                   </div>
                              </div>
                              <div class="row">
                                   <div class="col-md-8">
                                        <h5>Requerimientos del cliente</h5>
                                   </div>
                              </div>
                              <div class="row">
                                   <div class="col-md-6">
                                        <div class="form-group">
                                             <label for="email">Número de participantes</label>
                                             <input type="number" name="number_participants" class="form-control" value="{{ old('name') }}" placeholder="Número de participantes">
                                        </div>
                                   </div>
                              </div>
                         </div>
                    </div>

                    <button type="submit" class="btn btn-info btn-fill pull-right">Crear estudio</button>
                    <a href="{{ route('admin.study.index') }}" class="btn btn-default btn-fill pull-left">Cancelar</a>
                    <div class="clearfix"></div>
               </div>
          </div>

     </form>
</div>
@stop

@push('style')
<link href="https://cdnjs.cloudflare.com/ajax/libs/multi-select/0.9.12/css/multi-select.min.css" rel="stylesheet" type="text/css" />
<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" rel="stylesheet" type="text/css" />
<style media="screen">
input.form-control.datetimepicker.gj-textbox-md{
     border: 1px solid #e3e3e3;
     padding-left: 10px;
}
input.form-control.datetimepicker2.gj-textbox-md{
     border: 1px solid #e3e3e3;
     padding-left: 10px;
}
i.gj-icon{
     margin-top: 8px;
     margin-right: 5px;
}
div#ms-optgroup{
     width: 100%;
}
</style>
@endpush

@push('scripts')
<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/multi-select/0.9.12/js/jquery.multi-select.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js" type="text/javascript"></script>

<script>
$( document ).ready(function() {

     $('.datetimepicker').datepicker({
          format: 'yyyy-mm-dd'
     });

});
</script>
@endpush
