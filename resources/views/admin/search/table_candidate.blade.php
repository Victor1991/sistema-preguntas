<div class="col-md-12">
     <div class="card strpied-tabled-with-hover">
          <div class="card-header ">
               <h4 class="card-title">Participantes</h4>
               <p class="card-category">Lista de participantes</p>
          </div>
          <div class="card-body table-responsive">
               <table class="table table-hover table-striped ">
                    <thead>
                         <tr>
                              <th>ID</th>
                              <th>Nombre</th>
                              <th>Correo</th>
                              <th>Estatus</th>
                              <th class="text-center"><i class="nc-icon nc-preferences-circle-rotate"></i></th>
                         </tr>
                    </thead>
                    <tbody id="tbody_participantes">
                         <tr>
                              <th colspan="5" style="text-align: center;">No se encontraron participantes con estas características</th>
                         </tr>
                    </tbody>
               </table>
          </div>
     </div>
</div>

@push('scripts')

<script>

function filtrar_participantes() {
     var form = $("#new_search").serialize();
     $.ajax({
          url: "{{ route('admin.filter_participants') }}",
          data: form,
          type:'GET',
          success:  function (response) {
                    $( "#tbody_participantes" ).html('');
               if (response.length > 0) {
                    $.each(response, function( index, value ) {

                         $( "#tbody_participantes" ).append('<tr>'+
                                   '<td>'+value.id +'</td>'+
                                   '<td>'+concat_name(value.name, value.last_name_paternal ,value.last_name_maternal) +'</td>'+
                                   '<td>'+value.email +'</td>'+
                                   '<td>'+obtener_estatus(value.status)+'</td>'+
                                   '<td> <a class="btn btn-info btn-sm" target="_black" href="<?=url('/')?>/admin/candidate/'+value.id+'" ><i class="fa fa-eye"></i> </a></td>'+
                              '</tr>');
                    });
               }else {
                    $( "#tbody_participantes" ).append('<th colspan="5" style="text-align: center;">No se encontraron participantes con estas características</th>');
               }
          },
          statusCode: {
               404: function() {
                    alert('web not found');
               }
          },
          error:function(x,xs,xt){
               window.open(JSON.stringify(x));
               //alert('error: ' + JSON.stringify(x) +"\n error string: "+ xs + "\n error throwed: " + xt);
          }
     });
}

function concat_name($name = '', $last_name_paternal = '', $last_name_maternal = '' ) {
     $n_name = '';
     if ($name != '' && $name != null) {
          $n_name += $name;
     }
     if ($last_name_paternal != '' && $last_name_paternal != null) {
          $n_name += ' '+$last_name_paternal;
     }
     if ($last_name_maternal != '' && $last_name_maternal != null) {
          $n_name += ' '+$last_name_maternal;
     }
     return $n_name;
}

function obtener_estatus($status) {
     if ($status == 1) {
          return '<h6><span class="badge badge-success">Activo</span></h6>';
     }else{
          return '<h6><span class="badge badge-danger">Inactivo</span></h6>';
    }
}

</script>

@endpush
