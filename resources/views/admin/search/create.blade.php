@extends('admin.layout')

@section('content')
<div class="col-md-12">
     <div class="card strpied-tabled-with-hover">
          <div class="card-header ">
               <h4 class="card-title">Búsqueda </h4>
               <p class="card-category">Lista de participantes</p>
          </div>
          <div class="card-body">
               <form action="{{ route('admin.search.store') }}" id="new_search" method="post">
                    {{ csrf_field() }}

                    <input type="" hidden name="name" id="name" value="">
                    <div class="col-md-12">
                         <div class="row">
                              <div class="col-md-12 text-danger">
                                   <h6>Búsqueda de participantes</h6>
                                   <hr style="border-color:red;">
                              </div>
                              <div class="col-md-4">
                                   <div class="form-group">
                                        <label for="facebook">RANGO DE EDAD</label>
                                        <div class="row">
                                             <div class="col-md-6 col-sm-6" style="padding: 0px 15px 0px 15px;">
                                                  <input type="text" class="form-control" name="range1" placeholder="Min" value="">
                                             </div>
                                             <div class="col-md-6 col-sm-6" style="padding: 0px 15px 0px 15px;">
                                                  <input type="text" class="form-control" name="range2" placeholder="Max" value="">
                                             </div>
                                        </div>
                                        {{-- <input type="text" class="js-range-slider" name="range" value="" /> --}}
                                   </div>
                              </div>
                              <div class="col-md-4">
                                   <div class="form-group">
                                        <label for="facebook">AMA DE CASA</label>
                                        <select class="form-control" name="housewife" >
                                             <option value="" > -- Seleccione una opción</option>
                                             <option value="1">Si</option>
                                             <option value="2">No</option>
                                        </select>
                                   </div>
                              </div>
                              <div class="col-md-4">
                                   <div class="form-group">
                                        <label for="facebook">¿TRABAJA FUERA DE CASA?</label>
                                        <select class="form-control" name="out_of_home" >
                                             <option value="" > -- Seleccione una opción</option>
                                             <option value="1">Si</option>
                                             <option value="2">No</option>
                                        </select>
                                   </div>
                              </div>
                         </div>
                    </div>
                    <div class="col-md-12">
                         <hr>
                    </div>
                    <div class="col-md-12">
                         <div class="row">
                              <div class="col-md-12 text-danger">
                                   <h6>Dirección de busqueda</h6>
                                   <hr style="border-color:red;">
                              </div>
                              <div class="col-md-12">
                                   <div class="form-group">
                                        <label for="facebook">ESTADOS</label>
                                        <select class="form-control js-example-basic-single" multiple="multiple" name="estado_id[]" id="estado_id" onchange="get_municipios()" >
                                             <option value="" > -- Seleccione una opción</option>
                                             @foreach ($estados as $key => $estado)
                                             <option value="{{ $estado->id }}">{{ $estado->nombre }}</option>
                                             @endforeach
                                        </select>
                                   </div>
                              </div>
                              <div class="col-md-12">
                                   <div class="form-group">
                                        <label for="facebook">ALCALDÍA / MUNICIPIO</label>
                                        <select class="form-control js-example-basic-single" multiple="multiple" name="municipio_id[]" id="municipio_id" onchange="get_localidad()" >
                                             <option value="" > -- Seleccione una opción</option>
                                        </select>
                                   </div>
                              </div>
                              <div class="col-md-12">
                                   <div class="form-group">
                                        <label for="facebook">COLONIA</label>
                                        <select class="form-control js-example-basic-single" multiple="multiple" name="localidad_id[]" id="localidad_id" >
                                             <option value="" > -- Seleccione una opción</option>
                                        </select>
                                   </div>
                              </div>
                         </div>
                    </div>
                    <div class="col-md-12">
                         <hr>
                    </div>
                    <div class="col-md-12">
                         <div class="row">
                              <div class="col-md-4">

                              </div>
                              <div class="col-md-4">
                                   <div class="form-group">
                                        <label for="facebook">ÚLTIMA PARTICIPACIÓN </label>
                                        <select class="form-control" name="last_participation" id="last_participation">
                                             <option value="" > -- Seleccione una opción</option>
                                             <?php for ($i=1; $i <= 12 ; $i++) { ?>
                                                  <option value="{{$i}}" > {{ $i }}
                                                       <?php if ($i > 1): ?>
                                                            meses
                                                       <?php else: ?>
                                                            mes
                                                       <?php endif; ?>
                                                  </option>
                                             <?php } ?>
                                        </select>
                                   </div>
                              </div>
                              <div class="col-md-4">

                              </div>
                         </div>
                    </div>
                    <div class="col-md-12">
                         <hr>
                    </div>
                    <div class="col-md-12">
                         <div class="row">
                              <div class="col-md-12 text-danger">
                                   <h6>MARCAS QUE USA ACTUALMENTE</h6>
                                   <hr style="border-color:red;">
                              </div>
                              <div class="col-md-12">
                                   <label style="font-size: 11px;">CATEGORIAS</label>
                                   <select name="cateogira_id[]" class="form-control js-example-basic-single" multiple="multiple">
                                        <option value="">-- Seleciona una opción --</option>
                                        @foreach ($categorys as $key => $category)
                                             @foreach ($category->category as $key => $cat)
                                                  <option 
                                                       value="{{ $cat->id }}"
                                                       {{ collect(old('cateogira_id[]'))->contains($cat->id) ? 'selected':'' }}
                                                       >{{ $cat->name }}</option>
                                             @endforeach
                                        @endforeach
                                   </select>
                                   <br>
                              </div>
                              @foreach ($categorys as $key => $category)
                              <div class="col-md-4">
                                   <div class="form-group">
                                        <label style="font-size: 11px;">{{ $category->name }}</label>
                                        <select class="form-control js-example-basic-single" multiple="multiple" name="cat{{ $category->id }}_new[]">
                                             <option value="">-- Seleciona una opción --</option>
                                             @foreach ($category->category as $key => $cat)
                                             <optgroup label='{{$cat->name}}'>
                                                  @foreach ($cat->brands as $brands)
                                                  <option
                                                  value="{{ $brands->id }}"
                                                  {{ collect(old('brands[]'))->contains($brands->id) ? 'selected':'' }}
                                                  >
                                                  {{ $brands->name }}
                                             </option>
                                             @endforeach
                                        </optgroup>
                                        @endforeach
                                   </select>
                              </div>
                         </div>
                         @endforeach
                    </div>
               </div>
               <div class="col-md-12">
                    <hr>
               </div>
               <div class="col-md-12">
                    <div class="row">
                         <div class="col-md-12 text-danger">
                              <h6>MARCAS QUE USABA ANTERIORMENTE</h6>
                              <hr style="border-color:red;">
                         </div>
                         @foreach ($categorys as $key => $category)
                         <div class="col-md-4">
                              <div class="form-group">
                                   <label style="font-size: 11px;">{{ $category->name }}</label>
                                   <select class="form-control js-example-basic-single" name="cat{{ $category->id }}_old[]" multiple="multiple">
                                        <option>-- Seleciona una opción --</option>
                                        @foreach ($category->category as $key => $cat)
                                        <optgroup label='{{$cat->name}}'>
                                             @foreach ($cat->brands as $brands)
                                             <option
                                             value="{{ $brands->id }}"
                                             {{ collect(old('brands[]'))->contains($brands->id) ? 'selected':'' }}
                                             >
                                             {{ $brands->name }}
                                        </option>
                                        @endforeach
                                   </optgroup>
                                   @endforeach
                              </select>
                         </div>
                    </div>
                    @endforeach
               </div>
          </div>
          <div class="col-md-12">
               <hr>
          </div>
          <div class="col-md-12">
               <button type="button"  data-toggle="modal" data-target="#moda_guardar_busqueda" class="btn btn-success btn-fill pull-right">Guardar</button>
               <button type="button" style="margin-right: 5px;" class="btn btn-info btn-fill pull-right" onclick="filtrar_participantes()">Buscar</button>

               <a href="{{  route('admin.search.index')  }}"  class="btn btn-default btn-fill pull-left">Regresar</a>
               <div class="clearfix"></div>
          </div>
     </form>
</div>
</div>
</div>



@include('admin.search.table_candidate')


<div class="modal fade modal-mini modal-primary " id="moda_guardar_busqueda" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
     <div class="modal-dialog">
          <div class="modal-content">
               <div class="modal-body text-center">
                    <p> Nombre de la búsqueda : </p>
                    <input type="text" class="form-control" id="name_busqueda" placeholder="Nombre de la busqueda">
               </div>
               <div class="modal-footer">
                    <div class="col-md-12">
                         <button type="button" class="btn btn-default btn-simple" data-dismiss="modal">Cancelar</button>
                         <button type="button" onclick="guardar()" class="btn btn-primary">Enviar</button>
                    </div>
               </div>
          </div>
     </div>
</div>
@stop


@push('style')
<link href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" rel="stylesheet" type="text/css" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.1/css/ion.rangeSlider.min.css" rel="stylesheet" type="text/css" />
<style>
     span.select2.select2-container.select2-container--default.select2-container--focus{
          width: 100% !important;
     }
</style>
@endpush

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.1/js/ion.rangeSlider.min.js" type="text/javascript"></script>
<script>
$( document ).ready(function() {
     $(document).ready(function() {
          $('.js-example-basic-single').select2();
     });

     $(".js-range-slider").ionRangeSlider({
          type: "double",
          min: {{ $edadmin }},
          max: {{ $edadmax }},
          from: {{ $edadmin }},
          to: {{ $edadmax }},
     });
});


function get_municipios(){
     $('#municipio_id').html('');
     $.ajax({
          url: "{{ route('admin.municipios') }}",
          data:{'estado_id': $('#estado_id').val()},
          type:'get',
          success:  function (response) {
               $('#municipio_id').html();
               $.each(response, function (i, item) {
                    $('#municipio_id').append($('<option>', {
                         value: item.id,
                         text : item.nombre
                    }));
               });
          },
          statusCode: {
               404: function() {
                    alert('web not found');
               }
          },
          error:function(x,xs,xt){
               //alert('error: ' + JSON.stringify(x) +"\n error string: "+ xs + "\n error throwed: " + xt);
          }
     });
}

function get_localidad(){
     $('#localidad_id').html('');
     $.ajax({
          url: "{{ route('admin.localidades') }}",
          data:{'municipio_id': $('#municipio_id').val()},
          type:'get',
          success:  function (response) {
               $('#localidad_id').html();
               $.each(response, function (i, item) {
                    $('#localidad_id').append($('<option>', {
                         value: item.id,
                         text : item.nombre
                    }));
               });
          },
          statusCode: {
               404: function() {
                    alert('web not found');
               }
          },
          error:function(x,xs,xt){
               window.open(JSON.stringify(x));
               //alert('error: ' + JSON.stringify(x) +"\n error string: "+ xs + "\n error throwed: " + xt);
          }
     });
}

function guardar() {
     $('#name').val($( "#name_busqueda" ).val());
     if ($('#name_busqueda').val() != '') {
          $( "#new_search" ).submit();
     }else{
          swal("Nombre requerido", "el nombre es requerido", "warning");
     }
}




</script>

@endpush
