@extends('admin.layout')

@section('content')
<div class="col-md-12">
     <div class="card strpied-tabled-with-hover">
          <div class="card-header ">
               <h4 class="card-title">Búsquedas</h4>
               <p class="card-category">Lista de búsquedas</p>
               <a href="{{ route('admin.search.create') }}" class="btn btn-success btn-wd" style="float: right; margin-top: -50px;">
                    <i class="fa fa-plus"></i> Nuevo
               </a>
          </div>
          <div class="card-body table-responsive">
               <table class="table table-hover table-striped table_js">
                    <thead>
                         <tr>
                              <th>ID</th>
                              <th>Nombre</th>
                              <th class="text-center">Registrado por</th>
                              <th class="text-center">Creado</th>
                              <th class="text-center"><i class="nc-icon nc-preferences-circle-rotate"></i></th>
                         </tr>
                    </thead>
                    <tbody>
                         @foreach ($searchs as $key => $search)
                         <tr>
                              <td>{{ $search->id }}</td>
                              <td>{{ $search->name }}</td>
                              <td  class="text-center"> @if(isset($search->created_by->name)) {{ $search->created_by->name }} @endif</td>
                              <td  class="text-center"> {{ date("Y-m-d", strtotime( $search->created_at)) }}</td>
                              <td class="text-center">
                                   @can('Ver Administradores')
                                   <a href="{{ route('admin.export_search', $search) }}"
                                        class="btn btn-sm btn-success btn-outline"
                                   > <i class="fa fa-file-excel-o" aria-hidden="true"></i> </a>
                                   @endcan

                                   <a href="{{ route('admin.search.edit', $search) }}"
                                        class="btn btn-sm btn-info btn-outline"
                                   > <i class="fa fa-pencil"></i> </a>

                                   <form method="POST"
                                        action="{{ route('admin.search.destroy', $search) }}"
                                        style="display:inline">
                                        {{ csrf_field() }} {{ method_field('DELETE') }}
                                        <button class="btn btn-sm btn-danger btn-outline"
                                             onclick="return confirm('¿ Estás seguro de querer eliminar esta busqueda ?')"
                                         >
                                         <i class="fa fa-trash"></i>
                                        </button>
                                   </form>

                              </td>
                         </tr>
                         @endforeach
                    </tbody>
               </table>
          </div>
     </div>
</div>

@stop

@push('style')

@endpush


<!-- Escripts pasarlos al layout -->
@push('scripts')

@endpush
