@extends('admin.layout')

@section('content')
@if(auth()->user()->can('Editar roles'))
<div class="col-md-12">
@else
<div class="col-md-12 ml-auto mr-auto">
@endif
     <form class="form" method="post" action="{{ route('admin.company.store') }}">
          {{ csrf_field() }}

          <div class="card">
               <div class="card-header ">
                    <div class="card-header">
                         <h4 class="card-title">Datos cliente / empresa</h4>
                    </div>
               </div>
               <div class="card-body ">
                    <div class="row">
                         <div class="col-md-12">
                              @if($errors->any())
                                   <ul class="list-group">
                                        @foreach($errors->all() as $key => $error)
                                             <li class="list-group-item list-group-item-danger"> {{ $error }}</li>
                                        @endforeach
                                   </ul>
                              @endif
                         </div>
                         <div class="col-md-12">
                              <div class="row">
                                   <div class="col-md-12">
                                        <div class="form-group">
                                             <label for="name">Nombre</label>
                                             <input type="text" name="name" class="form-control" value="{{ old('name') }}" placeholder="Nombre">
                                        </div>
                                   </div>

                              </div>
                              <div class="row">
                                   <div class="col-md-12">
                                        <div class="form-group">
                                             <label for="email">dirección</label>
                                             <textarea name="direction" style="height:120px;"  class="form-control" value="{{ old('direction') }}"></textarea>
                                        </div>
                                   </div>
                              </div>
                         </div>
                    </div>

                    <button type="submit" class="btn btn-info btn-fill pull-right">Crear cliente / empresa</button>
                    <a href="{{ route('admin.company.index') }}" class="btn btn-default btn-fill pull-left">Cancelar</a>
                    <div class="clearfix"></div>
               </div>
          </div>

     </form>
</div>
@stop
