@extends('admin.layout')

@section('content')
<div class="col-md-12">
     <div class="card strpied-tabled-with-hover">
          <div class="card-header ">
               <h4 class="card-title">Buscador </h4>
               <p class="card-category">Lista de participantes</p>
          </div>
          <div class="card-body table-responsive">

               <div class="col-md-12">
                    <div class="row">
                         <div class="col-md-4">

                         </div>
                         <div class="col-md-4">
                              <div class="form-group">
                                   <label for="facebook">AMA DE CASA</label>
                                   <select class="form-control" name="housewife" >
                                        <option value="" hidden> -- Seleccione una opción</option>
                                        <option value="1">Si</option>
                                        <option value="2">No</option>
                                   </select>
                              </div>
                         </div>
                         <div class="col-md-4">
                              <div class="form-group">
                                   <label for="facebook">¿ TRABAJA FUERA DE CASA ? </label>
                                   <select class="form-control" name="out_of_home" >
                                        <option value="" hidden> -- Seleccione una opción</option>
                                        <option value="1">Si</option>
                                        <option value="2">No</option>
                                   </select>
                              </div>
                         </div>
                    </div>
               </div>
               <div class="col-md-12">
                    <hr>
               </div>
               <div class="col-md-12">
                    <div class="row">
                         <div class="col-md-3">
                              <div class="form-group">
                                   <label for="facebook">ESTADOS</label>
                                   <select class="form-control" name="estado_id" id="estado_id" onchange="get_municipios()" >
                                        <option value="" hidden> -- Seleccione una opción</option>
                                        @foreach ($estados as $key => $estado)
                                             <option value="{{ $estado->id }}">{{ $estado->nombre }}</option>
                                        @endforeach
                                   </select>
                              </div>
                         </div>
                         <div class="col-md-3">
                              <div class="form-group">
                                   <label for="facebook">Alcaldía / Municipio</label>
                                   <select class="form-control" name="municipio_id" id="municipio_id" onchange="get_localidad()" >
                                        <option value="" hidden> -- Seleccione una opción</option>
                                   </select>
                              </div>
                         </div>
                         <div class="col-md-3">
                              <div class="form-group">
                                   <label for="facebook">COLONIA</label>
                                   <select class="form-control" name="localidad_id" id="localidad_id" >
                                        <option value="" hidden> -- Seleccione una opción</option>
                                   </select>
                              </div>
                         </div>
                         <div class="col-md-3">
                              <div class="form-group">
                                   <label for="facebook">Codigo Postal</label>
                                   <input type="text" name="cp" class="form-control" value="{{ old('cp') }}" placeholder="Celular">
                              </div>
                         </div>
                    </div>
               </div>
               <div class="col-md-12">
                    <hr>
               </div>
               <div class="col-md-12">
                    <div class="row">
                         <div class="col-md-4">

                         </div>
                         <div class="col-md-4">
                              <div class="form-group">
                                   <label for="facebook">ÚLTIMA PARTICIPACIÓN </label>
                                   <select class="form-control" name="municipio_id" id="municipio_id">
                                        <option value="" hidden> -- Seleccione una opción</option>
                                        <option value="{1" >  1 DIA </option>
                                        <?php for ($i=2; $i <= 30 ; $i++) { ?>
                                             <option value="{{$i}}" > {{ $i }} DIAS </option>
                                        <?php } ?>
                                   </select>
                              </div>
                         </div>
                         <div class="col-md-4">

                         </div>
                    </div>
               </div>
               <div class="col-md-12">
                    <hr>
               </div>
               <div class="col-md-12">
                    <div class="row">
                         <div class="col-md-12 text-danger">
                              <h6>MARCAS QUE USA ACTUALMENTE</h6>
                              <hr style="border-color:red;">
                         </div>
                         <div class="col-md-4">
                              <div class="form-group">
                                   <label for="facebook">PRODUCTOS DE LIMPIEZA</label>
                                   <input type="text" name="facebook" class="form-control" value="" placeholder="">
                              </div>
                         </div>
                         <div class="col-md-4">
                              <div class="form-group">
                                   <label for="facebook">PRODUCTOS DE LAVADO</label>
                                   <input type="text" name="facebook" class="form-control" value="" placeholder="">
                              </div>
                         </div>
                         <div class="col-md-4">
                              <div class="form-group">
                                   <label for="facebook">PRODUCTOS DE USO PERSONAL</label>
                                   <input type="text" name="facebook" class="form-control" value="" placeholder="">
                              </div>
                         </div>
                    </div>
               </div>
               <div class="col-md-12">
                    <hr>
               </div>
               <div class="col-md-12">
                    <div class="row">
                         <div class="col-md-12 text-danger">
                              <h6>MARCAS QUE USABA ANTERIORMENTE</h6>
                              <hr style="border-color:red;">
                         </div>
                         <div class="col-md-4">
                              <div class="form-group">
                                   <label for="facebook">PRODUCTOS DE LIMPIEZA</label>
                                   <input type="text" name="facebook" class="form-control" value="" placeholder="">
                              </div>
                         </div>
                         <div class="col-md-4">
                              <div class="form-group">
                                   <label for="facebook">PRODUCTOS DE LAVADO</label>
                                   <input type="text" name="facebook" class="form-control" value="" placeholder="">
                              </div>
                         </div>
                         <div class="col-md-4">
                              <div class="form-group">
                                   <label for="facebook">PRODUCTOS DE USO PERSONAL</label>
                                   <input type="text" name="facebook" class="form-control" value="" placeholder="">
                              </div>
                         </div>
                    </div>
               </div>
               <div class="col-md-12">
                    <hr>
               </div>

          </div>
     </div>
</div>
@stop
