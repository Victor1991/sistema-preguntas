
<h4 class="card-title">Estudios</h4>
<p class="card-category">Lista de estudios del participantes</p>
<a href="javascript:void(0)" id="registrer-study" class="btn btn-wd btn-success" style="float: right; margin-top: -50px;">
     <i class="fa fa-plus"></i> Asignar estudio
</a>

<br>
<table class="table table-hover table-striped table_js">
     <thead>
          <tr>
               <th class="text-center">ID</th>
               <th class="text-center">Nombre estudio</th>
               <th class="text-center">Fecha de asignación</th>
               <th class="text-center"><i class="nc-icon nc-preferences-circle-rotate"></i></th>
          </tr>
     </thead>
     <tbody>
          @foreach($candidate->studys as $key => $studys)
          <tr>
               <td class="text-center">{{$studys->pivot->id }}</td>
               <td class="text-center">{{$studys->name }}</td>
               <td class="text-center">{{ \Carbon\Carbon::parse($studys->created_at)->diffForHumans() }}</td>
               <td class="td-actions text-center">
                    <a href="javascript:void(0)" id="group-user" data-id="{{ $studys->pivot->id }}" class="btn btn-sm btn-info btn-outline">
                         <i class="fa fa-pencil"></i>
                    </a>
                    <form method="POST"
                    action="{{ route('admin.destroy_assigned_study', $studys->pivot->id) }}"
                    style="display:inline" class="eliminar">
                    {{ csrf_field() }} {{ method_field('DELETE') }}
                    <button class="btn btn-sm btn-danger btn-outline" type="submit"   onclick="return confirm('¿ Estás seguro de querer eliminar este estudio ?')">
                         <i class="fa fa-trash" style="font-size: 15px;"></i>
                    </button>
               </form>


          </td>
     </tr>
     @endforeach
</tbody>
</table>

<div class="modal fade" id="ajax-crud-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
     <div class="modal-dialog" role="document">
          <div class="modal-content">
               <div class="modal-header">
                    <h5 class="modal-title" id="groupCrudModal"></h5>
               </div>
               <form id="groupForm" name="groupForm" enctype="multipart/form-data">
                    <input type="hidden" name="assigned_study_id" id="assigned_study_id" value="">
                    <input type="hidden" name="user_id" id="user_id_study" value="{{ $candidate->id }}">
                    <div class="modal-body">
                         <div class="row">
                              <div class="col-md-12">
                                   <div class="form-group">
                                        <label>Estudio a asignar</label>
                                        <select class="form-control" id="study_id" name="study_id">
                                             @foreach($sts as $st)
                                             <option value="{{ $st->id }}" > {{ $st->name }}</option>
                                             @endforeach
                                        </select>
                                   </div>
                              </div>
                         </div>

                         <div class="clearfix"></div>
                    </div>
                    <div class="modal-footer">
                         <button type="button" class="btn btn-secundary" data-dismiss="modal">Cancelar</button>
                         <button type="submit" class="btn btn-success" id="btn-save" value="create">Guardar</button>
                    </div>
               </form>
          </div>
     </div>
</div>


@push('scripts')
<script src="https://unpkg.com/huebee@1/dist/huebee.pkgd.min.js"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>

<script>
$(document).ready(function () {

     const queryString = window.location.search;
     const urlParams = new URLSearchParams(queryString);
     const page_type = urlParams.get('page_type');
     if (page_type == 2) {
          $('#account-tab').trigger('click');
     }


     console.log(page_type);

     $.ajaxSetup({
          headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
     });

     /*  When group click add user button */
     $('#registrer-study').click(function () {
          $('#btn-save').val("create-user");
          $('#groupForm').trigger("reset");
          $('#groupCrudModal').html("Agregar Nuevo Estudio");
          $('#ajax-crud-modal').modal('show');
     });


     /* When click edit user */
     $('body').on('click', '#group-user', function () {
          var user_id = $(this).data('id');
          $.get( "{{ url('admin/assigned_study_id') }}" +'/' + user_id , function (data) {
               $('#groupCrudModal').html("Editar Estudio Asignado");
               $('#btn-save').val("edit-user");
               $('#ajax-crud-modal').modal('show');
               $('#assigned_study_id').val(data.id);
               $('#study_id').val(data.study_id);
          })
     });

});



if ($("#groupForm").length > 0) {
     $("#groupForm").validate({

          submitHandler: function(form) {

               var actionType = $('#btn-save').val();
               $('#btn-save').html('Sending..');

               var formData = new FormData($("#groupForm")[0]);


               $.ajax({
                    // data: $('#groupForm').serialize(),
                    data: formData,
                    url: "{{ route('admin.assigned_study') }}",
                    type: "POST",
                    cache: false,
                    contentType: false,
                    processData: false,
                    dataType: 'json',
                    success: function (data) {
                         window.location.href = "{{ route('admin.candidate.edit', $candidate->id ) }}?page_type=2";
                    },
                    error: function (data) {
                         console.log('Error:', data);
                         $('#btn-save').html('Save Changes');
                    }
               });
          }
     })
}




</script>


@endpush
