@extends('admin.layout')

@section('content')
<div class="col-md-12 ">
     <form class="form" id="crear_participante" method="post" enctype="multipart/form-data" action="{{ route('admin.candidate.store') }}">
          {{ csrf_field() }}

          <div class="card">
               <div class="card-header ">
                    <div class="card-header">
                         <h4 class="card-title">Datos personales</h4>
                    </div>
               </div>
               <div class="card-body ">
                    <div class="row">
                         <div class="col-md-12">
                              @if($errors->any())
                              <ul class="list-group">
                                   @foreach($errors->all() as $key => $error)
                                   <li class="list-group-item list-group-item-danger"> {{ $error }}</li>
                                   @endforeach
                              </ul>
                              @endif
                         </div>
                         <div class="col-md-6">
                              <div class="row">
                                   <div class="col-md-12">
                                        <label for="name">Fotos del participante</label>
                                        <div id='img_container' style="text-align:center;">
                                             <img id="preview" style="max-height: 450px;" class="img-fluid img-thumbnail" src="https://x1.xingassets.com/assets/frontend_minified/img/users/nobody_m.original.jpg" alt="your image" title=''/>
                                        </div>
                                        <label class="input-file" style="margin-top: 10px;">
                                             Seleccionar imagen<input type="file" name="photo" class="inputfile" data-prev='preview'/>
                                        </label>
                                   </div>
                              </div>
                              <div class="row">
                                   <div class="col-md-12">
                                        <div class="form-group">
                                             <label for="name">Nombre(s)</label>
                                             <input type="text" name="name" id="name" class="form-control" value="{{ old('name') }}" placeholder="Nombre" required>
                                        </div>
                                   </div>
                              </div>
                              <div class="row">
                                   <div class="col-md-12">
                                        <div class="form-group">
                                             <label for="last_name_paternal">Apellido Paterno</label>
                                             <input type="text" name="last_name_paternal" class="form-control" value="{{ old('last_name_paternal') }}" placeholder="Apellido Paterno">
                                        </div>
                                   </div>
                              </div>
                              <div class="row">
                                   <div class="col-md-12">
                                        <div class="form-group">
                                             <label for="last_name_maternal">Apellido Materno</label>
                                             <input type="text" name="last_name_maternal" class="form-control" value="{{ old('last_name_maternal') }}" placeholder="Apellido Materno">
                                        </div>
                                   </div>
                              </div>
                              <div class="row">
                                   <div class="col-md-12">
                                        <div class="form-group">
                                             <label for="date_of_birth">Fecha de nacimiento</label>
                                             <input type="text" name="date_of_birth" id="date_of_birth" class="form-control datetimepicker" value="{{ old('date_of_birth') }}" placeholder="Fecha de nacimiento">
                                        </div>
                                   </div>
                              </div>
                         </div>
                         <div class="col-md-6">
                              <div class="row">
                                   <div class="col-md-12">
                                        <div class="form-group">
                                             <label for="status">Estatus</label><br>
                                             <div class="onoffswitch">
                                                  <input type="checkbox" name="status" value="1"  @if( old('status') == 1 ) checked @endif class="onoffswitch-checkbox" id="myonoffswitch">
                                                  <label class="onoffswitch-label" for="myonoffswitch">
                                                       <span class="onoffswitch-inner"></span>
                                                       <span class="onoffswitch-switch"></span>
                                                  </label>
                                             </div>
                                        </div>
                                   </div>
                              </div>

                              <div class="row">
                                   <div class="col-md-12">
                                        <div class="form-group">
                                             <label for="email">Correo</label>
                                             <input type="email" name="email" id="email" class="form-control" value="{{ old('email') }}" placeholder="Correo" required>
                                        </div>
                                   </div>
                              </div>
                              <div class="row">
                                   <div class="col-md-12">
                                        <div class="form-group">
                                             <label for="phone">Teléfono</label>
                                             <input type="text" name="phone" class="form-control" value="{{ old('phone') }}" placeholder="Teléfono">
                                        </div>
                                   </div>
                              </div>
                              <div class="row">
                                   <div class="col-md-12">
                                        <div class="form-group">
                                             <label for="cell_phone">Celular</label>
                                             <input type="text" name="cell_phone" class="form-control" value="{{ old('cell_phone') }}" placeholder="Celular">
                                        </div>
                                   </div>
                              </div>

                              <div class="row">
                                   <div class="col-md-12">
                                        <div class="form-group">
                                             <label for="ine_number">Número de INE</label>
                                             <input type="text" name="ine_number" class="form-control" value="{{ old('ine_number') }}" placeholder="Número de INE">
                                        </div>
                                   </div>
                              </div>
                              <div class="row">
                                   <div class="col-md-12">
                                        <div class="form-group">
                                             <label for="housewife">Ama de casa</label>
                                             <select class="form-control" name="housewife" >
                                                  <option value="" hidden> -- Seleccione una opción</option>
                                                  <option value="1">Si</option>
                                                  <option value="2">No</option>
                                             </select>
                                        </div>
                                   </div>
                              </div>
                              <div class="row">
                                   <div class="col-md-12">
                                        <div class="form-group">
                                             <label for="out_of_home">Trabaja fuera de casa</label>
                                             <select class="form-control" name="out_of_home" >
                                                  <option value="" hidden> -- Seleccione una opción</option>
                                                  <option value="1">Si</option>
                                                  <option value="2">No</option>
                                             </select>
                                        </div>
                                   </div>
                              </div>
                              <div class="row">
                                   <div class="col-md-12">
                                        <div class="form-group">
                                             <label for="facebook">Facebook</label>
                                             <input type="text" name="facebook" class="form-control" value="{{ old('facebook') }}" placeholder=" ID de Facebook">
                                        </div>
                                   </div>
                              </div>
                              <div class="row">
                                   <div class="col-md-12">
                                        <div class="form-group">
                                             <label for="facebook">Última fecha de participación</label>
                                             <input type="text" name="frecuencia_participacion" class="form-control datetimepicker2" value="{{ old('frecuencia_participacion') }}" placeholder="Fecha de participación">
                                        </div>
                                   </div>
                              </div>
                         </div>
                         <div class="col-md-12">
                              <div class="row">
                                   <div class="col-md-4">
                                        <div class="form-group">
                                             <label for="date_of_birth">Estado</label>
                                             <select class="form-control" name="estado_id[]" id="estado_id" onchange="get_municipios()" >
                                                  <option value="" hidden> -- Seleccione una opción</option>
                                                  @foreach ($estados as $key => $estado)
                                                       <option value="{{ $estado->id }}">{{ $estado->nombre }}</option>
                                                  @endforeach
                                             </select>
                                        </div>
                                   </div>
                                   <div class="col-md-4">
                                        <div class="form-group">
                                             <label for="date_of_birth">Alcaldía / Municipio</label>
                                             <select class="form-control" name="municipio_id[]" id="municipio_id" onchange="get_localidad()" >
                                                  <option value="" hidden> -- Seleccione una opción</option>
                                             </select>
                                        </div>
                                   </div>
                                   <div class="col-md-4">
                                        <div class="form-group">
                                             <label for="date_of_birth">Colonia</label>
                                             <select class="form-control" name="localidad_id[]" id="localidad_id" >
                                                  <option value="" hidden> -- Seleccione una opción</option>
                                             </select>
                                        </div>
                                   </div>
                                   <div class="col-md-4">
                                        <div class="form-group">
                                             <label for="date_of_birth">Calle y número</label>
                                             <input type="text" name="calle" class="form-control" value="{{ old('calle') }}" placeholder="Celular">
                                        </div>
                                   </div>
                                   <div class="col-md-4">
                                        <div class="form-group">
                                             <label for="cp">Codigo Postal</label>
                                             <input type="text" name="cp" class="form-control" value="{{ old('cp') }}" placeholder="Codigo Postal" onkeypress="soloNumeros()">
                                        </div>
                                   </div>
                              </div>
                              <div class="row">
                                   <div class="col-md-12">
                                        <div class="form-group">
                                             <label for="date_of_birth">Comentarios</label>
                                             <textarea style="height: 55px;" class="form-control" name="commentary" rows="5" ></textarea>
                                        </div>
                                   </div>
                              </div>
                         </div>
                         <div class="col-md-12">
                              <hr>
                         </div>
                         <div class="col-md-12">
                              <div class="row">
                                   <div class="col-md-4">
                                        <div class="row">
                                             <div class="col-md-12">
                                                  <label for="name">1. PRODUCTOS DE USO PERSONAL </label>
                                                  <div id='img_container' style="text-align:center;">
                                                       <button type="button" id="img_personal_btn_delete" class="btn btn-danger btn-sm elim_btn" name="button" data-img="img_personal" data-file="photo_personal"
                                                            style="display:none;">
                                                            <i class="fa fa-trash" aria-hidden="true"></i>
                                                       </button>
                                                       <img  style="max-height: 200px;" id="img_personal" class="img-fluid img-thumbnail" src="https://media.informabtl.com/wp-content/uploads/2017/05/bigstock-Cosmetic-Packaging-Spa-And-Bea-179251741-e1494219584976.jpg" alt="your image" title=''/>
                                                  </div>
                                                  <label class="input-file" style="margin-top: 10px;">
                                                       Seleccionar imagen<input type="file" name="photo_personal" class="inputfile" data-prev='img_personal'/>
                                                  </label>
                                             </div>
                                        </div>
                                   </div>
                                   <div class="col-md-4">
                                        <div class="row">
                                             <div class="col-md-12">
                                                  <label for="name">2. PRODUCTOS DE LAVADO</label>
                                                  <div id='img_container' style="text-align:center;">
                                                       <button type="button" id="img_lavado_btn_delete" class="btn btn-danger btn-sm elim_btn" name="button" data-img="img_lavado" data-file="photo_lavado"
                                                            style="display:none;">
                                                            <i class="fa fa-trash" aria-hidden="true"></i>
                                                       </button>
                                                       <img  style="max-height: 200px;" id="img_lavado" class="img-fluid img-thumbnail" src="https://media.informabtl.com/wp-content/uploads/2017/05/bigstock-Cosmetic-Packaging-Spa-And-Bea-179251741-e1494219584976.jpg" alt="your image" title=''/>
                                                  </div>
                                                  <label class="input-file" style="margin-top: 10px;">
                                                       Seleccionar imagen<input type="file" name="photo_lavado" class="inputfile" data-prev='img_lavado'/>
                                                  </label>
                                             </div>
                                        </div>
                                   </div>
                                   <div class="col-md-4">
                                        <div class="row">
                                             <div class="col-md-12">
                                                  <label for="name">3. PRODUCTOS PARA LA LIMPIEZA DEL HOGAR</label>
                                                  <div id='img_container' style="text-align:center;">
                                                       <button type="button" id="img_limpieza_btn_delete" class="btn btn-danger btn-sm elim_btn" name="button" data-img="img_limpieza" data-file="photo_limpieza"
                                                            style="display:none;">
                                                            <i class="fa fa-trash" aria-hidden="true"></i>
                                                       </button>
                                                       <img  style="max-height: 200px;" id="img_limpieza" class="img-fluid img-thumbnail" src="https://media.informabtl.com/wp-content/uploads/2017/05/bigstock-Cosmetic-Packaging-Spa-And-Bea-179251741-e1494219584976.jpg" alt="your image" title=''/>
                                                  </div>
                                                  <label class="input-file" style="margin-top: 10px;">
                                                       Seleccionar imagen<input type="file" name="photo_limpieza" class="inputfile" data-prev='img_limpieza'/>
                                                  </label>
                                             </div>
                                        </div>
                                   </div>

                              </div>
                         </div>
                         <div class="col-md-12">
                              <hr>
                         </div>
                         <div class="col-md-12">
                              <?php foreach ($categorys as $key => $category): ?>
                                   <br>

                                   <h5> <img src="{{ $category->logo }}" class="img-circle" style="width:30px;" alt=""> {{ $category->name }}</h5>
                                   <select id='optgroup' class="mutiselect" name="brands[]" multiple='multiple'>

                                        @foreach ($category->category as $key => $cat)
                                        <optgroup label='{{$cat->name}}'>
                                             @foreach ($cat->brands as $brands)
                                             <option
                                                  value="{{ $brands->id }}"
                                                  {{ collect(old('brands[]'))->contains($brands->id) ? 'selected':'' }}
                                             >
                                                  {{ $brands->name }}
                                             </option>
                                             @endforeach
                                        </optgroup>
                                        @endforeach

                                   </select>
                              <?php endforeach; ?>
                         </div>
                         <div class="col-md-12" style="margin-top:20px;">
                              <span class="help-block"> La contraseña será generada y enviada al nuevo usuario vía correo.</span>
                         </div>
                         <div class="col-md-12">
                              <hr>
                         </div>
                    </div>
                    <input type="hidden" name="complete_information" id="complete_information" value="">
                    <button type="button" onclick="enviar_form(1)" class="btn btn-success btn-fill pull-right">Completado</button>
                    <button type="button" onclick="enviar_form(2)" class="btn btn-warning btn-fill pull-right" style="margin-right: 10px;">Pendiente</button>
                    <a href="{{ route('admin.leader.index') }}" class="btn btn-default btn-fill pull-left">Cancelar</a>
                    <div class="clearfix"></div>
               </div>
          </div>

     </form>
</div>
@stop

@push('style')
<link href="https://cdnjs.cloudflare.com/ajax/libs/multi-select/0.9.12/css/multi-select.min.css" rel="stylesheet" type="text/css" />
<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" rel="stylesheet" type="text/css" />
<style media="screen">
input.form-control.datetimepicker.gj-textbox-md{
     border: 1px solid #e3e3e3;
     padding-left: 10px;
}
input.form-control.datetimepicker2.gj-textbox-md{
     border: 1px solid #e3e3e3;
     padding-left: 10px;
}
i.gj-icon{
     margin-top: 8px;
     margin-right: 5px;
}
div#ms-optgroup{
     width: 100%;
}
</style>

<style>
input[type="file"]
{
     opacity:0;
     -moz-opacity: 0;
     /* IE 5-7 */
     filter: alpha(Opacity=0);
     /* Safari  */
     -khtml-opacity: 0;
     /* IE 8 */
     -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";

}
.input-file
{
     margin: 0 auto;
     text-align: center;
     background-color: #3276b1;
     color: #fff !important;
     display: block;
     width: 180px;
     height: 28px;
     font-size: 17px;
     color: #fff;
     padding: 5px;
     font-weight: bold;
     border-radius: 10px;
}
.ms-container .ms-optgroup-label {
     margin: 0;
     padding: 5px 0 0 5px;
     cursor: pointer;
     color: #3276b1;
}
</style>
@endpush
<!-- Escripts pasarlos al layout -->
@push('scripts')
<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/multi-select/0.9.12/js/jquery.multi-select.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js" type="text/javascript"></script>

<script>
var sin_imgen = 'https://media.informabtl.com/wp-content/uploads/2017/05/bigstock-Cosmetic-Packaging-Spa-And-Bea-179251741-e1494219584976.jpg';

$( document ).ready(function() {
     if ($('.datetimepicker').val() != '') {
          calcular_edad();
     }
     

     $('.datetimepicker').datepicker({
          format: 'yyyy-mm-dd'
     }).on('change', function(value, date) {
          calcular_edad();
    });

     $('.datetimepicker2').datepicker({
          format: 'yyyy-mm-dd'
     });

     $('.mutiselect').multiSelect({
          selectableOptgroup: true
     });

     $('.elim_btn').click(function(){
          var img_change =$(this).data("img");
          var input_change = $(this).data("file");

          $('#'+img_change).attr('src', sin_imgen);
          $("[name='"+input_change+"']").val('');
          $('#'+$(this).attr('id')).hide();

     });

});

function calcular_edad() {
     var fecha1 = moment($('#date_of_birth').val());
     var fecha3 = moment();
     console.log( fecha3.diff(fecha1, 'year'), ' dias de diferencia' );
     console.log(fecha1);
     if ($('#date_of_birth').val() != '') {
          $('#n_edad').val( fecha3.diff(fecha1, 'year') + ' años');
     }else {
          $('#n_edad').val( '' );

     }
}

$(".inputfile").change(function(event) {
     readURL(this);
});

function enviar_form($type) {
     $('#complete_information').val($type);

     if($('#name').val() == ""){
          $( "#name" ).focus();
          swal({
               title: 'Nombre(s) es requerido',
               text: " El nombre es requerido para guardar",
               type: "warning",
          });
          return false;
     }

     if($('#email').val() == ""){
          $( "#email" ).focus();
          swal({
               title: 'Correo es requerido',
               text: " El correo es requerido para guardar",
               type: "warning",
          });
          return false;
     }

     if(!validateEmail($('#email').val())){
          $( "#email" ).focus();
          swal({
               title: 'Correo no valido',
               text: " El correo debe ser valido",
               type: "warning",
          });
          return false;
     }



     function validateEmail($email) {
          var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
          return emailReg.test( $email );
     }


     if ($type == 1) {
          $mensaje  = '¿ Crear participante con información completa ?';
     }else{
          $mensaje  = '¿ Crear participante con información pendiente ?';
     }

     swal({
          title: $mensaje,
          text: " Se notificará al jefe de capturitas ",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#87CB16",
          confirmButtonText: "Si, crear participante",
          cancelButtonText: "Cancelar",
          closeOnCancel: true,
          closeOnConfirm: false
     },
     function(){
          $('#crear_participante').submit();
     });
}

function get_municipios(){
     $('#municipio_id').html('');
     $.ajax({
          url: "{{ route('admin.municipios') }}",
          data:{'estado_id': $('#estado_id').val()},
          type:'get',
          success:  function (response) {
               $('#municipio_id').html();
               $('#municipio_id').append($('<option>', {
                    value: '',
                    text : 'seleccione una opción'
               }));
               $.each(response, function (i, item) {
                    $('#municipio_id').append($('<option>', {
                         value: item.id,
                         text : item.nombre
                    }));
               });
          },
          statusCode: {
               404: function() {
                    alert('web not found');
               }
          },
          error:function(x,xs,xt){
               //alert('error: ' + JSON.stringify(x) +"\n error string: "+ xs + "\n error throwed: " + xt);
          }
     });
}

function get_localidad(){
     $('#localidad_id').html('');
     $.ajax({
          url: "{{ route('admin.localidades') }}",
          data:{'municipio_id': $('#municipio_id').val()},
          type:'get',
          success:  function (response) {
               $('#localidad_id').html();
               $('#localidad_id').append($('<option>', {
                    value: '',
                    text : 'seleccione una opción'
               }));
               $.each(response, function (i, item) {
                    $('#localidad_id').append($('<option>', {
                         value: item.id,
                         text : item.nombre
                    }));
               });
          },
          statusCode: {
               404: function() {
                    alert('web not found');
               }
          },
          error:function(x,xs,xt){
               window.open(JSON.stringify(x));
               //alert('error: ' + JSON.stringify(x) +"\n error string: "+ xs + "\n error throwed: " + xt);
          }
     });
}

function soloNumeros(e){
  var key = window.event ? e.which : e.keyCode;
  if (key < 48 || key > 57) {
    e.preventDefault();
  }
}

function readURL(input) {

     console.log(input);
     if (input.files && input.files[0]) {
          var reader = new FileReader();
          var filename = $(input).val();
          var prev = $(input).attr('data-prev');
          console.log(prev);
          filename = filename.substring(filename.lastIndexOf('\\')+1);
          reader.onload = function(e) {
               console.log(prev);
               $('#'+prev).attr('src', e.target.result);
               $('#'+prev+'_btn_delete').show();
          }
          reader.readAsDataURL(input.files[0]);
     }
}
</script>
@endpush
