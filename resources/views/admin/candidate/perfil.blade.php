@extends('admin.layout')

@section('content')
<div class="col-md-12">
     <div class="card ">
          <div class="card-header ">
               <div class="card-header">
                    <h4 class="card-title">Datos del participante</h4>
               </div>
          </div>
          <div class="card-body ">

               <div class="tab-content">
                         <form class="form" method="post" id="crear_participante" enctype="multipart/form-data" action="{{ route('admin.edit_perfil') }}">
                              <div class="container">
                                   <div class="row">
                                        <div class="col-md-12">
                                             @if($errors->any())
                                             <ul class="list-group">
                                                  @foreach($errors->all() as $key => $error)
                                                  <li class="list-group-item list-group-item-danger"> {{ $error }}</li>
                                                  @endforeach
                                             </ul>
                                             @endif
                                        </div>
                                   </div>
                                   <div class="row">
                                        <div class="col-md-6">
                                             {{ csrf_field() }} {{ method_field('PUT') }}
                                             <div class="col-md-12">
                                                  <label for="name">Fotos del participante</label>
                                                  <div id='img_container' style="text-align:center;">
                                                       <img id="preview" style="max-height: 200px;" class="img-fluid img-thumbnail"
                                                       src="
                                                       @if($candidate->photo)
                                                       {{ $candidate->photo }}
                                                       @else
                                                       https://x1.xingassets.com/assets/frontend_minified/img/users/nobody_m.original.jpg
                                                       @endif
                                                       "
                                                       alt="Foto"
                                                       title=''/>
                                                  </div>
                                                  <label class="input-file" style="margin-top: 10px;">
                                                       Seleccionar imagen<input type="file" name="photo" class="inputfile" data-prev='preview'/>
                                                  </label>
                                             </div>
                                             <br>
                                             <br>
                                             <br>
                                             <div class="col-md-12">
                                                  <div class="form-group">
                                                       <label for="name">Nombre</label>
                                                       <input type="text" name="name" class="form-control" value="{{ old('name', $candidate->name) }}" placeholder="Nombre">
                                                  </div>
                                             </div>
                                             <div class="col-md-12">
                                                  <div class="form-group">
                                                       <label for="last_name_paternal">Apellido Paterno</label>
                                                       <input type="text" name="last_name_paternal" class="form-control" value="{{ old('last_name_paternal', $candidate->last_name_paternal) }}" placeholder="Apellido Paterno">
                                                  </div>
                                             </div>
                                             <div class="col-md-12">
                                                  <div class="form-group">
                                                       <label for="last_name_maternal">Apellido Materno</label>
                                                       <input type="text" name="last_name_maternal" class="form-control" value="{{ old('last_name_maternal', $candidate->last_name_maternal) }}" placeholder="Apellido Materno">
                                                  </div>
                                             </div>
                                             <div class="col-md-12">
                                                  <div class="form-group">
                                                       <label for="email">Correo</label>
                                                       <input type="email" name="email" class="form-control" value="{{ old('email', $candidate->email) }}" placeholder="Correo">
                                                  </div>
                                             </div>
                                             <div class="col-md-12">
                                                  <div class="form-group">
                                                       <label for="facebook">Facebook</label>
                                                       <input type="text" name="facebook" class="form-control" value="{{ old('facebook', $candidate->facebook) }}" placeholder="ID de Facebook">
                                                  </div>
                                             </div>

                                        </div>
                                        <div class="col-md-6">


                                             <div class="col-md-12">
                                                  <div class="row">
                                                       <div class="col-sm-8">
                                                            <div class="form-group">
                                                                 <label for="date_of_birth">Fecha de nacimiento</label>
                                                                 <input type="text" name="date_of_birth" id="date_of_birth" class="form-control datetimepicker" value="{{ old('date_of_birth', $candidate->date_of_birth) }}" placeholder="Fecha de nacimiento">
                                                            </div>
                                                       </div>
                                                       <div class="col-sm-4">
                                                            <div class="form-group">
                                                                 <label>Edad</label>
                                                                 <input type="text" id="n_edad" class="form-control" readonly>
                                                            </div>
                                                       </div>
                                                  </div>
                                             </div>
                                             <div class="col-md-12">
                                                  <div class="form-group">
                                                       <label for="phone">Teléfono</label>
                                                       <input type="text" name="phone" class="form-control" value="{{ old('phone', $candidate->phone) }}" placeholder="Teléfono">
                                                  </div>
                                             </div>
                                             <div class="col-md-12">
                                                  <div class="form-group">
                                                       <label for="cell_phone">Celular</label>
                                                       <input type="text" name="cell_phone" class="form-control" value="{{ old('cell_phone', $candidate->cell_phone) }}" placeholder="Celular">
                                                  </div>
                                             </div>
                                             <div class="col-md-12">
                                                  <div class="form-group">
                                                       <label for="ine_number">Número de INE</label>
                                                       <input type="text" name="ine_number" class="form-control" value="{{ old('ine_number', $candidate->ine_number) }}" placeholder="Número de INE">
                                                  </div>
                                             </div>
                                             <div class="col-md-12">
                                                  <div class="form-group">
                                                       <label for="housewife">Ama de casa</label>
                                                       <select class="form-control" name="housewife" >
                                                            <option value="" hidden> -- Seleccione una opción</option>
                                                            <option value="1" @if(old('housewife', $candidate->housewife) == 1) selected @endif >Si</option>
                                                            <option value="2" @if(old('housewife', $candidate->housewife) == 2) selected @endif>No</option>
                                                       </select>
                                                  </div>
                                             </div>
                                             <div class="col-md-12">
                                                  <div class="form-group">
                                                       <label for="out_of_home">Trabaja fuera de casa</label>
                                                       <select class="form-control" name="out_of_home" >
                                                            <option value="" hidden> -- Seleccione una opción</option>
                                                            <option value="1" @if(old('out_of_home', $candidate->out_of_home) == 1) selected @endif>Si</option>
                                                            <option value="2" @if(old('out_of_home', $candidate->out_of_home) == 2) selected @endif>No</option>
                                                       </select>
                                                  </div>
                                             </div>

                                        </div>
                                   </div>
                                   <div class="row">
                                        <div class="col-md-12">
                                             <hr>
                                        </div>
                                   </div>
                                   <div class="row">
                                        <div class="col-md-6">
                                             <div class="form-group">
                                                  <label for="password">Contraseña</label>
                                                  <input type="password" name="password" class="form-control"  placeholder="Contraseña">
                                                  <span class="help-block">Dejar en blanco para no cambiar la contraseña</span>
                                             </div>
                                        </div>

                                        <div class="col-md-6">
                                             <div class="form-group">
                                                  <label for="password_confirmation">Repite la contraseña</label>
                                                  <input type="password" name="password_confirmation" class="form-control"  placeholder="Repite la contraseña">
                                             </div>
                                        </div>
                                   </div>
                                   <div class="row">
                                        <div class="col-md-12">
                                             <hr>
                                        </div>
                                   </div>

                                   <div class="row">
                                        <div class="col-md-4">
                                             <div class="form-group">
                                                  <label for="date_of_birth">Estado</label>
                                                  <select class="form-control" name="estado_id[]" id="estado_id" onchange="get_municipios()" >
                                                       <option value="" hidden> -- Seleccione una opción</option>
                                                       @foreach ($estados as $key => $estado)
                                                       <option
                                                       value="{{ $estado->id }}"
                                                       @if(old('estado_id', $candidate->estado_id) == $estado->id) selected @endif
                                                       >{{ $estado->nombre }}</option>
                                                       @endforeach
                                                  </select>
                                             </div>
                                        </div>
                                        <div class="col-md-4">
                                             <div class="form-group">
                                                  <label for="date_of_birth">Alcaldía / Municipio</label>
                                                  <select class="form-control" name="municipio_id[]" id="municipio_id" onchange="get_localidad()" >
                                                       <option value="" hidden> -- Seleccione una opción</option>
                                                       @foreach ($municiopios as $key => $municipio)
                                                       <option
                                                       value="{{ $municipio->id }}"
                                                       @if(old('municipio_id', $candidate->municipio_id) == $municipio->id) selected @endif
                                                       >{{ $municipio->nombre }}</option>
                                                       @endforeach
                                                  </select>
                                             </div>
                                        </div>
                                        <div class="col-md-4">
                                             <div class="form-group">
                                                  <label for="date_of_birth">Colonia</label>
                                                  <select class="form-control" name="localidad_id[]" id="localidad_id" >
                                                       <option value="" hidden> -- Seleccione una opción</option>
                                                       @foreach ($localidades as $key => $localidad)
                                                       <option
                                                       value="{{ $localidad->id }}"
                                                       @if(old('estado_id', $candidate->localidad_id) == $localidad->id) selected @endif
                                                       >{{ $localidad->nombre }}</option>
                                                       @endforeach
                                                  </select>
                                             </div>
                                        </div>
                                        <div class="col-md-4">
                                             <div class="form-group">
                                                  <label for="date_of_birth">Calle y número</label>
                                                  <input type="text" name="calle" class="form-control" value="{{ $candidate->calle }}" placeholder="Celular">
                                             </div>
                                        </div>
                                        <div class="col-md-4">
                                             <div class="form-group">
                                                  <label for="cp">Codigo Postal</label>
                                                  <input type="text" name="cp" class="form-control" value="{{  $candidate->codigo_postal }}" placeholder="Codigo postal" maxlength="5" onkeypress="soloNumeros()">
                                             </div>
                                        </div>
                                   </div>

                                   <div class="row">
                                        <div class="col-md-12">
                                             <div class="form-group">
                                                  <label for="date_of_birth">Comentarios</label>
                                                  <textarea style="height: 140px;" class="form-control" name="commentary" rows="5" >{{ old('commentary', $candidate->commentary) }}</textarea>
                                             </div>
                                        </div>
                                   </div>
                                   <div class="row">
                                        <div class="col-md-12">
                                             <hr>
                                        </div>
                                   </div>
                                   <div class="row">
                                        <div class="col-md-4">
                                             <div class="row">
                                                  <div class="col-md-12">
                                                       <label for="name">1. PRODUCTOS PARA EL CUIDADO PERSONAL</label>
                                                       <div id='img_container' style="text-align:center;">

                                                            <button type="button" id="img_personal_btn_delete" class="btn btn-danger btn-sm elim_btn" name="button" data-img="img_personal" data-file="photo_personal"
                                                            @if(!$candidate->photo_personal) style="display:none;" @endif > <i class="fa fa-trash" aria-hidden="true"></i> </button>
                                                            <input type="hidden" name="eliminar_photo_personal" value="0" />
                                                            <img style="max-height: 200px;" id="img_personal" class="img-fluid img-thumbnail"
                                                            src="
                                                            @if($candidate->photo_personal)
                                                            {{ $candidate->photo_personal }}
                                                            @else
                                                            https://media.informabtl.com/wp-content/uploads/2017/05/bigstock-Cosmetic-Packaging-Spa-And-Bea-179251741-e1494219584976.jpg
                                                            @endif
                                                            "/>
                                                       </div>

                                                       <label class="input-file" style="margin-top: 10px;">
                                                            Seleccionar imagen<input type="file" name="photo_personal" class="inputfile" data-prev='img_personal'/>
                                                       </label>
                                                  </div>
                                             </div>
                                        </div>
                                        <div class="col-md-4">
                                             <div class="row">
                                                  <div class="col-md-12">
                                                       <label for="name">2. PRODUCTOS PARA LA LIMPIEZA DEL HOGAR</label>
                                                       <div id='img_container' style="text-align:center;">
                                                            <button type="button" id="img_limpieza_btn_delete" class="btn btn-danger btn-sm elim_btn" name="button" data-img="img_limpieza" data-file="photo_limpieza"
                                                            style="display:none;"> <i class="fa fa-trash" aria-hidden="true"></i> </button>
                                                            <input type="hidden" name="eliminar_photo_limpieza" value="0">

                                                            <img  style="max-height: 200px;" id="img_limpieza" class="img-fluid img-thumbnail"
                                                            src="
                                                            @if($candidate->photo_limpieza)
                                                            {{ $candidate->photo_limpieza }}
                                                            @else
                                                            https://media.informabtl.com/wp-content/uploads/2017/05/bigstock-Cosmetic-Packaging-Spa-And-Bea-179251741-e1494219584976.jpg
                                                            @endif
                                                            "
                                                            alt="your image" title=''/>
                                                       </div>
                                                       <label class="input-file" style="margin-top: 10px;">
                                                            Seleccionar imagen<input type="file" name="photo_limpieza" class="inputfile" data-prev='img_limpieza'/>
                                                       </label>
                                                  </div>
                                             </div>
                                        </div>
                                        <div class="col-md-4">
                                             <div class="row">
                                                  <div class="col-md-12">
                                                       <label for="name">3. PRODUCTOS DE LAVADO</label>
                                                       <div id='img_container' style="text-align:center;">
                                                            <button type="button" id="img_lavado_btn_delete" class="btn btn-danger btn-sm elim_btn" name="button" data-img="img_lavado" data-file="photo_lavado"
                                                            style="display:none;"> <i class="fa fa-trash" aria-hidden="true"></i> </button>
                                                            <input type="hidden" name="eliminar_photo_lavado" value="0">
                                                            <img  style="max-height: 200px;" id="img_lavado" class="img-fluid img-thumbnail"
                                                            src="
                                                            @if($candidate->photo_lavado)
                                                            {{ $candidate->photo_lavado }}
                                                            @else
                                                            https://media.informabtl.com/wp-content/uploads/2017/05/bigstock-Cosmetic-Packaging-Spa-And-Bea-179251741-e1494219584976.jpg
                                                            @endif
                                                            "
                                                            alt="your image" title=''/>
                                                       </div>
                                                       <label class="input-file" style="margin-top: 10px;">
                                                            Seleccionar imagen<input type="file" name="photo_lavado" class="inputfile" data-prev='img_lavado'/>
                                                       </label>
                                                  </div>
                                             </div>
                                        </div>
                                   </div>
                                   <div class="row">
                                        <div class="col-md-12">
                                             <hr>
                                        </div>
                                   </div>
                                   <div class="row">
                                        <div class="col-md-12">
                                             <?php foreach ($categorys as $key => $category): ?>
                                                  <br>
                                                  <h5> <img src="{{ $category->logo }}" class="img-circle" style="width:30px;" alt=""> {{ $category->name }}</h5>
                                                  <select id='optgroup' class="mutiselect" name="brands[]" multiple='multiple'>
                                                       @foreach ($category->category as $key => $cat)
                                                       <optgroup label='{{$cat->name}}'>
                                                            @foreach ($cat->brands as $brands)
                                                            <option
                                                            value="{{ $brands->id }}"
                                                            {{ collect($candidate->brands->pluck('id'))->contains($brands->id) ? 'selected':'' }}
                                                            > {{ $brands->name }} </option>
                                                            @endforeach
                                                       </optgroup>
                                                       @endforeach

                                                  </select>
                                             <?php endforeach; ?>
                                        </div>
                                   </div>
                                   <div class="row">
                                        <div class="col-md-12">
                                             <hr>
                                        </div>
                                   </div>
                                   <input type="hidden" name="complete_information" id="complete_information" value="">
                                   <button type="submit" class="btn btn-success btn-fill pull-right">Actualizar</button>
                                   <div class="clearfix"></div>
                              </div>
                         </form>

               </div>
          </div>

     </div>
</div>
@stop


@push('style')
<link href="https://cdnjs.cloudflare.com/ajax/libs/multi-select/0.9.12/css/multi-select.min.css" rel="stylesheet" type="text/css" />
<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" rel="stylesheet" type="text/css" />
<style media="screen">

input.form-control.datetimepicker.gj-textbox-md{
     border: 1px solid #e3e3e3;
     padding-left: 10px;
}
input.form-control.datetimepicker2.gj-textbox-md{
     border: 1px solid #e3e3e3;
     padding-left: 10px;
}
i.gj-icon{
     margin-top: 8px;
     margin-right: 5px;
}
div#ms-optgroup{
     width: 100%;
}
</style>

<style>
input[type="file"]
{
     opacity:0;
     -moz-opacity: 0;
     /* IE 5-7 */
     filter: alpha(Opacity=0);
     /* Safari  */
     -khtml-opacity: 0;
     /* IE 8 */
     -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";

}
.input-file
{
     margin: 0 auto;
     text-align: center;
     background-color: #3276b1;
     color: #fff !important;
     display: block;
     width: 180px;
     height: 28px;
     font-size: 17px;
     color: #fff;
     padding: 5px;
     font-weight: bold;
     border-radius: 10px;
}
.ms-container .ms-optgroup-label {
     margin: 0;
     padding: 5px 0 0 5px;
     cursor: pointer;
     color: #3276b1;
}
</style>
@endpush
<!-- Escripts pasarlos al layout -->
@push('scripts')
<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/multi-select/0.9.12/js/jquery.multi-select.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js" type="text/javascript"></script>
<script src="http://momentjs.com/downloads/moment.min.js"></script>

<script>
var sin_imgen = 'https://media.informabtl.com/wp-content/uploads/2017/05/bigstock-Cosmetic-Packaging-Spa-And-Bea-179251741-e1494219584976.jpg';

function soloNumeros(e){
     var key = window.event ? e.which : e.keyCode;
     if (key < 48 || key > 57) {
          e.preventDefault();
     }
}

$( document ).ready(function() {


     $('.datetimepicker').datepicker({
          format: 'yyyy-mm-dd',
     }).on('change', function(value, date) {
          calcular_edad();
     });

     calcular_edad();


     $('.datetimepicker2').datepicker({
          format: 'yyyy-mm-dd'
     });

     $('.mutiselect').multiSelect({ selectableOptgroup: true });

     $('.elim_btn').click(function(){
          var img_change =$(this).data("img");
          var input_change = $(this).data("file");

          $('#'+img_change).attr('src', sin_imgen);
          $("[name='eliminar_"+input_change+"']").val(1);
          $("[name='"+input_change+"']").val('');
          $('#'+$(this).attr('id')).hide();

     });

});

function calcular_edad() {
     var fecha1 = moment($('#date_of_birth').val());
     var fecha3 = moment();
     console.log( fecha3.diff(fecha1, 'year'), ' dias de diferencia' );
     console.log(fecha1);
     if ($('#date_of_birth').val() != '') {
          $('#n_edad').val( fecha3.diff(fecha1, 'year') + ' años');
     }else {
          $('#n_edad').val( '' );

     }
}

$(".inputfile").change(function(event) {
     readURL(this);
});

function enviar_form($type) {
     $('#complete_information').val($type);
     if ($type == 1) {
          $mensaje  = '¿ Editar participante con información completa ?';
     }else{
          $mensaje  = '¿ Editar participante con información pendiente ?';
     }

     swal({
          title: $mensaje,
          text: " Se notificará al jefe de capturitas ",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#87CB16",
          confirmButtonText: "Si, crear participante",
          cancelButtonText: "Cancelar",
          closeOnCancel: true,
          closeOnConfirm: false
     },
     function(){
          $('#crear_participante').submit();
     });
}


function get_municipios(){
     $('#municipio_id').html('');
     $.ajax({
          url: "{{ route('admin.municipios') }}",
          data:{'estado_id': $('#estado_id').val()},
          type:'get',
          success:  function (response) {
               $('#municipio_id').html();
               $.each(response, function (i, item) {
                    $('#municipio_id').append($('<option>', {
                         value: item.id,
                         text : item.nombre
                    }));
               });
          },
          statusCode: {
               404: function() {
                    alert('web not found');
               }
          },
          error:function(x,xs,xt){
               //alert('error: ' + JSON.stringify(x) +"\n error string: "+ xs + "\n error throwed: " + xt);
          }
     });
}

function get_localidad(){
     $('#localidad_id').html('');
     $.ajax({
          url: "{{ route('admin.localidades') }}",
          data:{'municipio_id': $('#municipio_id').val()},
          type:'get',
          success:  function (response) {
               $('#localidad_id').html();
               $.each(response, function (i, item) {
                    $('#localidad_id').append($('<option>', {
                         value: item.id,
                         text : item.nombre
                    }));
               });
          },
          statusCode: {
               404: function() {
                    alert('web not found');
               }
          },
          error:function(x,xs,xt){
               window.open(JSON.stringify(x));
               //alert('error: ' + JSON.stringify(x) +"\n error string: "+ xs + "\n error throwed: " + xt);
          }
     });
}



function readURL(input) {

     console.log(input);
     if (input.files && input.files[0]) {
          var reader = new FileReader();
          var filename = $(input).val();
          var prev = $(input).attr('data-prev');
          console.log(prev);
          filename = filename.substring(filename.lastIndexOf('\\')+1);
          reader.onload = function(e) {
               $('#'+prev).attr('src', e.target.result);
               $('#'+prev+'_btn_delete').show();

          }
          reader.readAsDataURL(input.files[0]);
     }
}

</script>
@endpush
