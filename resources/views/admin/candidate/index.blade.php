@extends('admin.layout')

@section('content')
<div class="col-md-12">
     <div class="card strpied-tabled-with-hover">
          <div class="card-header ">
               <h4 class="card-title">Participantes</h4>
               <p class="card-category">Lista de participantes</p>
               <a href="{{ route('admin.candidate.create') }}" class="btn btn-success btn-wd" style="float: right; margin-top: -50px;">
                    <i class="fa fa-plus"></i> Nuevo
               </a>
          </div>
          <div class="card-body table-responsive">
               <table class="table table-hover table-striped table_js">
                    <thead>
                         <tr>
                              <th>ID</th>
                              <th>Nombre</th>
                              <th>Correo</th>
                              <th class="text-center">Estatus</th>
                              <th class="text-center">Registrado por</th>
                              <th class="text-center">Información</th>
                              @if(auth()->user()->hasRole('Jefe_capturista'))
                                   <th class="text-center">Contactar capturista</th>
                              @else
                                   <th class="text-center">Comentario Jefe</th>
                              @endif
                              <th class="text-center"><i class="nc-icon nc-preferences-circle-rotate"></i></th>
                         </tr>
                    </thead>
                    <tbody>
                         @foreach ($users as $user)
                         <tr>
                              <td>{{ $user->id }}</td>
                              <td>{{ $user->name }} {{ $user->last_name_paternal }} {{ $user->last_name_maternal }}</td>
                              <td>{{ $user->email }}</td>
                              <td class="text-center">
                                   @if($user->status == 1)
                                        <h6><span class="badge badge-success">Activo</span></h6>
                                   @else
                                        <h6><span class="badge badge-danger">Inactivo</span></h6>
                                   @endif
                              </td>
                              <td  class="text-center">
                                   <?php if (isset($user->created_by->name )): ?>
                                        {{ $user->created_by->name }}
                                   <?php endif; ?>
                              </td>
                              <td class="text-center">
                                   @if($user->complete_information == 1)
                                        <h6><span class="badge badge-success">Completa</span></h6>
                                   @else
                                        <h6><span class="badge badge-danger">Pendiente</span></h6>
                                   @endif
                              </td>
                              @if(auth()->user()->hasRole('Jefe_capturista'))
                              <td class="text-center">
                                   <input type="checkbox" class="l_participante" value="{{ $user->id }}">
                              </td>
                              @else
                              <td class="text-center">
                                   @if($user->boss_message != '')
                                   <button type="button" class="btn btn-info btn-outline btn-sm" onclick="ver_coment('{{ $user->boss_message }}')">
                                        <span class="btn-label">
                                             <i class="fa fa-commenting" aria-hidden="true"></i>
                                        </span>
                                   </button>
                                   @endif
                              </td>
                              @endif
                              <td class="text-center">
                                   @can('Ver Cantidato')
                                   <a href="{{ route('answer.index', ['user_id' => $user->id]) }}"
                                        class="btn btn-sm btn-warning btn-outline"
                                   > <i class="fa fa-check-square-o" aria-hidden="true"></i> </a>
                                   @endcan
                                   @can('Ver Cantidato')
                                   <a href="{{ route('admin.candidate.show', $user) }}"
                                        class="btn btn-sm btn-default btn-outline"
                                   > <i class="fa fa-eye"></i> </a>
                                   @endcan
                                   @can('Editar Cantidato')
                                   <a href="{{ route('admin.candidate.edit', $user) }}"
                                        class="btn btn-sm btn-info btn-outline"
                                   > <i class="fa fa-pencil"></i> </a>
                                   @endcan
                                   @can('Eliminar Cantidato')
                                   <form method="POST"
                                        action="{{ route('admin.candidate.destroy', $user) }}"
                                        style="display:inline">
                                        {{ csrf_field() }} {{ method_field('DELETE') }}
                                        <button class="btn btn-sm btn-danger btn-outline"
                                             onclick="return confirm('¿ Estás seguro de querer eliminar este participante ?')"
                                         >
                                         <i class="fa fa-trash"></i>
                                        </button>
                                   </form>
                                   @endcan

                              </td>
                         </tr>
                         @endforeach
                    </tbody>
                    <tfoot>
                         <tr>
                              @if(auth()->user()->hasRole('Jefe_capturista'))
                              <td colspan="8">
                              @else
                              <td colspan="7">
                              @endif
                                   <button style="display:none; float:right;" id="btn_activar"  type="button" data-toggle="modal" data-target="#activar_capturistas" class="btn btn-info pull-left">Asignar</button>
                              </td>
                         </tr>
                    </tfoot>
               </table>
          </div>
     </div>
</div>



<div class="modal fade modal-mini modal-primary " id="ver_coment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
     <div class="modal-dialog">
          <div class="modal-content">
               <div class="modal-header justify-content-center">
                    <div class="modal-profile">
                         <i class="nc-icon nc-email-83"></i>
                    </div>
               </div>
               <div class="modal-body text-center">
                    <p> Comentarios : </p>
                    <div class="col-md-12" id="div_coment">

                    </div>
               </div>
               <div class="modal-footer">
                    <div class="col-md-12" style="text-align:center;">
                         <button type="button" class="btn btn-default btn-simple" data-dismiss="modal">Cancelar</button>

                    </div>
               </div>
          </div>
     </div>
</div>



<div class="modal fade modal-mini modal-primary " id="activar_capturistas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
     <form method="POST" action="{{ route('admin.send_comment') }}">
          {{ csrf_field() }}
          <div class="modal-dialog">
               <div class="modal-content">
                    <div class="modal-header justify-content-center">
                         <div class="modal-profile">
                              <i class="nc-icon nc-email-83"></i>
                         </div>
                    </div>
                    <div class="modal-body text-center">
                         <p> Comentarios : </p>
                         <textarea style="height: 140px;" class="form-control" name="commentary" rows="5" ></textarea>
                         <div id="list_user_active">
                         </div>
                    </div>
                    <div class="modal-footer">
                         <div class="col-md-12">
                              <button type="button" class="btn btn-default btn-simple" data-dismiss="modal">Cancelar</button>
                              <button type="submit" class="btn btn-primary">Enviar</button>
                         </div>
                    </div>
               </div>
          </div>
     </form>
</div>
@stop

@push('style')
<style media="screen">
.popover.clockpicker-popover.bottom.clockpicker-align-left{
    z-index: 9999;
}
</style>

@endpush


<!-- Escripts pasarlos al layout -->
@push('scripts')
<script>
$( document ).ready(function() {

     $('.l_participante').change(function() {
          var ver_boton = false;
          $('#btn_activar').hide();
          $('#list_user_active').html('');
          $('.l_participante').each(function () {
               if (this.checked) {
                    $('#btn_activar').show();
                    $('#list_user_active').append('<input type="hidden" name="user[]" value="'+this.value+'" />');
               }
          });

     });

});

function ver_coment($coment) {
     $('#div_coment').html($coment);
     $('#ver_coment').modal('show');
}

</script>
@endpush
