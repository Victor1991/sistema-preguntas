@extends('admin.layout')

@section('content')
<div class="col-md-12 ml-auto mr-auto">
     <div class="card card-user">
          <div class="card-header no-padding">
               <div class="card-image" style="background-color: #004b8d;">
                    <!-- <img src="/img/background-perfil.jpg" alt="..."> -->
               </div>
          </div>
          <div class="card-body ">
               <div class="author" style="overflow: initial;">
                    <a href="#">
                         <img class="avatar border-gray" src="https://us.123rf.com/450wm/tuktukdesign/tuktukdesign1608/tuktukdesign160800042/61010829-icono-de-usuario-hombre-perfil-hombre-de-negocios-avatar-ilustraci%C3%B3n-vectorial-persona-glifo.jpg?ver=6" alt="...">
                    </a>
                    <p class="card-description">
                         <div class="row">
                              <div class="col-md-6">
                                   <h6 class="card-title">Número identificador</h6>
                                   {{ $candidate->id }}
                                   <hr>
                                   <h6 class="card-title">Nombre completo</h6>
                                   {{ $candidate->name }} {{ $candidate->last_name_paternal }} {{ $candidate->last_name_maternal }}
                                   <hr>
                                   <h6 class="card-title">Correo</h6>
                                   {{ $candidate->email }}
                                   <hr>
                                   <h6 class="card-title">Estatus</h6>
                                   @if($candidate->status == 1)
                                   <h6><span class="badge badge-success">Activo</span></h6>
                                   @else
                                   <h6><span class="badge badge-danger">Inactivo</span></h6>
                                   @endif
                                   <hr>
                                   <h6 class="card-title">Fecha registro</h6>
                                   {{ $candidate->created_at }}
                                   <hr>
                                   <h6 class="card-title">Fecha nacimiento</h6>
                                   {{ $candidate->date_of_birth }}

                              </div>
                              <div class="col-md-6">
                                   <h6 class="card-title">Teléfono</h6>
                                   @if($candidate->phone) {{ $candidate->phone }} @else S/N @endif
                                   <hr>
                                   <h6 class="card-title">Celular</h6>
                                   @if($candidate->cell_phone) {{ $candidate->cell_phone }} @else S/N @endif
                                   <hr>
                                   <h6 class="card-title">Número de ine</h6>
                                   @if($candidate->ine_number) {{ $candidate->ine_number }} @else S/N @endif
                                   <hr>
                                   <h6 class="card-title">Ama de casa</h6>
                                   @if($candidate->housewife == 1) Si @else No @endif
                                   <hr>
                                   <h6 class="card-title">Trabaja fuera de casa</h6>
                                   @if($candidate->out_of_home == 1) Si @else No @endif
                                   <hr>
                                   <h6 class="card-title">Facebook</h6>
                                   @if($candidate->facebook) {{ $candidate->facebook }} @else S/F @endif
                              </div>
                         </div>
                         <div class="row">
                              <div class="col-md-12">
                                   <hr>
                              </div>
                              <div class="col-md-3">
                                   <h6 class="card-title">Estado</h6>
                                   {{ $candidate->get_estado->nombre }}
                              </div>
                              <div class="col-md-3">
                                   <h6 class="card-title"> Alcaldía / Municipio </h6>
                                   {{ $candidate->get_municipio->nombre }}
                              </div>
                              <div class="col-md-3">
                                   <h6 class="card-title">Colonia</h6>
                                   {{ $candidate->get_localidad->nombre }}
                              </div>
                              <div class="col-md-3">
                                   <h6 class="card-title">Código Postal</h6>
                                   @if($candidate->codigo_postal == 1) {{ $candidate->codigo_postal }} @else Sin Info. @endif
                              </div>
                              <div class="col-md-12">
                                   <hr>
                              </div>
                              <div class="col-md-6">
                                   <h6 class="card-title">Calle y Número</h6>
                                   @if($candidate->calle == 1) {{ $candidate->calle }} @else Sin Info. @endif
                              </div>

                              <div class="col-md-6">
                                   <h6 class="card-title">Comentarios</h6>
                                   {{ $candidate->commentary }}
                              </div>
                         </div>



                    </p>
               </div>
          </div>

     </div>
</div>
<div class="col-md-12">
     <div class="card">
          <div class="card-body ">
               <table class="table">
                    <thead>
                         <tr>
                              <th> Categoría </th>
                              <th> Marca </th>
                         </tr>
                    </thead>
                    <tbody>
                         @foreach($candidate->brands as $brands)
                         <tr>
                              <td> {{ $brands->category->name }} </td>
                              <td> {{ $brands->name }} </td>
                         </tr>
                         @endforeach
                    </tbody>
               </table>
          </div>
     </div>


</div>
@stop
