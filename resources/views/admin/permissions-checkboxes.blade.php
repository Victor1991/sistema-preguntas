@foreach( $permissions as $id => $name )
<div class="col-md-12">
     <div class="checkbox">
          <label>
               <input
                    type="checkbox"
                    name="permissions[]"
                    value="{{ $name }}"
                    @if(isset($leader))
                         {{ $leader->permissions->contains($id) ? 'checked' : '' }}
                    @endif
                    @if(isset($user))
                         {{ $user->permissions->contains($id) ? 'checked' : '' }}
                    @endif
                    @if(isset($candidate))
                         {{ $candidate->permissions->contains($id) ? 'checked' : '' }}
                    @endif
               >
               {{ $name }}
          </label>
     </div>
</div>
@endforeach
