@extends('admin.layout')

@section('content')
<div class="col-md-12">
     <div class="row">
          <div class="col-md-12">
               <div class="tab-content">
                    <div class="card strpied-tabled-with-hover">
                         <div class="card-header ">
                              <h4 class="card-title">{{ $category->name }}</h4>
                              <p class="card-category">Lista de marcas</p>
                              <a href="{{ route('brand.create', [ 'category_id' => $category->id ]) }}" class="btn btn-success btn-wd" style="float: right; margin-top: -50px;">
                                   <i class="fa fa-plus"></i> Nuevo
                              </a>
                         </div>
                         <div class="card-body table-responsive">
                              <table class="table table-hover table-striped table_js">
                                   <thead>
                                        <tr>
                                             <th>ID</th>
                                             <th>Nombre</th>
                                             <th class="text-center">Estatus</th>
                                             <th class="text-center"><i class="nc-icon nc-preferences-circle-rotate"></i></th>
                                        </tr>
                                   </thead>
                                   <tbody>
                                        @foreach ($brands as $key => $brand)
                                             <tr>
                                                  <th>{{ $brand->id }}</th>
                                                  <th>{{ $brand->name }}</th>
                                                  <th class="text-center">
                                                       @if($brand->status == 1)
                                                       <h6><span class="badge badge-success">Activo</span></h6>
                                                       @else
                                                       <h6><span class="badge badge-danger">Inactivo</span></h6>
                                                       @endif
                                                  </th>
                                                  <th class="text-center">
                                                       <a href="{{ route('brand.edit' , ['category_id' => $category->id, 'brand' => $brand]) }}"
                                                            class="btn btn-sm btn-info btn-outline" >
                                                            <i class="fa fa-edit" aria-hidden="true"></i> Editar
                                                       </a>
                                                  </th>
                                             </tr>
                                        @endforeach
                                   </tbody>
                              </table>
                         </div>
                    </div>
               </div>
          </div>
     </div>
</div>

@stop
