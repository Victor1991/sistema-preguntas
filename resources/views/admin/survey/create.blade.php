@extends('admin.layout')

@section('content')

<div class="col-md-12">
     <form class="form" method="post" action="{{ route('admin.survey.store') }}">
          {{ csrf_field() }}
          <div class="card">
               <div class="card-header ">
                    <div class="card-header">
                         <h4 class="card-title">Crear Encuesta</h4>
                    </div>
               </div>
               <div class="card-body ">
                    <div class="row">
                         <div class="col-md-12">
                              @if($errors->any())
                              <ul class="list-group">
                                   @foreach($errors->all() as $key => $error)
                                   <li class="list-group-item list-group-item-danger"> {{ $error }}</li>
                                   @endforeach
                              </ul>
                              @endif
                         </div>
                         <div class="col-md-12">
                              <div class="row">
                                   <div class="col-md-12">
                                        <div class="form-group">
                                             <label for="name">Titulo</label>
                                             <input type="text" name="title" class="form-control" value="{{ old('title') }}" placeholder="Titulo" required>
                                        </div>
                                   </div>
                                   <div class="col-md-12">
                                        <div class="form-group">
                                             <label for="study_id">Estudio</label>
                                             <select class="form-control" name="study_id" required  >
                                                  <option value="" hidden> -- Seleccione una opción</option>
                                                  @foreach ($studys as $key => $study)
                                                  <option value="{{ $study->id }}">{{ $study->name }}</option>
                                                  @endforeach
                                             </select>
                                        </div>
                                   </div>
                                   <div class="col-md-12">
                                        <div class="form-group">
                                             <label for="email">Descripción</label>
                                             <textarea class="form-control" name="description" style="height:100px;">{{ old('description') }}</textarea>
                                        </div>
                                   </div>
                                   <div class="col-md-12">
                                        <hr>
                                   </div>
                                   <div class="col-md-12">
                                        <button type="button" class="btn btn-success pull-right" id="btn-add-quetion"> Agregar pregunta </button>
                                   </div>
                                   <div class="col-md-12" id="repuestas_save">
                                        <?php if (old('pregunta')): ?>
                                             <?php foreach (old('pregunta') as $key => $pregunta): ?>
                                                  <div class="jumbotron">
                                                       <div class="col-md-12">
                                                            <div class="form-group">
                                                                 <div class="row">
                                                                      <div class="col-md-12">
                                                                           <div class="form-group">
                                                                                <label for="email">Pregunta</label><br>
                                                                                <input type="text" name="pregunta[<?=$key?>]"  class="form-control" value="<?=$pregunta?>"  required>
                                                                           </div>
                                                                      </div>
                                                                 </div>
                                                                 <hr>
                                                                 <div class="row">
                                                                      <div class="col-sm-8">
                                                                           <label for="email">Repuestas</label>
                                                                      </div>
                                                                      <div class="col-sm-4">
                                                                           <button type="button" name="button" onClick="agrega_respuesta(<?=$key?>)" class="btn btn-success btn-sm pull-right"> Agregar Respuesta </button>
                                                                      </div>
                                                                      <div class="col-md-12" id="div_repustas_<?=$key?>">
                                                                           <div class="row">
                                                                                <div class="col-sm-10">
                                                                                     <input type="text" name="respueta[<?=$key?>][]" class="form-control" value="" placeholder="Respuesta" required="">
                                                                                </div>
                                                                           </div>
                                                                      </div>
                                                                 </div>
                                                            </div>
                                                       </div>
                                                       <div class="col-md-12">
                                                            <hr>
                                                       </div>
                                                       <div class="col-md-12">
                                                            <button type="button" class="btn btn-danger btn-sm pull-right" onClick="eliminar_repuesta(this)"> <i class="fa fa-trash" aria-hidden="true"></i> Eliminar pregunta </button>
                                                       </div>
                                                       <div class="clearfix"></div>
                                                  </div>
                                             <?php endforeach; ?>
                                        <?php endif; ?>
                                   </div>
                                   <div class="col-md-12">
                                        <hr>
                                   </div>
                              </div>
                         </div>
                    </div>

                    <button type="submit" class="btn btn-info btn-fill pull-right">Crear Encuesta</button>
                    <a href="{{ route('admin.survey.index') }}" class="btn btn-default btn-fill pull-left">Cancelar</a>
                    <div class="clearfix"></div>
               </div>
          </div>

     </form>
</div>
<!-- @include('admin.survey.modal_question') -->

@stop

@push('style')
<link href="https://cdnjs.cloudflare.com/ajax/libs/multi-select/0.9.12/css/multi-select.min.css" rel="stylesheet" type="text/css" />
<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" rel="stylesheet" type="text/css" />
<style media="screen">
input.form-control.datetimepicker.gj-textbox-md{
     border: 1px solid #e3e3e3;
     padding-left: 10px;
}
input.form-control.datetimepicker2.gj-textbox-md{
     border: 1px solid #e3e3e3;
     padding-left: 10px;
}
i.gj-icon{
     margin-top: 8px;
     margin-right: 5px;
}
div#ms-optgroup{
     width: 100%;
}
.jumbotron{
     padding: 20px;
}
</style>
@endpush

@push('scripts')
<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/multi-select/0.9.12/js/jquery.multi-select.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js" type="text/javascript"></script>

<script>
$( document ).ready(function() {
     $('.datetimepicker').datepicker({
          format: 'yyyy-mm-dd'
     });
});

let options = {!! json_encode($catalog_questions) !!};
let select;

options.forEach( function(valor, indice, array) {
     select += '<option value="'+valor.id+'">'+valor.name+'</option>';
});

var posi = 0;

function agrega_respuesta(ps){
     console.log(ps);
     $( "#div_repustas_"+ps ).append( '<div class="row">'+
     '<div class="col-sm-10">'+
     '<input type="text" name="respueta['+ps+'][]" class="form-control" value="" placeholder="Respuesta" required>'+
     '</div>'+
     '<div class="col-sm-2">'+
     '<button class="btn btn-danger pull-right" onClick="eliminar_repuesta(this)"><i class="fa fa-trash" aria-hidden="true"></i></button>'+
     '</div>'+
     '</div>' );
}

function ocultar_respuesta(obj, posi ){
      var vl = $(obj).find('option:selected').val();
      if (vl == 2) {
           $('.res_pre_'+posi).hide();
           $('#div_res_'+posi).html('');
      }else {
           $('.res_pre_'+posi).show();
           $('#div_res_'+posi).html('<div class="col-sm-8">'+
                                         '<label for="email">Repuestas</label>'+
                                    '</div>'+
                                    '<div class="col-sm-4">'+
                                         '<button type="button" name="button" onClick="agrega_respuesta('+posi+')" class="btn btn-success btn-sm pull-right"> Agregar Respuesta </button>'+
                                    '</div>'+
                                    '<div class="col-md-12" id="div_repustas_'+posi+'">'+
                                         '<div class="row">'+
                                              '<div class="col-sm-10">'+
                                                   '<input type="text" name="respueta['+posi+'][]" class="form-control" value="" placeholder="Respuesta" required="">'+
                                              '</div>'+
                                         '</div>'+
                                    '</div>');
      }
}

$('#btn-add-quetion').click(function() {
     $( "#repuestas_save" ).append(
          '<div class="jumbotron">'+
               '<div class="col-md-12">'+
                    '<div class="form-group">'+
                         '<div class="row">'+
                              '<div class="col-md-8">'+
                                   '<div class="form-group">'+
                                        '<label>Pregunta</label><br>'+
                                        '<input type="text" name="pregunta['+posi+']"  class="form-control" value=""  required>'+
                                   '</div>'+
                              '</div>'+
                              '<div class="col-md-4">'+
                                   '<div class="form-group">'+
                                        '<label> Tipo pregunta</label><br>'+
                                        '<select class="form-control" onchange="ocultar_respuesta(this, '+posi+')" name="type['+posi+']" required>'+select+
                                        '</select>'+
                                   '</div>'+
                              '</div>'+
                         '</div>'+
                    '<hr class="res_pre_'+posi+'">'+
                    '<div class="row" id="div_res_'+posi+'">'+
                         '<div class="col-sm-8">'+
                              '<label for="email">Repuestas</label>'+
                         '</div>'+
                         '<div class="col-sm-4">'+
                              '<button type="button" name="button" onClick="agrega_respuesta('+posi+')" class="btn btn-success btn-sm pull-right"> Agregar Respuesta </button>'+
                         '</div>'+
                         '<div class="col-md-12" id="div_repustas_'+posi+'">'+
                              '<div class="row">'+
                                   '<div class="col-sm-10">'+
                                        '<input type="text" name="respueta['+posi+'][]" class="form-control" value="" placeholder="Respuesta" required="">'+
                                   '</div>'+
                              '</div>'+
                         '</div>'+
                    '</div>'+
               '</div>'+
          '</div>'+
          '<div class="col-md-12">'+
               '<hr>'+
          '</div>'+
          '<div class="col-md-12">'+
               '<button type="button" class="btn btn-danger btn-sm pull-right" onClick="eliminar_repuesta(this)"> <i class="fa fa-trash" aria-hidden="true"></i> Eliminar pregunta </button>'+
          '</div>'+
          '<div class="clearfix"></div>'+
     '</div>' );
          posi++;

     });
</script>
@endpush
