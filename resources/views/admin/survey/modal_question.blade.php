<div class="modal fade" id="modal_question" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
     <div class="modal-dialog modal-lg">
          <div class="modal-content">
               <div class="modal-body">
                    <div class="row">
                         <div class="col-md-12">
                              <div class="form-group">
                                   <label for="name">Pregunta</label>
                                   <input type="text" name="title" id="title" class="form-control" value="" placeholder="Titulo" required>
                              </div>
                         </div>
                         <div class="col-md-12">
                              <hr>
                         </div>
                         <div class="col-md-8">
                              <p>Respuestas</p>
                         </div>
                         <div class="col-md-4">
                              <button type="button" name="button" id="ageragar_pregunta" class="btn btn-success btn-sm pull-right"> Agregar Respuesta </button>
                         </div>
                         <div class="col-md-12" id="div_repustas">

                         </div>
                         <div class="col-md-12">
                              <hr>
                         </div>
                    </div>
               </div>
               <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-primary" id="add_pregunta">Guardar</button>
               </div>
          </div>
     </div>
</div>
@push('scripts')
<script>
var ind = 0;
$( document ).ready(function() {


     $('#add_pregunta').click(function() {

          var title, repuestas, txt_resp, json_all = [];
          titulo = $('#title').val();
          repuestas =  $("input[name='respueta[]']");
          txt_resp = '';

          if (titulo == "") {
               swal("Titulo requerido", "Es necesario el titulo para continuar", "error");
               return false;
          }

          json_all.push(['pregunta', titulo]);


          if (repuestas.length == 0) {
               swal("Respuesta es requerida", "Es necesaria al menos una pregunta para continuar", "error");
               return false;
          }

          var ar_preg = []
          repuestas.each(function() {
               if ($(this).val() == "") {
                    swal("Respuesta no pueden ir vacía", "La respuesta no puede ir vacía", "error");
                    return false;
               }
               ar_preg.push($(this).val());
               txt_resp += '<div class="col-md-3">'+$(this).val() +'</div>';
          });

          json_all.push(['repuestas', ar_preg]);
          console.log(json_all);
          var json_all = JSON.stringify(json_all);

          $("#repuestas_save").append('<div class="jumbotron" id="preg_'+ind+'">'+
          '<input type="text" value='+json_all+' id="json_'+ind+'" >'+
          '<div class="row">'+
               '<div class="col-md-12">'+
                    '<div class="form-group">'+
                         '<label for="email">Pregunta</label><br>'+titulo+
                    '</div>'+
               '</div>'+
          '</div>'+
          '<hr>'+
          '<div class="row">'+
               '<div class="col-md-12"><label for="email">Repuestas</label></div>'+txt_resp+
          '</div>'+
          '<hr>'+
          '<div class="row">'+
               '<div class="col-md-12">'+
               '<button type="button" class="btn btn-danger pull-right btn-sm" style="margin-left: 10px;" onClick="eliminar_preguta(this)">Eliminar</button>'+
               '<button type="button" class="btn btn-info pull-right btn-sm" onClick="editar_pregunta('+ind+')" >Editar</button>'+
               '</div>'+
          '</div>'+
          '</div>' );

          $('#modal_question').modal('hide');

     });

});

function eliminar_repuesta(objeto) {
     $(objeto).parent().parent().remove();
}
function eliminar_preguta(objeto) {
     $(objeto).parent().parent().parent().remove();
}
function editar_pregunta(position) {
     console.log(position);
}
</script>
@endpush
