@extends('admin.layout')

@section('content')
<div class="col-md-12">
     <form class="form" method="post" action="{{ route('admin.survey.update', $survey) }}">
          {{ csrf_field() }} {{ method_field('PUT') }}
          <div class="card">
               <div class="card-header ">
                    <div class="card-header">
                         <h4 class="card-title">Editar Encuesta</h4>
                    </div>
               </div>
               <div class="card-body ">
                    <div class="row">
                         <div class="col-md-12">
                              @if($errors->any())
                              <ul class="list-group">
                                   @foreach($errors->all() as $key => $error)
                                   <li class="list-group-item list-group-item-danger"> {{ $error }}</li>
                                   @endforeach
                              </ul>
                              @endif
                         </div>
                         <div class="col-md-12">
                              <div class="row">
                                   <div class="col-md-12">
                                        <div class="form-group">
                                             <label for="name">Titulo</label>
                                             <input type="text" name="title" class="form-control" value="{{ $survey->title }}" placeholder="Titulo" required>
                                        </div>
                                   </div>
                                   <div class="col-md-12">
                                        <div class="form-group">
                                             <label for="study_id">Estudio</label>
                                             <select class="form-control" name="study_id" required  >
                                                  @foreach ($studys as $key => $study)
                                                  <option
                                                  @if($survey->study_id == $study->id) selected @endif
                                                  value="{{ $study->id }}">{{ $study->name }}</option>
                                                  @endforeach
                                             </select>
                                        </div>
                                   </div>
                                   <div class="col-md-12">
                                        <div class="form-group">
                                             <label for="email">Descripción</label>
                                             <textarea class="form-control" name="description" style="height:100px;">{{ $survey->description }}</textarea>
                                        </div>
                                   </div>
                                   <div class="col-md-12">
                                        <hr>
                                   </div>
                                   <div class="col-md-12">
                                        <button type="button" class="btn btn-success pull-right" id="btn-add-quetion"> Agregar pregunta </button>
                                   </div>
                                   <div class="col-md-12" id="repuestas_save">
                                        <?php foreach ($survey->question as $key => $pregunta): ?>
                                             <div class="jumbotron">
                                                  <div class="col-md-12">
                                                       <div class="form-group">
                                                            <div class="row">
                                                                 <div class="col-md-8">
                                                                      <div class="form-group">
                                                                           <label>Pregunta</label><br>
                                                                           <input type="text" name="pregunta[<?=$pregunta->id?>]"  class="form-control" value="<?=$pregunta->questions?>"  required>
                                                                      </div>
                                                                 </div>
                                                                 <div class="col-md-4">
                                                                      <div class="form-group">
                                                                           <label>Tipo pregunta</label><br>
                                                                           <select class="form-control" onchange="ocultar_respuesta(this, <?=$pregunta->id?>)" name="type['<?=$pregunta->id?>]" required>
                                                                                <?php foreach ($catalog_questions as $key => $catalog_question): ?>
                                                                                     <option
                                                                                          value="<?=$catalog_question->id?>"
                                                                                       @if($pregunta->catalog_question_id == $catalog_question->id) selected @endif
                                                                                     > <?=$catalog_question->name?></option>
                                                                                <?php endforeach; ?>
                                                                           </select>
                                                                      </div>
                                                                 </div>
                                                            </div>
                                                            <hr class="res_pre_<?=$pregunta->id?>" <?php  if($pregunta->catalog_question_id == 2): ?> style="Display:none;" <?php endif;?> >
                                                            <div class="row" id="div_res_<?=$pregunta->id?>">
                                                                 <?php if($pregunta->catalog_question_id != 2): ?>
                                                                 <div class="col-sm-8">
                                                                      <label for="email">Repuestas</label>
                                                                 </div>
                                                                 <div class="col-sm-4">
                                                                      <button type="button" name="button" onClick="agrega_respuesta(<?=$pregunta->id?>)" class="btn btn-success btn-sm pull-right"> Agregar Respuesta </button>
                                                                 </div>
                                                                 <div class="col-md-12" id="div_repustas_<?=$pregunta->id?>">
                                                                      <?php foreach ($pregunta->answers as $key => $respueta): ?>
                                                                           <div class="row">
                                                                                <div class="col-sm-10">
                                                                                     <input type="text" name="respueta[<?=$pregunta->id?>][<?=$respueta->id?>]" class="form-control" value="<?=$respueta->answer?>" placeholder="Respuesta" required="">
                                                                                </div>
                                                                                <?php if ($key != 0): ?>
                                                                                     <div class="col-sm-2">
                                                                                          <button class="btn btn-danger pull-right" onClick="eliminar_repuesta(this)"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                                                                     </div>
                                                                                <?php endif; ?>
                                                                           </div>
                                                                      <?php endforeach; ?>
                                                                 </div>
                                                             <?php endif; ?>
                                                            </div>
                                                       </div>
                                                  </div>
                                                  <div class="col-md-12">
                                                       <hr>
                                                  </div>
                                                  <div class="col-md-12">
                                                       <button type="button" class="btn btn-danger btn-sm pull-right" onClick="eliminar_repuesta(this)"> <i class="fa fa-trash" aria-hidden="true"></i> Eliminar pregunta </button>
                                                  </div>
                                                  <div class="clearfix"></div>
                                             </div>
                                        <?php endforeach; ?>
                                   </div>
                                   <div class="col-md-12">
                                        <hr>
                                   </div>
                              </div>
                         </div>
                    </div>

                    <button type="submit" class="btn btn-info btn-fill pull-right">Actualizar Encuesta</button>
                    <a href="{{ route('admin.survey.index') }}" class="btn btn-default btn-fill pull-left">Cancelar</a>
                    <div class="clearfix"></div>
               </div>
          </div>

     </form>
</div>
<!-- @include('admin.survey.modal_question') -->

@stop

@push('style')
<link href="https://cdnjs.cloudflare.com/ajax/libs/multi-select/0.9.12/css/multi-select.min.css" rel="stylesheet" type="text/css" />
<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" rel="stylesheet" type="text/css" />
<style media="screen">
input.form-control.datetimepicker.gj-textbox-md{
     border: 1px solid #e3e3e3;
     padding-left: 10px;
}
input.form-control.datetimepicker2.gj-textbox-md{
     border: 1px solid #e3e3e3;
     padding-left: 10px;
}
i.gj-icon{
     margin-top: 8px;
     margin-right: 5px;
}
div#ms-optgroup{
     width: 100%;
}
.jumbotron{
     padding: 20px;
}
</style>
@endpush

@push('scripts')
<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/multi-select/0.9.12/js/jquery.multi-select.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js" type="text/javascript"></script>

<script>

$( document ).ready(function() {

     $('.datetimepicker').datepicker({
          format: 'yyyy-mm-dd'
     });

});

let options = {!! json_encode($catalog_questions) !!};
let select;

options.forEach( function(valor, indice, array) {
     select += '<option value="'+valor.id+'">'+valor.name+'</option>';
});

function ocultar_respuesta(obj, posi ){
      var vl = $(obj).find('option:selected').val();
      if (vl == 2) {
           $('.res_pre_'+posi).hide();
           $('#div_res_'+posi).html('');
      }else {
           $('.res_pre_'+posi).show();
           $('#div_res_'+posi).html('<div class="col-sm-8">'+
                                         '<label for="email">Repuestas</label>'+
                                    '</div>'+
                                    '<div class="col-sm-4">'+
                                         '<button type="button" name="button" onClick="agrega_respuesta('+posi+')" class="btn btn-success btn-sm pull-right"> Agregar Respuesta </button>'+
                                    '</div>'+
                                    '<div class="col-md-12" id="div_repustas_'+posi+'">'+
                                         '<div class="row">'+
                                              '<div class="col-sm-10">'+
                                                   '<input type="text" name="respueta['+posi+'][]" class="form-control" value="" placeholder="Respuesta" required="">'+
                                              '</div>'+
                                         '</div>'+
                                    '</div>');
      }
}

var posi = <?=$survey->question->count()?>;

function agrega_respuesta(ps){
     console.log(ps);
     $( "#div_repustas_"+ps ).append( '<div class="row">'+
     '<div class="col-sm-10">'+
     '<input type="text" name="respueta['+ps+'][]" class="form-control" value="" placeholder="Respuesta" required>'+
     '</div>'+
     '<div class="col-sm-2">'+
     '<button class="btn btn-danger pull-right" onClick="eliminar_repuesta(this)"><i class="fa fa-trash" aria-hidden="true"></i></button>'+
     '</div>'+
     '</div>' );
}

$('#btn-add-quetion').click(function() {
     posi++;
     $( "#repuestas_save" ).append(
          '<div class="jumbotron">'+
               '<div class="col-md-12">'+
                    '<div class="form-group">'+
                         '<div class="row">'+
                              '<div class="col-md-12">'+
                                   '<div class="form-group">'+
                                   '<label for="email">Pregunta</label><br>'+
                                   '<input type="text" name="pregunta['+posi+']"  class="form-control" value=""  required>'+
                                   '</div>'+
                                   '</div>'+
                                   '</div>'+
                                   '<hr>'+
                                   '<div class="row">'+
                                   '<div class="col-sm-8">'+
                                   '<label for="email">Repuestas</label>'+
                                   '</div>'+
                                   '<div class="col-sm-4">'+
                                   '<button type="button" name="button" onClick="agrega_respuesta('+posi+')" class="btn btn-success btn-sm pull-right"> Agregar Respuesta </button>'+
                                   '</div>'+
                                   '<div class="col-md-12" id="div_repustas_'+posi+'">'+
                                   '<div class="row">'+
                                   '<div class="col-sm-10">'+
                                   '<input type="text" name="respueta['+posi+'][]" class="form-control" value="" placeholder="Respuesta" required="">'+
                                   '</div>'+
                                   '</div>'+
                              '</div>'+
                         '</div>'+
                    '</div>'+
               '</div>'+
               '<div class="col-md-12">'+
                    '<hr>'+
               '</div>'+
               '<div class="col-md-12">'+
                    '<button type="button" class="btn btn-danger btn-sm pull-right" onClick="eliminar_repuesta(this)"> <i class="fa fa-trash" aria-hidden="true"></i> Eliminar pregunta </button>'+
               '</div>'+
               '<div class="clearfix"></div>'+
          '</div>' );
     });
</script>
@endpush
