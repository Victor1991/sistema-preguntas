@extends('admin.layout_nomargin')

@section('content')
@can('Ver Administradores')
<div class="col-lg-6 col-sm-6 p-0" style="background-image:url('/img/1.jpg')" >
     <div class="row justify-content-md-center h-100">
          <div class="row align-items-center h-100">
                <div class="col-12 mx-auto">
                    <div class="info_card" style="">
                         <i class="nc-icon nc-badge text-warning" style="font-size:50px; color:#fff !important"></i>
                         <p class="card-category" style="color:#fff !important">Administradores</p>

                         <a class="btn btn-primary btn-sm" href="{{ route('admin.user.index') }}">
                              Ver Administradores
                        </a>

                        <p class="card-title" style="color:#fff !important; font-size:25px;">{{ $admin }}</p>

                    </div>
                </div>
            </div>
     </div>
</div>
@endcan
@can('Ver Jefe Capturista')
<div class="col-lg-6 col-sm-6 p-0" style="background-image:url('/img/2.jpg')" >
     <div class="row justify-content-md-center h-100">
          <div class="row align-items-center h-100">
                <div class="col-12 mx-auto">
                    <div class="info_card" style="">
                         <i class="nc-icon nc-bag" style="font-size:50px; color:#fff !important"></i>
                         <p class="card-category" style="color:#fff !important">Jefe de Capturistas</p>

                         <a class="btn btn-primary btn-sm" href="{{ route('admin.leader.index') }}">
                              Ver Jefe de Capturistas
                        </a>

                        <p class="card-title" style="color:#fff !important; font-size:25px;">{{ $leader }}</p>

                    </div>
                </div>
            </div>
     </div>
</div>

@endcan
@can('Ver Capturista')
<div class="col-lg-6 col-sm-6 p-0" style="background-image:url('/img/3.jpg')" >
     <div class="row justify-content-md-center h-100">
          <div class="row align-items-center h-100">
                <div class="col-12 mx-auto">
                    <div class="info_card" style="">
                         <i class="nc-icon nc-single-copy-04" style="font-size:50px; color:#fff !important"></i>
                         <p class="card-category" style="color:#fff !important">Capturistas</p>

                         <a class="btn btn-primary btn-sm" href="{{ route('admin.capturist.index') }}">
                              Ver Capturistas
                        </a>

                        <p class="card-title" style="color:#fff !important; font-size:25px;">{{ $capturist }}</p>

                    </div>
                </div>
            </div>
     </div>
</div>

@endcan
@can('Ver Cantidato')
<div class="col-lg-6 col-sm-6 p-0" style="background-image:url('/img/4.jpg')" >
     <div class="row justify-content-md-center h-100">
          <div class="row align-items-center h-100">
                <div class="col-12 mx-auto">
                    <div class="info_card" style="">
                         <i class="nc-icon nc-satisfied" style="font-size:50px; color:#fff !important"></i>
                         <p class="card-category" style="color:#fff !important">Candidatos</p>

                         <a class="btn btn-primary btn-sm" href="{{ route('admin.candidate.index') }}">
                              Ver Candidatos
                        </a>

                        <p class="card-title" style="color:#fff !important; font-size:25px;">{{ $candidate }}</p>

                    </div>
                </div>
            </div>
     </div>
</div>
@endcan
@can('Ver Cantidato')
<div class="col-lg-6 col-sm-6 p-0" style="background-image:url('/img/5.jpg')" >
     <div class="row justify-content-md-center h-100">
          <div class="row align-items-center h-100">
                <div class="col-12 mx-auto">
                    <div class="info_card" style="">
                         <i class="nc-icon nc-zoom-split" style="font-size:50px; color:#fff !important"></i>
                         <p class="card-category" style="color:#fff !important">Buscar Participantes</p>

                         <a class="btn btn-danger btn-sm" href="{{ route('admin.buscador') }}">
                              Buscar Participantes
                        </a>

                    </div>
                </div>
            </div>
     </div>
</div>

@endcan
@stop
@push('style')
<style>
.info_card{
     text-align:center;
     background-color: rgba(10, 10, 10, 0.66);
     margin-top:80px;
     margin-bottom:80px;
     padding: 80px;
     border-radius: 15px;
}
</style>

@endpush
