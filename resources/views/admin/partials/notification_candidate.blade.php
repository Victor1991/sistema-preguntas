<a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">
     <i class="nc-icon nc-email-83"></i>
     <span class="notification" id="count_noti">0</span>
     <span class="d-lg-none">Notification</span>
</a>
<ul class="dropdown-menu  dropdown-menu-right" id="link_new">

</ul>
@push('scripts')
<script>
$( document ).ready(function() {
     existen_nuevos()
});


setInterval('existen_nuevos()', 8000);

var pr = 0;
function existen_nuevos() {
     $.ajax({
          url: "{{ route('admin.new_candidate') }}",
          type:'get',
          success:  function (response) {
               $('#link_new').html('');
               $('span#count_noti').html(response.length);
               $.each(response, function( index, value ) {
                    $( "#link_new" ).append( '<a class="dropdown-item" href="{{ url("admin/candidate/") }}/'+value.id+'">Candidato '+value.name+'</a>' );
               });
          },
          error:function(x,xs,xt){
               window.open(JSON.stringify(x));
               //alert('error: ' + JSON.stringify(x) +"\n error string: "+ xs + "\n error throwed: " + xt);
          }
     });
     pr++;
     console.log(pr);
}


</script>
@endpush
