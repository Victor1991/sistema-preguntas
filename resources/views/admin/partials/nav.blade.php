<ul class="nav">
     <li class="nav-item ">
          <a class="nav-link" href="{{ route('dashboard') }}">
               <i class="nc-icon nc-chart-pie-35"></i>
               <p>Panel principal</p>
          </a>
     </li>
     @can('Ver Administradores')
     <li class="nav-item ">
          <a class="nav-link" href="{{ route('admin.user.index') }}">
               <i class="nc-icon nc-badge"></i>
               <p>Administradores</p>
          </a>
     </li>
     @endcan
     @can('Ver Jefe Capturista')
     <li class="nav-item ">
          <a class="nav-link" href="{{ route('admin.leader.index') }}">
               <i class="nc-icon nc-bag"></i>
               <p>Jefe Capturistas</p>
          </a>
     </li>
     @endcan
     @can('Ver Capturista')
     <li class="nav-item ">
          <a class="nav-link" href="{{ route('admin.capturist.index') }}">
               <i class="nc-icon nc-single-copy-04"></i>
               <p>Capturistas</p>
          </a>
     </li>
     @endcan
     @can('Ver Cantidato')
     <li class="nav-item ">
          <a class="nav-link" href="{{ route('admin.candidate.index') }}">
               <i class="nc-icon nc-satisfied"></i>
               <p>Participantes</p>
          </a>
     </li>

     <li class="nav-item ">
          <a class="nav-link" href="{{ route('admin.search.index') }}">
               <i class="nc-icon nc-zoom-split"></i>
               <p>Buscar participantes</p>
          </a>
     </li>

     <li class="nav-item ">
          <a class="nav-link" href="{{ route('admin.company.index') }}">
               <i class="fa fa-building-o" aria-hidden="true"></i>
               <p>Cliente / Empresa</p>
          </a>
     </li>

     <li class="nav-item ">
          <a class="nav-link" href="{{ route('admin.study.index') }}">
               <i class="fa fa-briefcase" aria-hidden="true"></i>
               <p>Estudio</p>
          </a>
     </li>

     <li class="nav-item ">
          <a class="nav-link" href="{{ route('admin.category.index') }}">
               <i class="fa fa-sitemap" aria-hidden="true"></i>
               <p>Categorías / Productos</p>
          </a>
     </li>

     <li class="nav-item ">
          <a class="nav-link" href="{{ route('admin.survey.index') }}">
               <i class="fa fa-check-square-o" aria-hidden="true"></i>
               <p>Encuestas</p>
          </a>
     </li>
     @endcan
     <?php $user = App\User::where('id', auth()->user()->id)->first(); ?>
     <?php if($user->hasRole('Candidato')): ?>
          <li class="nav-item ">
               <a class="nav-link" href="{{ route('admin.perfil') }}">
                    <i class="nc-icon nc-badge"></i>
                    <p>Perfil</p>
               </a>
          </li>
          <li class="nav-item ">
               <a class="nav-link" href="{{ route('answer.index', ['user_id' => auth()->user()->id]) }}">
                    <i class="fa fa-check-square-o" aria-hidden="true"></i>
                    <p>Encuestas</p>
               </a>
          </li>
     <?php endif; ?>
</ul>
