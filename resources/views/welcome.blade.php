<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
     <meta charset="utf-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1">

     <title>Sistema</title>

     <!-- Fonts -->
     <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

     <!-- Styles -->
     <style>
     html, body {
          background-color: #001E38;
          color: #636b6f;
          font-family: 'Raleway', sans-serif;
          font-weight: 100;
          height: 100vh;
          margin: 0;
     }

     .full-height {
          height: 100vh;
     }

     .flex-center {
          align-items: center;
          display: flex;
          justify-content: center;
     }

     .position-ref {
          position: relative;
     }

     .top-right {
          position: absolute;
          right: 10px;
          top: 18px;
     }

     .content {
          text-align: center;
     }

     .title {
          font-size: 84px;
     }


     .m-b-md {
          margin-bottom: 30px;
     }







     a.button7{
 display:inline-block;
 padding:0.7em 1.7em;
 margin:0 0.3em 0.3em 0;
 border-radius:0.2em;
 box-sizing: border-box;
 text-decoration:none;
 font-family:'Roboto',sans-serif;
 font-weight:400;
 color:#FFFFFF;
 background-color:#3369ff;
 box-shadow:inset 0 -0.6em 1em -0.35em rgba(0,0,0,0.17),inset 0 0.6em 2em -0.3em rgba(255,255,255,0.15),inset 0 0 0em 0.05em rgba(255,255,255,0.12);
 text-align:center;
 position:relative;
}
a.button7:active{
 box-shadow:inset 0 0.6em 2em -0.3em rgba(0,0,0,0.15),inset 0 0 0em 0.05em rgba(255,255,255,0.12);
}
@media all and (max-width:30em){
 a.button7{
  display:block;
  margin:0.4em auto;
 }
}
     </style>
</head>
<body>

     <canvas class="container" id="container" role="main"></canvas>
     <div class="content">
          <div style="

">
               <img src="/admin_theme/img/logb.png" alt="sistemaintermerk.com.mx" class="img-responsive">
               <div class="links" style="padding: 40px;">

                   @auth
                         <h3 ><a style="color:#002d74 !important" class="btn btn-info" href="{{ url('/admin') }}">Sistema</a></h3>
                   @else
                        <a class="btn btn-secondary btn-md" style="width:200px;"
                         href="{{ route('login') }}">Entrar</a>
                   @endauth

               </div>
          </div>

          <!--
          <div class="blur blurTop"><canvas class="canvas"id="blurCanvasTop"></canvas></div>
          <div class="blur blurBottom">
               <canvas width="1000px" height="1000px" class="canvas" id="blurCanvasBottom"></canvas></div>
-->

          <style>
          html,body {
               font-family: Raleway;
               overflow:hidden;
               width:100%;
               height:100%;
               position:absolute;

          }
          .container  {
               width:100%;
               height:100%;
               position:absolute;
          }
          #blurCanvasTop{
               left:0%;
               top:0%;
               position:absolute;
          }
          #blurCanvasBottom{
               left:50%;
               top:0;
               position:absolute;
          }
          .content {
               left:15%;
               top:20%;
               width:70%;
               position:relative;
          }
          h1.title {
               color:white;
               font-size: 4vw;
               display:inline;
               font-weight:500;
          }
          p.desc{
               position:relative;
               width:100%;
               font-size:4vw;
               color:rgba(255,255,255,1);
               font-weight: 200;
               margin-bottom:40px;
          }
          .contacts {
               position:absolute;
               right:0%;
               bottom:0;
               margin-bottom:1vw;
               margin-right:1vw;
          }
          .contact li {
               list-style-type:none;
               float:left;
               color: rgba(255,255,255,0.8);
               font-weight:100;
               font-size:17px;
          }
          .contact li a {
               text-decoration:none;
               color: rgba(255,255,255,0.8);
          }
          .contact li a:hover{
               color:rgba(255,255,255,1);
          }
          .contact li~li {
               margin-left:1vw;
          }
          .blur {
               width:100%;
               height:100%;
               position:absolute;
               overflow:hidden;
          }
          .blurTop{
               left:40%;
               top:-110%;
               transform:rotate(20deg);
               transform-origin: 0 100%;
          }
          .blurBottom{
               left:-60%;
               top:100%;
               transform:rotate(20deg);
               transform-origin: 100% 0%;
          }
     </style>

     <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

</script>

<script>
var canvas = document.getElementById('container');
var clone = document.getElementById('blurCanvasBottom');

var cloneCtx = clone.getContext('2d');
var ctx = canvas.getContext('2d');


var w = $('#blurCanvasTop').width();
var h = $('#blurCanvasTop').height();

var ww = $(window).width();
var wh = $(window).height();
canvas.width = ww;
canvas.height= wh;
var partCount = 100;
var particles = [];

function particle(){
     this.color = 'rgba(255,255,255,'+ Math.random()+')';
     console.log(this.color);
     this.x = randomInt(0,ww);
     this.y = randomInt(0,wh);
     this.direction = {
          "x": -1 + Math.random() * 2,
          "y": -1 + Math.random() * 2
     };
     this.vx = 0.3 * Math.random();
     this.vy = 0.3 * Math.random();
     this.radius = randomInt(2,3);
     this.float = function(){
          this.x += this.vx * this.direction.x;
          this.y += this.vy * this.direction.y;
     };
     this.changeDirection = function (axis) {
          this.direction[axis] *= -1;
     };
     this.boundaryCheck = function () {
          if (this.x >= ww) {
               this.x = ww;
               this.changeDirection("x");
          } else if (this.x <= 0) {
               this.x = 0;
               this.changeDirection("x");
          }
          if (this.y >= wh) {
               this.y = wh;
               this.changeDirection("y");
          } else if (this.y <= 0) {
               this.y = 0;
               this.changeDirection("y");
          }
     };
     this.draw = function () {
          ctx.beginPath();
          ctx.fillStyle = this.color;
          ctx.arc(this.x, this.y, this.radius, 0, Math.PI * 2, false);
          ctx.fill();
     };
}
function clearCanvas() {
     cloneCtx.clearRect(0, 0, ww, wh);
     ctx.clearRect(0, 0, ww, wh);
}
function createParticles(){
     for (i=0;i<partCount;i++){
          var p = new particle();
          particles.push(p);
     }
}
function drawParticles() {
     for (i=0;i<particles.length;i++) {
          p = particles[i];
          p.draw();
     }
}
function updateParticles() {
     for (var i = particles.length - 1; i >= 0; i--) {
          p = particles[i];
          p.float();
          p.boundaryCheck();
     }
}
createParticles();
drawParticles();
function animateParticles() {
     clearCanvas();
     drawParticles();
     updateParticles();
     cloneCtx.drawImage(canvas, 0, 0);
     requestAnimationFrame(animateParticles);
}
requestAnimationFrame(animateParticles);
cloneCtx.drawImage(canvas, 0, 0);

$(window).on('resize',function(){
     ww = $(window).width();
     wh = $(window).height();
     canvas.width = ww;
     canvas.height= wh;
     clearCanvas();
     particles = [];
     createParticles();
     drawParticles();
});
function randomInt(min,max)
{
     return Math.floor(Math.random()*(max-min+1)+min);
}
function velocityInt(min,max)
{
     return Math.random()*(max-min+1)+min;
}
</script>

</body>
</html>
