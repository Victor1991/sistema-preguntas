<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="/admin_theme/img/apple-icon.png">
    <link rel="icon" type="image/png" href="/admin_theme/img/favicon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>{{ config('app.name') }} - login </title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />

    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
    <!-- CSS Files -->
    <link href="/admin_theme/css/bootstrap.min.css" rel="stylesheet" />
    <link href="/admin_theme/css/light-bootstrap-dashboard.css?v=2.0.1" rel="stylesheet" />
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="/admin_theme/css/demo.css" rel="stylesheet" />

    </head>

    <body >

    <div class="wrapper wrapper-full-page" style=" background-color: #001E38">

        <div class="" data-color="" data-image="" ;>
            <!--   you can change the color of the filter page using: data-color="blue | purple | green | orange | red | rose " -->
            <div class="content" style="padding-top: 10vh;">
                <div class="container">
                    <div class="col-md-5 col-sm-6 ml-auto mr-auto">
                        <form class="form" method="POST" action="{{ route('login') }}">
                           {{ csrf_field() }}
                            <div class="card card-login">
                                <div class="card-header ">
                                   <img src="/admin_theme/img/logb.png" alt="sistemaintermerk.com.mx" style="width: 100%">
                                </div>
                                <div class="card-body ">
                                    <div class="card-body">

                                        <div class="row">
                                             <div class="col-md-12">
                                                  @include('admin.partials.flash-message')
                                             </div>
                                        </div>

                                        <input type="hidden" name="ip" id="ip" value="">

                                        <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                            <label>Correo</label>
                                            <input type="email" placeholder="Enter email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
                                            @if ($errors->has('email'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                            <label>Contraseña</label>
                                            <input type="password" placeholder="Password" name="password" class="form-control" required>
                                            @if ($errors->has('password'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <div class="form-check">
                                                <label class="form-check-label">
                                                     <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                                    <span class="form-check-sign"></span>
                                                   Recuérdame
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer ml-auto mr-auto" style="margin-top: -20px; text-align: center;">
                                    <button type="submit" class="btn btn-warning btn-wd">Entrar</button><br>
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        Restablecer contraseña
                                    </a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="full-page-background" ></div>

        <footer class="footer">
            <div class="container">
                <nav>
                    <p class="copyright text-center">
                        ©
                        <script>
                            document.write(new Date().getFullYear())
                        </script>
                         Sistema Web
                    </p>
                </nav>
            </div>
        </footer>
    </div>

</body>
<!--   Core JS Files   -->
<script src="/admin_theme/js/core/jquery.3.2.1.min.js" type="text/javascript"></script>
<script src="/admin_theme/js/core/popper.min.js" type="text/javascript"></script>
<script src="/admin_theme/js/core/bootstrap.min.js" type="text/javascript"></script>
<!--  Plugin for Switches, full documentation here: https://www.jque.re/plugins/version3/bootstrap.switch/ -->
<script src="/admin_theme/js/plugins/bootstrap-switch.js"></script>
<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB2Yno10-YTnLjjn_Vtk0V8cdcY5lC4plU"></script>
<!--  Chartist Plugin  -->
<script src="/admin_theme/js/plugins/chartist.min.js"></script>
<!--  Notifications Plugin    -->
<script src="/admin_theme/js/plugins/bootstrap-notify.js"></script>

<!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
<script src="/admin_theme/js/light-bootstrap-dashboard.js?v=2.0.1" type="text/javascript"></script>
<!-- Light Dashboard DEMO methods, don't include it in your project! -->
<script src="/admin_theme/js/demo.js"></script>
<script>
$( document ).ready(function() {

     $.getJSON('https://api.ipify.org?format=json', function(data){
          $('#ip').val(data.ip);
          console.log(data.ip);
     });

});
</script>

</html>
