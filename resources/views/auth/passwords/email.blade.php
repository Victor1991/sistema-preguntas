<!DOCTYPE html>
<html lang="en">

<head>
     <meta charset="utf-8" />
     <link rel="apple-touch-icon" sizes="76x76" href="/admin_theme/img/apple-icon.png">
     <link rel="icon" type="image/png" href="/admin_theme/img/favicon.png">
     <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
     <title>{{ config('app.name') }} - login </title>
     <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />

     <!--     Fonts and icons     -->
     <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
     <!-- CSS Files -->
     <link href="/admin_theme/css/bootstrap.min.css" rel="stylesheet" />
     <link href="/admin_theme/css/light-bootstrap-dashboard.css?v=2.0.1" rel="stylesheet" />
     <!-- CSS Just for demo purpose, don't include it in your project -->
     <link href="/admin_theme/css/demo.css" rel="stylesheet" />

</head>

<body >

     <div class="wrapper wrapper-full-page" style="background-image: url(/admin_theme/img/9819.jpg) ">

          <div class="full-page  section-image" data-color="black" data-image="https://demos.creative-tim.com/light-bootstrap-dashboard-pro/assets/img/full-screen-image-2.jpg" ;>
               <!--   you can change the color of the filter page using: data-color="blue | purple | green | orange | red | rose " -->
               <div class="content" style="padding-top: 10vh;">
                    <div class="container">
                         <div class="col-md-5 col-sm-6 ml-auto mr-auto">
                              <div class="panel panel-default">
                                   <div class="panel-heading">Reset Password</div>

                                   <div class="panel-body">
                                        @if (session('status'))
                                        <div class="alert alert-success">
                                             {{ session('status') }}
                                        </div>
                                        @endif

                                        <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
                                             {{ csrf_field() }}

                                             <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                                  <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                                                  <div class="col-md-6">
                                                       <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                                       @if ($errors->has('email'))
                                                       <span class="help-block">
                                                            <strong>{{ $errors->first('email') }}</strong>
                                                       </span>
                                                       @endif
                                                  </div>
                                             </div>

                                             <div class="form-group">
                                                  <div class="col-md-6 col-md-offset-4">
                                                       <button type="submit" class="btn btn-primary">
                                                            Send Password Reset Link
                                                       </button>
                                                  </div>
                                             </div>
                                        </form>
                                   </div>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
          <div class="full-page-background" ></div>

          <footer class="footer">
               <div class="container">
                    <nav>
                         <p class="copyright text-center">
                              ©
                              <script>
                              document.write(new Date().getFullYear())
                              </script>
                              Sistema Web
                         </p>
                    </nav>
               </div>
          </footer>
     </div>

</body>
<!--   Core JS Files   -->
<script src="/admin_theme/js/core/jquery.3.2.1.min.js" type="text/javascript"></script>
<script src="/admin_theme/js/core/popper.min.js" type="text/javascript"></script>
<script src="/admin_theme/js/core/bootstrap.min.js" type="text/javascript"></script>
<!--  Plugin for Switches, full documentation here: https://www.jque.re/plugins/version3/bootstrap.switch/ -->
<script src="/admin_theme/js/plugins/bootstrap-switch.js"></script>
<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB2Yno10-YTnLjjn_Vtk0V8cdcY5lC4plU"></script>
<!--  Chartist Plugin  -->
<script src="/admin_theme/js/plugins/chartist.min.js"></script>
<!--  Notifications Plugin    -->
<script src="/admin_theme/js/plugins/bootstrap-notify.js"></script>

<!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
<script src="/admin_theme/js/light-bootstrap-dashboard.js?v=2.0.1" type="text/javascript"></script>
<!-- Light Dashboard DEMO methods, don't include it in your project! -->
<script src="/admin_theme/js/demo.js"></script>
<script>
$( document ).ready(function() {

     $.getJSON('https://api.ipify.org?format=json', function(data){
          $('#ip').val(data.ip);
          console.log(data.ip);
     });

});
</script>

</html>
