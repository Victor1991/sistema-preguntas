@component('mail::message')
# Se ha activado tu usuario

Utiliza tus credenciales para acceder al sistema.

@component('mail::table')
     | Correo | Tienes hasta |
     |:----------|:------------|
     |{{ $user->email }} | {{ $caducidad }} |
@endcomponent


@component('mail::button', ['url' => url('login')])
Login
@endcomponent

Saludos,<br>
{{ config('app.name') }}
@endcomponent
