<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', 'PagesController@home');

Route::get('email', function(){
     return new App\Mail\LoginCredencials( App\User::first(), '123434');
});

Route::get('email2', function(){
     return new App\Mail\ActivationCapturist( App\User::first(), date('Y-m-d H:i:s'));
});

Route::group([
     'prefix' => 'admin',
     'namespace' => 'Admin',
     'middleware' => 'auth'],
function(){
     Route::get('/', 'AdminController@index')->name('dashboard');
     Route::resource('user', 'UserController', ['as' => 'admin']);
     Route::resource('leader', 'LeaderController', ['as' => 'admin']);
     Route::resource('capturist', 'CapturistController', ['as' => 'admin']);
     Route::resource('candidate', 'CandidateController', ['as' => 'admin']);
     Route::resource('search', 'SearchController', ['as' => 'admin']);
     Route::resource('company', 'CompanyController', ['as' => 'admin']);
     Route::resource('study', 'StudyController', ['as' => 'admin']);
     Route::resource('category', 'CategoryController', ['as' => 'admin']);
     Route::resource('survey', 'SurveyController', ['as' => 'admin']);

     Route::group(['prefix' => '{category_id}'], function($category_id){
           Route::resource('category', 'CategoryController');
      });

      Route::group(['prefix' => '{user_id}'], function($user_id){
           Route::resource('answer', 'AnswerController');
      });

      Route::get('answer/create/{user_id}/{survey_id}', 'AnswerController@create')->name('answer.create');
      Route::get('answer/edit/{user_id}/{survey_id}', 'AnswerController@edit')->name('answer.edit');

      Route::group(['prefix' => '{category_id}'], function($category_id){
           Route::resource('brand', 'BrandController');
      });

      // Pagina de expotar survey
     Route::get('export_survey/{survey}', 'SurveyController@export_survey')->name('admin.export_survey');

     // Actualizar permisos de usuarios
     Route::put('leader/{user}/permissions', 'UserPermissionsController@update')->name('admin.leader.permissions.update');

     // Activar capturistas
     Route::post('capturista/activar_capturistas', 'CapturistController@activar_capturistas')->name('admin.capturista.activar_capturistas');

     // Pagina de buscador
     Route::get('buscador', 'CandidateController@buscador')->name('admin.buscador');

     // Pagina de buscador
     Route::get('export_search/{search_id}', 'CandidateController@export_search')->name('admin.export_search');

     // Obtener municipios
     Route::get('municipios', 'DireccionesController@municipios')->name('admin.municipios');

     // Obtener localidades
     Route::get('localidades', 'DireccionesController@localidades')->name('admin.localidades');

     // Obtener nuevos candidatos
     Route::get('new_candidate', 'CandidateController@new_candidate')->name('admin.new_candidate');

     // Obtener informar a capturistas
     Route::post('send_comment', 'CandidateController@send_comment')->name('admin.send_comment');

     // Obtener participantes buscador
     Route::get('filter_participants', 'CandidateController@filter_participants')->name('admin.filter_participants');

     // Perfil
     Route::get('perfil', 'CandidateController@perfil')->name('admin.perfil');
     Route::put('edit_perfil', 'CandidateController@edit_perfil')->name('admin.edit_perfil');


     Route::post('assigned_study', 'CandidateController@assigned_study')->name('admin.assigned_study');
     Route::get('assigned_study_id/{id}', 'CandidateController@assigned_study_id')->name('admin.assigned_study_id');
     Route::delete('destroy_assigned_study/{id}', 'CandidateController@destroy_assigned_study')->name('admin.destroy_assigned_study');
});

// Obtener municipios
Route::get('municipios', 'Admin\DireccionesController@municipios')->name('municipios');

// Obtener localidades
Route::get('localidades', 'Admin\DireccionesController@localidades')->name('localidades');

// Desactivar capturistas con cron
Route::get('capturista/desactivar_capturistas', 'PagesController@deactivate_user')->name('capturista.desactivar_capturistas');

// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::get('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
Route::get('register', 'Admin\CandidateController@showRegistrationForm')->name('register');
Route::put('registro', 'Admin\CandidateController@registro')->name('registro');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');
