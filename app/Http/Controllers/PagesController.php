<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class PagesController extends Controller
{

     public function home(){
          return view('welcome');
     }

     public function deactivate_user(){
          $users = User::role('Capturista')->get();
          foreach ($users as $key => $user) {
               $fecha_baja = $this->calcular_fecha($user->activation_date, $user->time_active);
               if ($fecha_baja <= date("Y-m-d H:i:s") ) {
                    $user->status = 0;
                    $user->time_active = null;
                    $user->activation_date = null;
                    $user->ip = null;
                    $user->save();
               }
          }
     }

     public function calcular_fecha($fecha_activacion, $tiempo)
     {
          $timpo_array = explode(":", $tiempo);
          $str = '+'.$timpo_array[0].' hour +'.$timpo_array[1].' minutes +'.$timpo_array[2].' seconds';
          return $cenvertedTime = date('Y-m-d H:i:s', strtotime($str, strtotime($fecha_activacion)) );
     }

}
