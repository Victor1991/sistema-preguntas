<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Category;
use App\MainCategory;
use App\Estado;
use App\Municipio;
use App\Localidad;
use App\Study;
use App\Brand;
use App\StudyUser;
use App\Brand_user_old;
use Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateUserRequest;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Roles;
use App\Events\UserWasCreated;
use Illuminate\Support\Facades\Storage;
use Auth;
use Carbon\Carbon;
use PHPExcel;
use PHPExcel_IOFactory;
use App\Search;
use App\Survey;


class CandidateController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index()
     {
          $users = User::role('Candidato')->get();
          foreach ($users as $key => $user) {
               $users[$key]->created_by = User::where('id', $user->user_id )->first();
          }
          return view('admin.candidate.index', compact('users'));
     }

     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function create()
     {
          $permissions = Permission::pluck('name', 'id');
          $leader = new User;
          $categorys = Category::whereNull('category_id')->orderBy('order', 'ASC')->get();

          foreach ($categorys as $key => $category) {
               $categorys[$key]->category = Category::where('category_id', $category->id )->get();
          }
          $estados = Estado::all();

          return view('admin.candidate.create', compact('leader', 'permissions', 'categorys', 'estados'));
     }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
     {

          $data = $request->validate([
               'name' => 'required|string|max:255',
               'email' => 'required|string|email|max:255|unique:users'
          ]);

          $data['password'] = str_random(8);
          $data['status'] = request('status') == 1 ? 1 : 0 ;
          $data['last_name_paternal'] = request('last_name_paternal');
          $data['last_name_maternal'] = request('last_name_maternal');
          $data['date_of_birth'] = request('date_of_birth');
          $data['phone'] = request('phone');
          $data['cell_phone'] = request('cell_phone');

          $data['ine_number'] = request('ine_number');
          $data['housewife'] = request('housewife');
          $data['out_of_home'] = request('out_of_home');
          $data['facebook'] = request('facebook');
          $data['address'] = request('address');
          $data['commentary'] = request('commentary');
          $data['frecuencia_participacion'] = request('frecuencia_participacion');

          $data['codigo_postal'] = request('cp');
          $data['estado_id'] = request('estado_id')[0];
          $data['municipio_id'] = request('municipio_id')[0];
          $data['localidad_id'] = request('localidad_id')[0];
          $data['calle'] = request('calle');
          $data['user_id'] = Auth::id();

          $data['complete_information'] = request('complete_information');



          if ($request->file('photo') != null) {
               $photo = $request->file('photo')->store('public');
               $data['photo'] = Storage::url($photo);
          }


          if ($request->file('photo_personal') != null) {
               $photo_personal = $request->file('photo_personal')->store('public');
               $data['photo_personal'] = Storage::url($photo_personal);
          }



          if ($request->file('photo_limpieza') != null) {
               $photo_limpieza = $request->file('photo_limpieza')->store('public');
               $data['photo_limpieza'] = Storage::url($photo_limpieza);
          }


          if ($request->file('photo_lavado') != null) {
               $photo_lavado = $request->file('photo_lavado')->store('public');
               $data['photo_lavado'] = Storage::url($photo_lavado);
          }


          $user = User::create($data);

          $user->assignRole('Candidato');

          $user->brands()->attach($request->get('brands'));

          if ($request->filled('permissions')) {
               $user->givePermissionTo($request->permissions);
          }

          // UserWasCreated::dispatch($user, $data['password']);

          return redirect()->route('admin.candidate.index')->with('success', 'Candidato registrado');

     }

     /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function show(User $candidate)
     {
          if(auth()->user()->hasRole('Jefe_capturista')){
               $data['seen_by_boss'] = 1;
               $candidate->update( $data );
          }

          $category = Category::all();
          $surveys = Survey::all();
          return view('admin.candidate.show', compact('candidate','category', 'surveys'));
     }

     /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function edit(User $candidate)
     {

          $permissions = Permission::pluck('name', 'id');
          $categorys = Category::whereNull('category_id')->get();
          $sts = Study::where('removed', 0)->get();
          $surveys = Survey::all();


          foreach ($categorys as $key => $category) {
               $categorys[$key]->category = Category::where('category_id', $category->id )->get();
          }
          $estados = Estado::all();
          $municiopios = array();
          if ($candidate->estado_id) {
               $municiopios = Municipio::where('estado_id', $candidate->estado_id)->orderBy('nombre','ASC')->get();
          }

          $localidades = array();
          if ($candidate->municipio_id) {
               $localidades =  Localidad::where('municipio_id', $candidate->municipio_id)->orderBy('nombre','ASC')->get();
          }

          return view('admin.candidate.edit', compact('candidate', 'permissions', 'categorys', 'estados', 'municiopios', 'localidades', 'sts', 'surveys'));
     }

     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function update(Request $request, User $candidate)
     {
          $data = $request->validate([
               'name' => 'required|string|max:255',
               'email' => 'required|string|email|max:255|unique:users,email,'.$candidate->id,
          ]);

          if($request->filled('password')) {
               $data =  $request->validate([
                    'password' => 'confirmed', 'min:7',
               ]);
          }

          $data['status'] = request('status') == 1 ? 1 : 0 ;
          $data['last_name_paternal'] = request('last_name_paternal');
          $data['last_name_maternal'] = request('last_name_maternal');
          $data['date_of_birth'] = request('date_of_birth');
          $data['phone'] = request('phone');
          $data['cell_phone'] = request('cell_phone');
          $data['ine_number'] = request('ine_number');
          $data['housewife'] = request('housewife');
          $data['out_of_home'] = request('out_of_home');
          $data['facebook'] = request('facebook');
          $data['address'] = request('address');
          $data['commentary'] = request('commentary');
          $data['frecuencia_participacion'] = request('frecuencia_participacion');
          $data['complete_information'] = request('complete_information');

          $data['codigo_postal'] = request('cp');
          $data['estado_id'] = request('estado_id')[0];
          $data['municipio_id'] = request('municipio_id')[0];
          $data['localidad_id'] = request('localidad_id')[0];
          $data['calle'] = request('calle');


          if ($request->file('photo') != null) {
               $photo = $request->file('photo')->store('public');
               $data['photo'] = Storage::url($photo);
          }


          if ($request->file('photo_personal') != null) {
               $photo_personal = $request->file('photo_personal')->store('public');
               $data['photo_personal'] = Storage::url($photo_personal);
          }

          if (request('eliminar_photo_personal') == 1) {
               $data['photo_personal'] = '';
          }

          if ($request->file('photo_limpieza') != null) {
               $photo_limpieza = $request->file('photo_limpieza')->store('public');
               $data['photo_limpieza'] = Storage::url($photo_limpieza);
          }

          if (request('eliminar_photo_limpieza') == 1) {
               $data['photo_limpieza'] = '';
          }

          if ($request->file('photo_lavado') != null) {
               $photo_lavado = $request->file('photo_lavado')->store('public');
               $data['photo_lavado'] = Storage::url($photo_lavado);
          }

          if (request('eliminar_photo_lavado') == 1) {
               $data['photo_lavado'] = '';
          }


          $tst = $candidate->brands->pluck('id')->all();
          $pst_bran = $request->get('brands');

          $resultado = array_diff($pst_bran, $tst);
          $resultado1 = array_diff($tst, $pst_bran);


          if (!empty($resultado) || !empty($resultado1)) {
               foreach ($pst_bran as $key => $brn) {
                    $old = array(
                         'brand_id' => (int)$brn,
                         'user_id' => $candidate->id,
                         'user_modif' =>  Auth::id()
                    );
                    Brand_user_old::create($old);
               }
          }


          $candidate->brands()->sync($request->get('brands'));

          $candidate->update( $data );

          return redirect()->route('admin.candidate.index')->with('success', 'Candidato actualizado');
     }

     /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function destroy( User $candidate)
     {
          $candidate->delete();
          return back()->with('success', 'Cantidat@ eliminado');
     }

     public function buscador()
     {
          $estados = Estado::all();

          return view('admin.candidate.search', compact('estados'));
     }

     public function new_candidate()
     {
          $new_candidate = User::role('Candidato')->whereNull( 'seen_by_boss' )->get();
          return Response::json($new_candidate);
     }

     public function send_comment(Request $request)
     {
          $commentary  = request('commentary');
          foreach ( request('user') as  $usr) {
               $user = User::where('id', $usr)->first();
               $user->boss_message = $commentary;
               $user->save();
          }
          return back()->with('success', 'Capturist@s activado(s) correctamente');
     }

     public function filter_participants(Request $request){

          // dump($request);
          // dump($request->cateogira_id);

          // $categories = Brand::select('id')->whereIn('category_id', $request->cateogira_id)->get();
          // $_ids = array();
          // foreach($categories as $cat){
          //      $_ids[] = $cat->id;
          // }
          // dump( $_ids);

          // die;

          $query = User::query();
          $query->select('users.*');


          $f_inicio = $request->range1;
          $f_fin = $request->range2;
          $fecha_actual = date('Y-m-d');
              
          // Fecha inicio
          if($f_inicio && $f_fin ){
               $inicio = strtotime('-'.$f_inicio.' year' , strtotime($fecha_actual)); //Se resta un año menos
               $fechas_1 = date('Y-m-d', $inicio);
               $fin = strtotime('-'. $f_fin .' year' , strtotime($fecha_actual)); //Se resta un año menos
               $fechas_2 = date('Y-m-d',$fin);
               $query->whereBetween('users.date_of_birth', array($fechas_2,$fechas_1));
          }

          if($f_inicio && $f_fin == null){
               $inicio = strtotime('-'.$f_inicio.' year' , strtotime($fecha_actual)); //Se resta un año menos
               $fechas_1 = date('Y-m-d', $inicio);
               $query->where('date_of_birth', '<=', $fechas_1);

          }

          if($f_inicio == null && $f_fin){
               $fin = strtotime('-'. $f_fin .' year' , strtotime($fecha_actual)); //Se resta un año menos
               $fechas_2 = date('Y-m-d',$fin);
               $query->where('date_of_birth', '>=', $fechas_2);
          }

          // Fecha fin
          // $fechas_request = explode(';',$request->range );
          // $fecha_actual = date('Y-m-d');
          // $inicio = strtotime('-'.$fechas_request[1].' year' , strtotime($fecha_actual)); //Se resta un año menos
          // $fechas_1 = date('Y-m-d', $inicio);
          // $fin = strtotime('-'.$fechas_request[0].' year' , strtotime($fecha_actual)); //Se resta un año menos
          // $fechas_2 = date('Y-m-d',$fin);
     
          // $query->where('date_of_birth', '>=', $fechas_1);
          // $query->where('date_of_birth', '<=', $fechas_2);
          // dump($fechas_1);
          // dump($fechas_2);

          if($request->cateogira_id){
               $categories = Brand::select('id')->whereIn('category_id', $request->cateogira_id)->get();
               $_ids = array();
               foreach($categories as $cat){
                    $_ids[] = $cat->id;
               }
               $query->whereIn('brand_user.brand_id', $_ids);
          }

          if ($request->housewife) {
               $query->where('housewife', $request->housewife);
          }

          if ($request->out_of_home) {
               $query->where('out_of_home', $request->out_of_home);
          }

          if ($request->estado_id) {
               $query->whereIn('estado_id', $request->estado_id);
          }

          if ($request->municipio_id) {
               $query->whereIn('municipio_id', $request->municipio_id);
          }

          if ($request->localidad_id) {
               $query->whereIn('localidad_id', $request->localidad_id);
          }

          //new
          if ($request->cat32_new) {
               $query->whereIn('brand_user.brand_id', $request->cat32_new);
          }

          if ($request->cat33_new) {
               $query->whereIn('brand_user.brand_id', $request->cat33_new);
          }

          if ($request->cat34_new) {
               $query->whereIn('brand_user.brand_id', $request->cat34_new);
          }

          // old
          if ($request->cat32_old) {
               $query->whereIn('brand_user_olds.brand_id', $request->cat32_old);
          }

          if ($request->cat33_old) {
               $query->whereIn('brand_user_olds.brand_id', $request->cat33_old);
          }

          if ($request->cat34_old) {
               $query->whereIn('brand_user_olds.brand_id', $request->cat34_old);
          }

          if ( $request->cat32_old || $request->cat33_old || $request->cat34_old || $request->cat32_new || $request->cat33_new || $request->cat34_new) {
               if ($request->last_participation ) {
                    $occDate= date('Y-m-d');
                    $forOdNextMonth= date('Y-m-d', strtotime("+".$request->last_participation." month", strtotime($occDate)));
                    $query->where('brand_user.created_at', '>=',$forOdNextMonth);
                    $query->where('brand_user_olds.created_at', '>=' ,$forOdNextMonth);
               }
          }


          $results = $query->leftJoin("brand_user","brand_user.user_id","=","users.id")->leftJoin("brand_user_olds","brand_user_olds.user_id","=","users.id")->groupBy('users.id')->get();
          // $results = $query->leftJoin("brand_user","brand_user.user_id","=","users.id")->leftJoin("brand_user_olds","brand_user_olds.user_id","=","users.id")->groupBy('users.id')->toSql();

          return Response::json($results);
     }

     public function filter_participants_new($search){
          // dump($search);
          // dump($search->range);
          // die;

          $query = User::query();
          $query->select('users.*');

          // Beetween
          if ($search->range) {
               $fechas_request = explode(';',$search->range );
               $fecha_actual = date('Y-m-d');
               $inicio = strtotime ('-'.$fechas_request[1].' year' , strtotime($fecha_actual)); //Se resta un año menos
               $fechas_1 = date ('Y-m-d', $inicio);
               $fin = strtotime ('-'.$fechas_request[0].' year' , strtotime($fecha_actual)); //Se resta un año menos
               $fechas_2 = date ('Y-m-d',$fin);
               $query->whereBetween('users.date_of_birth', array($fechas_1,$fechas_2));
          }

          if ($search->housewife) {
               $query->where('housewife', $search->housewife);
          }

          if ($search->out_of_home) {
               $query->where('out_of_home', $search->out_of_home);
          }

          if ($search->estado_id != '[]') {
               $query->whereIn('estado_id', json_decode($search->estado_id));
          }

          if ($search->municipio_id != '[]') {
               $query->whereIn('municipio_id',  json_decode($search->municipio_id));
          }

          if ($search->localidad_id != '[]') {
               $query->whereIn('localidad_id',  json_decode($search->localidad_id));
          }

          //new
          if ($search->cat32_new != '[]') {
               $query->whereIn('brand_user.brand_id',  json_decode($search->cat32_new));
          }

          if ($search->cat33_new != '[]') {
               $query->whereIn('brand_user.brand_id',  json_decode($search->cat33_new));
          }

          if ($search->cat34_new != '[]') {
               $query->whereIn('brand_user.brand_id',  json_decode($search->cat34_new));
          }

          if ($search->cat32_old != '[]') {
               $query->whereIn('brand_user_olds.brand_id',  json_decode($search->cat32_old));
          }

          if ($search->cat33_old != '[]') {
               $query->whereIn('brand_user_olds.brand_id',  json_decode($search->cat33_old));
          }

          if ($search->cat34_old != '[]') {
               $query->whereIn('brand_user_olds.brand_id',  json_decode($search->cat34_old));
          }


          if ( $search->cat32_old || $search->cat33_old || $search->cat34_old || $search->cat32_new || $search->cat33_new || $search->cat34_new) {
               if ($search->last_participation) {
                    $occDate= date('Y-m-d');
                    $forOdNextMonth= date('Y-m-d', strtotime("+".$search->last_participation." month", strtotime($occDate)));
                    $query->where('brand_user.created_at', '>=',$forOdNextMonth);
                    $query->where('brand_user_olds.created_at', '>=' ,$forOdNextMonth);
               }

          }

          $results = $query->leftJoin("brand_user","brand_user.user_id","=","users.id")->leftJoin("brand_user_olds","brand_user_olds.user_id","=","users.id")->groupBy('users.id')->get();
          return $results;
     }



     public function export_search($search_id){

          $objPHPExcel = new PHPExcel();

          $search = Search::where('id', $search_id)->first();

          // $cats_32 = json_decode($search->cat32_new);
          // foreach ($cats_32 as $key => $cat_32) {
          //      $b_32 = Brand::where('id', $cat_32)->first();
          //      dump($b_32->name);
          // }

          // $usrs = $this->filter_participants_new($search);
          // foreach($usrs as $usr){
          //      dump($usr);
          //      $brands = $this->get_categorias_usr($usr->brands, 32, $search);
          //      dump($brands);

          //      die;
          // }

          // dump($search);

          // die;

          $objPHPExcel->getProperties()->setCreator('SUMA WEB DISENO')
          ->setTitle('Busqueda');
          $letras = array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","U","V","W","X","Y","Z");

          // Parte 1
          $objPHPExcel->getActiveSheet()->mergeCells('A1:C1');
          $objPHPExcel->getActiveSheet()->mergeCells('A2:C2');
          $objPHPExcel->getActiveSheet()->mergeCells('A3:C3');
          $tituloscolumnas = array('NOMBRE DE BÚSQUEDA','FECHA', 'CAPTURISTA');
          $objPHPExcel->setActiveSheetIndex(0)
          ->setCellValue('A1', $tituloscolumnas[0])
          ->setCellValue('D1', $search->name)
          ->setCellValue('A2', $tituloscolumnas[1])
          ->setCellValue('D2', date("d-m-Y", strtotime($search->created_at)))
          ->setCellValue('A3', $tituloscolumnas[2])
          ->setCellValue('D3', $search->created_by->name);


          // Parte 2
          $objPHPExcel->getActiveSheet()->mergeCells('A5:C5');
          $objPHPExcel->getActiveSheet()->mergeCells('A6:C6');
          $objPHPExcel->getActiveSheet()->mergeCells('A7:C7');
          $objPHPExcel->getActiveSheet()->mergeCells('A8:C8');
          $tituloscolumnas = array('TÉRMINOS DE BÚSQUEDA','RANGO DE EDAD', 'AMA DE CASA', '¿ TRABAJA FUERA DE CASA ?');
          $rang = array();
          if ($search->range) {
               $rang = explode(';', $search->range);
          }
          $objPHPExcel->setActiveSheetIndex(0)
          ->setCellValue('A5', $tituloscolumnas[0])
          ->setCellValue('A6', $tituloscolumnas[1])
          ->setCellValue('D6', !empty($rang) ? $rang[0].' años - '.$rang[1].' años' : 'Sin filtro')
          ->setCellValue('A7', $tituloscolumnas[2])
          ->setCellValue('D7', $search->housewife == 1 ? 'Si' : 'No' )
          ->setCellValue('A8', $tituloscolumnas[2])
          ->setCellValue('D8', $search->out_of_home == 1 ? 'Si' : 'No' );

          // Parte 3
          $objPHPExcel->getActiveSheet()->mergeCells('A10:C10');
          $objPHPExcel->getActiveSheet()->mergeCells('A11:C11');
          $objPHPExcel->getActiveSheet()->mergeCells('A12:C12');
          $objPHPExcel->getActiveSheet()->mergeCells('A13:C13');
          $tituloscolumnas = array('DIRECCIÓN DE BÚSQUEDA','ESTADOS', 'ALCALDÍA(S) / MUNICIPIO(S)', 'COLONIA');
          $rang = array();
          if ($search->range) {
               $rang = explode(';', $search->range);
          }
          $objPHPExcel->setActiveSheetIndex(0)
          ->setCellValue('A10', $tituloscolumnas[0])
          ->setCellValue('A11', $tituloscolumnas[1]);
          if ($search->estado_id != '[]') {
               $stados = json_decode($search->estado_id);
               $letra = 3;
               foreach ($stados as $key => $stado) {
                    $std = Estado::where('id', $stado)->first();
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras[$letra].'11', $std->nombre);
                    $letra++;
               }
          }else {
               $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D11', 'Sin filtro');
          }

          $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A12', $tituloscolumnas[2]);
          if ($search->municipio_id != '[]') {
               $municipios = json_decode($search->municipio_id);
               $letra = 3;
               foreach ($municipios as $key => $municipio) {
                    $mns = Municipio::where('id', $municipio)->first();
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras[$letra].'12', $mns->nombre);
                    $letra++;
               }
          }else {
               $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D12', 'Sin filtro');
          }

          $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A13', $tituloscolumnas[3]);
          if ($search->localidad_id != '[]') {
               $localidades = json_decode($search->localidad_id);
               $letra = 3;
               foreach ($localidades as $key => $localidad) {
                    $lcl = Localidad::where('id', $localidad)->first();
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras[$letra].'13', $lcl->nombre);
                    $letra++;
               }
          }else {
               $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D13', 'Sin filtro');
          }

          $objPHPExcel->getActiveSheet()->mergeCells('A15:C15');
          $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A15', 'ÚLTIMA PARTICIPACIÓN');
          $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D15', $search->last_participation.' '.($search->last_participation >= 1 ? 'Meses' : 'Mes'));


          // Parte 4
          $objPHPExcel->getActiveSheet()->mergeCells('A17:C17');
          $objPHPExcel->getActiveSheet()->mergeCells('A18:C18');
          $objPHPExcel->getActiveSheet()->mergeCells('A19:C19');
          $objPHPExcel->getActiveSheet()->mergeCells('A20:C20');
          $tituloscolumnas = array('MARCAS QUE USA ACTUALMENTE','PRODUCTOS DE LAVADO', 'PRODUCTOS PARA LA LIMPIEZA DEL HOGAR', 'PRODUCTOS PARA EL CUIDADO PERSONAL');

          $objPHPExcel->setActiveSheetIndex(0)
          ->setCellValue('A17', $tituloscolumnas[0])
          ->setCellValue('A18', $tituloscolumnas[1]);
          if ($search->cat32_new != '[]') {
               $cats_32 = json_decode($search->cat32_new);
               $letra = 3;
               foreach ($cats_32 as $key => $cat_32) {
                    $b_32 = Brand::where('id', $cat_32)->first();
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras[$letra].'18', $b_32->name);
                    $letra++;
               }
          }else {
               $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D18', 'Sin filtro');
          }

          $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A19', $tituloscolumnas[2]);
          if ($search->cat33_new != '[]') {
               $cats_33 = json_decode($search->cat33_new);
               $letra = 3;
               foreach ($cats_33 as $key => $cat_33) {
                    $b_33 = Brand::where('id', $cat_33)->first();
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras[$letra].'19', $b_33->name);
                    $letra++;
               }
          }else {
               $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D19', 'Sin filtro');
          }

          $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A20', $tituloscolumnas[3]);
          if ($search->cat34_new != '[]') {
               $cats_34 = json_decode($search->cat34_new);
               $letra = 3;
               foreach ($cats_34 as $key => $cat_34) {
                    $b_34 = Brand::where('id', $cat_34)->first();
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras[$letra].'20', $b_34->name);
                    $letra++;
               }
          }else {
               $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D20', 'Sin filtro');
          }


          // Parte 5
          $objPHPExcel->getActiveSheet()->mergeCells('A22:C22');
          $objPHPExcel->getActiveSheet()->mergeCells('A23:C23');
          $objPHPExcel->getActiveSheet()->mergeCells('A24:C24');
          $objPHPExcel->getActiveSheet()->mergeCells('A25:C25');

          $tituloscolumnas = array('MARCAS QUE USABA','PRODUCTOS DE LAVADO', 'PRODUCTOS PARA LA LIMPIEZA DEL HOGAR', 'PRODUCTOS PARA EL CUIDADO PERSONAL');

          $objPHPExcel->setActiveSheetIndex(0)
          ->setCellValue('A22', $tituloscolumnas[0])
          ->setCellValue('A23', $tituloscolumnas[1]);
          if ($search->cat32_old != '[]') {
               $cats_32 = json_decode($search->cat32_old);
               $letra = 3;
               foreach ($cats_32 as $key => $cat_32) {
                    $b_32 = Brand::where('id', $cat_32)->first();
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras[$letra].'23', $b_32->name);
                    $letra++;
               }
          }else {
               $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D23', 'Sin filtro');
          }

          $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A24', $tituloscolumnas[2]);
          if ($search->cat33_old != '[]') {
               $cats_33 = json_decode($search->cat33_new);
               $letra = 3;
               foreach ($cats_33 as $key => $cat_33) {
                    $b_33 = Brand::where('id', $cat_33)->first();
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras[$letra].'24', $b_33->name);
                    $letra++;
               }
          }else {
               $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D24', 'Sin filtro');
          }

          $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A25', $tituloscolumnas[3]);
          if ($search->cat34_old != '[]') {
               $cats_34 = json_decode($search->cat34_new);
               $letra = 3;
               foreach ($cats_34 as $key => $cat_34) {
                    $b_34 = Brand::where('id', $cat_34)->first();
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras[$letra].'25', $b_34->name);
                    $letra++;
               }
          }else {
               $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D25', 'Sin filtro');
          }

          //
          $objPHPExcel->getActiveSheet()->mergeCells('A29:J29');
          $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A29', 'PARTICUPANTES');

          //
          $objPHPExcel->getActiveSheet()->mergeCells('E31:G31');
          $objPHPExcel->getActiveSheet()->mergeCells('H31:J31');
          $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E31', 'PRODUCTOS QUE USA');
          $objPHPExcel->setActiveSheetIndex(0)->setCellValue('H31', 'PRODUCTOS QUE USABA');

          //
          $heads = array('NOMBRE','EDAD','EMAIL','TELÉFONO', 'CELULAR', 'REGISTRADO' , 'FECHA DE ÚLTIMA PARTICIPACIÓN', 'LAVADO','LIMPIEZA DEL HOGAR','CUIDADO PERSONAL','LAVADO','LIMPIEZA DEL HOGAR','CUIDADO PERSONAL');
          $letra = 0;
          foreach ($heads as $key => $head) {
               $objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras[$letra].'32', $head);
               $letra++;
          }

          $usrs = $this->filter_participants_new($search);
          $num = 33;
          foreach ($usrs as $key => $usr) {
               $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$num, $usr->name.' '.$usr->last_name_paternal.' '.$usr->last_name_maternal);
               $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$num,
               $usr->date_of_birth != '' ? $this->calcular_edad(date('Y-m-d'), $usr->date_of_birth) : 'S/R');
               $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$num, $usr->email);
               $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$num, $usr->phone);
               $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.$num, $usr->cell_phone);
               $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$num, date("d-m-Y", strtotime($usr->created_at)));
     
               $objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$num, $usr->frecuencia_participacion != "" ? date("d-m-Y", strtotime($usr->frecuencia_participacion)) : '' );
               $objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$num,
                    $this->get_categorias_usr($usr->brands, 32, $search)
               );
               $objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.$num,
                    $this->get_categorias_usr($usr->brands, 33, $search)
               );
               $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J'.$num,
                    $this->get_categorias_usr($usr->brands, 34, $search)
               );
               $objPHPExcel->setActiveSheetIndex(0)->setCellValue('K'.$num,
                    $this->get_categorias_usr_old($usr->brands_old, 32, $search)
               );
               $objPHPExcel->setActiveSheetIndex(0)->setCellValue('L'.$num,
                    $this->get_categorias_usr_old($usr->brands_old, 33, $search)
               );
               $objPHPExcel->setActiveSheetIndex(0)->setCellValue('M'.$num,
                    $this->get_categorias_usr_old($usr->brands_old, 34, $search)
               );
               $num++;
          }

          foreach($letras as $columnID) {
               $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
          }

          header('Content-Type: application/vnd.ms-excel;charset=utf-8');
          header('Content-Disposition: attachment;filename="Usuarios.xls"');
          header('Cache-Control: max-age=1');
          $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
          $objWriter->save('php://output');
          exit;
     }

     public function calcular_edad($inicio, $fin)
     {
          $currentDate = Carbon::createFromFormat('Y-m-d', $inicio);
          $shippingDate = Carbon::createFromFormat('Y-m-d', $fin);
          return $diferencia_en_dias = $currentDate->diffInYears($shippingDate);
     }

     public function get_categorias_usr($cat_usu = array() , $id = '32', $search){
          $categorias = Category::where('category_id', $id)->pluck('id')->toArray();
          // dump($cat_usu);
          // die;
          $_categoria = 'cat'.$id.'_new';
          $_cats = array();
          if($search->$_categoria){
               $_cats = json_decode($search->$_categoria);
          }
          
          if($search->categorys_id != '[]'){
               $_ids_cat = json_decode($search->categorys_id);
               $_get_cat = Category::whereIn('id', $_ids_cat)->where('category_id', $id)->get();
               foreach($_get_cat as $_val){
                    foreach($_val->brands as $_val_bran){
                         $_cats[] = $_val_bran->id;
                    }
               }    
          }
         
          // dump($search);
          // dump($categorias);
          // dump($categorias);
          $search_cats = array();
          $search_cats = json_decode($search->$_categoria);
          if($_cats){
               $ret_cat = array();
               foreach ($cat_usu as $key => $c_u) {
                    if (in_array($c_u->id, $_cats)) {
                         // $brn = Brand::where('id', $c_u->brand_id)->first();
                         if($search_cats){
                              if (in_array($c_u->id, $search_cats)) {
                                   $ret_cat[] = $c_u->name ;
                              }
                         }else{
                              $ret_cat[] = $c_u->name ;

                         }
                    }
               }
               return implode(",", $ret_cat);
          }elseif($search->categorys_id){
               $ret_cat = array();
               foreach (json_decode($search->categorys_id) as $key => $c_u) {
           
                    if (in_array($c_u, $categorias)) {
                         $brn = Brand::where('id', $c_u)->first();
                         // if (in_array($ret_cat->ret_cat, $categorias)) {
                              $ret_cat[] = $brn->name ;
                         // }
                    }
               }
               return implode(",", $ret_cat);
          }else{
               return '';
          }
     }

     public function get_categorias_usr_old($cat_usu = array() , $id = '32', $search ){
          $categorias = Category::where('category_id', $id)->pluck('id')->toArray();
          $ret_cat = array();

          $_categoria = 'cat'.$id.'_old';
          if($search->$_categoria){
               $_cats = json_decode($search->$_categoria);
          }
          
          if($search->categorys_id != '[]'){
               $_ids_cat = json_decode($search->categorys_id);
               $_get_cat = Category::whereIn('id', $_ids_cat)->where('category_id', $id)->get();
               foreach($_get_cat as $_val){
                    foreach($_val->brands as $_val_bran){
                         $_cats[] = $_val_bran->id;
                    }
               }    
          }

          $search_cats = array();
          $search_cats = json_decode($search->$_categoria);

          if($_cats){
               foreach ($cat_usu as $key => $c_u) {
                    if (in_array($c_u->id, $_cats)) {
                         $brn = Brand::where('id', $c_u->brand_id)->first();
                         // if (in_array($brn->category_id, $categorias)) {
                         // $ret_cat[] = $brn->name ;
                         // }
                         if($search_cats){
                              if (in_array($c_u->id, $search_cats)) {
                                   $ret_cat[] = $c_u->name ;
                              }
                         }else{
                              $ret_cat[] = $c_u->name ;

                         }
                    }
               }
               return implode(",", $ret_cat);
          }elseif($search->categorys_id){
                    $ret_cat = array();
                    
                    foreach (json_decode($search->categorys_id) as $key => $c_u) {
                
                         if (in_array($c_u, $categorias)) {
                              $brn = Brand::where('id', $c_u)->first();
                              // if (in_array($ret_cat->ret_cat, $categorias)) {
                                   $ret_cat[] = $brn->name ;
                              // }
                         }
                    }
                    return implode(",", $ret_cat);
          }else{
               return '';

          }
     }

     public function assigned_study_id($id)
     {
          $StudyUser = StudyUser::where('id', $id)->first();
           return Response::json($StudyUser);
     }

     public function destroy_assigned_study($id)
     {
          $dat = $StudyUser = StudyUser::where('id', $id)->first();
          $StudyUser->delete();
          return redirect('admin/candidate/'.$dat->user_id.'/edit?page_type=2')->with('success', 'Estudio eliminado');
     }

     public function assigned_study(Request $request)
     {
          $data['id'] = request('assigned_study_id');
          $data['study_id'] = request('study_id');
          $data['user_id'] = request('user_id');
          $data['assigned_by'] = Auth::user()->id;
          if (request('assigned_study_id')) {
               $study = StudyUser::where('id', request('assigned_study_id'))->first();
               $study->update( $data );
          }else {
               $study = StudyUser::create($data);

          }
          return Response::json($study);
     }

     public function showRegistrationForm()
     {
          $candidate = new User;
          $categorys = Category::whereNull('category_id')->orderBy('order', 'ASC')->get();
          $surveys = Survey::all();
          $estados = Estado::all();

          return view('admin.candidate.register', compact('candidate','categorys', 'surveys', 'estados'));
     }

     public function registro(Request $request)
     {

          $data = $request->validate([
               'name' => 'required|string|max:255',
               'email' => 'required|string|email|max:255|unique:users',
              'password' => 'required|confirmed|min:6'
          ]);

          $data['password'] = request('password');
          $data['status'] =  1 ;
          $data['last_name_paternal'] = request('last_name_paternal');
          $data['last_name_maternal'] = request('last_name_maternal');
          $data['date_of_birth'] = request('date_of_birth');
          $data['phone'] = request('phone');
          $data['cell_phone'] = request('cell_phone');

          $data['ine_number'] = request('ine_number');
          $data['housewife'] = request('housewife');
          $data['out_of_home'] = request('out_of_home');
          $data['facebook'] = request('facebook');
          $data['address'] = request('address');
          $data['commentary'] = request('commentary');
          $data['frecuencia_participacion'] = request('frecuencia_participacion');

          $data['codigo_postal'] = request('cp');
          $data['estado_id'] = request('estado_id')[0];
          $data['municipio_id'] = request('municipio_id')[0];
          $data['localidad_id'] = request('localidad_id')[0];
          $data['calle'] = request('calle');
          $data['user_id'] = Auth::id();

          $data['complete_information'] = request('complete_information');



          if ($request->file('photo') != null) {
               $photo = $request->file('photo')->store('public');
               $data['photo'] = Storage::url($photo);
          }


          if ($request->file('photo_personal') != null) {
               $photo_personal = $request->file('photo_personal')->store('public');
               $data['photo_personal'] = Storage::url($photo_personal);
          }



          if ($request->file('photo_limpieza') != null) {
               $photo_limpieza = $request->file('photo_limpieza')->store('public');
               $data['photo_limpieza'] = Storage::url($photo_limpieza);
          }


          if ($request->file('photo_lavado') != null) {
               $photo_lavado = $request->file('photo_lavado')->store('public');
               $data['photo_lavado'] = Storage::url($photo_lavado);
          }


          $user = User::create($data);

          $user->assignRole('Candidato');

          $user->brands()->attach($request->get('brands'));

          if ($request->filled('permissions')) {
               $user->givePermissionTo($request->permissions);
          }

          UserWasCreated::dispatch($user, $data['password']);

          return redirect()->route('login')->with('success', 'Candidato registrado');
     }

     public function perfil()
     {
          $categorys = Category::whereNull('category_id')->get();
          $sts = Study::where('removed', 0)->get();
          $surveys = Survey::all();
          $candidate = User::where('id', auth()->user()->id)->first();

          foreach ($categorys as $key => $category) {
               $categorys[$key]->category = Category::where('category_id', $category->id )->get();
          }
          $estados = Estado::all();
          $municiopios = array();
          if ($candidate->estado_id) {
               $municiopios = Municipio::where('estado_id', $candidate->estado_id)->get();
          }

          $localidades = array();
          if ($candidate->municipio_id) {
               $localidades =  Localidad::where('municipio_id', $candidate->municipio_id)->get();
          }

          return view('admin.candidate.perfil', compact('candidate', 'categorys', 'estados', 'municiopios', 'localidades', 'sts', 'surveys'));

     }

     public function edit_perfil(Request $request)
     {

          $candidate = User::where('id', auth()->user()->id)->first();

          $data = $request->validate([
               'name' => 'required|string|max:255',
               'email' => 'required|string|email|max:255|unique:users,email,'.$candidate->id,
          ]);

          if($request->filled('password')) {
               $data =  $request->validate([
                    'password' => 'confirmed', 'min:7',
               ]);
          }

          $data['last_name_paternal'] = request('last_name_paternal');
          $data['last_name_maternal'] = request('last_name_maternal');
          $data['date_of_birth'] = request('date_of_birth');
          $data['phone'] = request('phone');
          $data['cell_phone'] = request('cell_phone');
          $data['ine_number'] = request('ine_number');
          $data['housewife'] = request('housewife');
          $data['out_of_home'] = request('out_of_home');
          $data['facebook'] = request('facebook');
          $data['address'] = request('address');
          $data['commentary'] = request('commentary');
          $data['complete_information'] = request('complete_information');

          $data['codigo_postal'] = request('cp');
          $data['estado_id'] = request('estado_id')[0];
          $data['municipio_id'] = request('municipio_id')[0];
          $data['localidad_id'] = request('localidad_id')[0];
          $data['calle'] = request('calle');


          if ($request->file('photo') != null) {
               $photo = $request->file('photo')->store('public');
               $data['photo'] = Storage::url($photo);
          }


          if ($request->file('photo_personal') != null) {
               $photo_personal = $request->file('photo_personal')->store('public');
               $data['photo_personal'] = Storage::url($photo_personal);
          }

          if (request('eliminar_photo_personal') == 1) {
               $data['photo_personal'] = '';
          }

          if ($request->file('photo_limpieza') != null) {
               $photo_limpieza = $request->file('photo_limpieza')->store('public');
               $data['photo_limpieza'] = Storage::url($photo_limpieza);
          }

          if (request('eliminar_photo_limpieza') == 1) {
               $data['photo_limpieza'] = '';
          }

          if ($request->file('photo_lavado') != null) {
               $photo_lavado = $request->file('photo_lavado')->store('public');
               $data['photo_lavado'] = Storage::url($photo_lavado);
          }

          if (request('eliminar_photo_lavado') == 1) {
               $data['photo_lavado'] = '';
          }


          $tst = $candidate->brands->pluck('id')->all();
          $pst_bran = $request->get('brands');

          $resultado = array_diff($pst_bran, $tst);
          $resultado1 = array_diff($tst, $pst_bran);


          if (!empty($resultado) || !empty($resultado1)) {
               foreach ($pst_bran as $key => $brn) {
                    $old = array(
                         'brand_id' => (int)$brn,
                         'user_id' => $candidate->id,
                         'user_modif' =>  Auth::id()
                    );
                    Brand_user_old::create($old);
               }
          }


          $candidate->brands()->sync($request->get('brands'));

          $candidate->update( $data );

          return redirect()->route('admin.perfil')->with('success', 'información actualizada');
     }



}
