<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Survey;
use App\User;
use App\SurveyUser;
use App\AnswerUser;
use App\Question;


class AnswerController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index($user_id)
     {
          $user = User::where('id', $user_id )->first();
          $surveys = Survey::all();
          return view('admin.answer.index', compact('surveys','user'));

     }

     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function create($user_id, $survey_id)
     {
          $user = User::where('id', $user_id )->first();
          $survey = Survey::where('id', $survey_id )->first();
          return view('admin.answer.create', compact('survey','user'));
     }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store($user_id, Request $request)
     {
          $save['survey_id'] = request('survey_id');
          $save['user_id'] = $user_id;
          $survey_users_id = SurveyUser::create($save);
          if (request('pregunta')) {
               foreach (request('pregunta') as $key => $pregunta){
                    $prg = Question::where('id', $key )->first();
                    $p_save['question_id'] = $key;
                    $p_save['survey_id'] = request('survey_id');
                    if ($prg->catalog_question_id == 1) {
                         $p_save['answer_id'] = $pregunta;
                         $p_save['open_answer'] = '';
                    }else {
                         $p_save['answer_id'] = '';
                         $p_save['open_answer'] = $pregunta;
                    }
                    $p_save['user_id'] = $user_id;
                    $p_save['survey_users_id'] = $survey_users_id->id;
                    AnswerUser::create($p_save);
               }
               $survey = Survey::where('id', request('survey_id') )->first();

               if ($survey->question->count() == count(request('pregunta'))) {
                    $dt['finish'] = 1;
                    SurveyUser::where('id', $survey_users_id->id)->update($dt);
               }
          }

          return redirect()->route('answer.index', ['user_id' => $user_id] )->with('success', 'Encuesta conestada');

     }


     /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function show($id)
     {
          //
     }

     /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function edit($user_id, $survey_id)
     {
          $user = User::where('id', $user_id )->first();
          $survey = Survey::where('id', $survey_id )->first();
          return view('admin.answer.edit', compact('survey','user'));
     }

     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function update($user_id, Request $request)
     {

          $survey_users_id = SurveyUser::where('survey_id', request('survey_id'))->where('user_id', $user_id)->first();
          if (request('pregunta')) {
               foreach (request('pregunta') as $key => $pregunta) {
                    $exit =  AnswerUser::where('survey_id', request('survey_id'))->where('user_id', $user_id)->where('question_id', $key)->first();
                    $prg = Question::where('id', $key )->first();
                    if ($exit) {
                         if ($prg->catalog_question_id == 1) {
                              $save_q['answer_id'] = $pregunta;
                              $save_q['open_answer'] = '';
                         }else {
                              $save_q['answer_id'] = '';
                              $save_q['open_answer'] = $pregunta;
                         }
                         $exit->update( $save_q );
                    }else {
                         $p_save['question_id'] = $key;
                         $p_save['survey_id'] = request('survey_id');
                         if ($prg->catalog_question_id == 1) {
                              $p_save['answer_id'] = $pregunta;
                              $p_save['open_answer'] = '';
                         }else {
                              $p_save['answer_id'] = '';
                              $p_save['open_answer'] = $pregunta;
                         }
                         $p_save['user_id'] = $user_id;
                         $p_save['survey_users_id'] = $survey_users_id->id;
                         AnswerUser::create($p_save);
                    }

               }
               $survey = Survey::where('id', request('survey_id') )->first();

               if ($survey->question->count() == count(request('pregunta'))) {
                    $dt['finish'] = 1;
                    SurveyUser::where('id', $survey_users_id->id)->update($dt);
               }
          }

          return redirect()->route('answer.index', ['user_id' => $user_id] )->with('success', 'Encuesta actualizada');

     }

     /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function destroy($id)
     {
          //
     }
}
