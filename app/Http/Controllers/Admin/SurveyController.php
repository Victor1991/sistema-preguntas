<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Study;
use App\User;
use App\Survey;
use App\Question;
use App\Answer;
use App\SurveyUser;
use App\AnswerUser;
use App\CatalogQuestion;
use PHPExcel;
use PHPExcel_IOFactory;


class SurveyController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index()
     {
          $surveys = Survey::all();
          return view('admin.survey.index', compact('surveys'));
     }

     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function create()
     {
          $survey = new Survey;
          $studys = Study::where('removed', 0)->get();
          $catalog_questions = CatalogQuestion::all();
          return view('admin.survey.create', compact('survey','studys','catalog_questions'));
     }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
     {

          $data = $request->validate([
               'title' => 'required|string|max:200',
               'pregunta' => 'required',
               'respueta' => 'required',
               'study_id' => 'required'
          ]);

          $data['title'] = request('title');
          $data['study_id'] = request('study_id');
          $data['description'] = request('description');

          $survey = Survey::create($data);
          $question_id = array();
          foreach (request('pregunta') as $key => $pregunta) {
               $save_q['survey_id'] = $survey->id;
               $save_q['questions'] = $pregunta;
               $save_q['catalog_question_id'] = request('pregunta')[$key];
               $question = Question::create($save_q);
                array_push($question_id, $question->id);
          }

          foreach (request('respueta') as $key => $respueta) {
               foreach ($respueta as $rsp) {
                    $save_r['question_id'] = $question_id[$key];
                    $save_r['answer'] = $rsp;
                    $question = Answer::create($save_r);
               }
          }

          return redirect()->route('admin.survey.index')->with('success', 'Encuesta registrada');

     }

     /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function show($id)
     {
          //
     }

     /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function edit(Survey $survey)
     {
          $studys = Study::where('removed', 0)->get();
          $catalog_questions = CatalogQuestion::all();
          return view('admin.survey.edit', compact('survey', 'studys', 'catalog_questions'));
     }

     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function update(Request $request, Survey $survey)
     {
          $data = $request->validate([
               'title' => 'required|string|max:200',
               'pregunta' => 'required',
               'respueta' => 'required',
               'study_id' => 'required'
          ]);

          $data['title'] = request('title');
          $data['study_id'] = request('study_id');
          $data['description'] = request('description');

          $survey->update( $data );

          $question_id = array();
          foreach (request('pregunta') as $key => $pregunta) {
               $exis_pre = Question::where('id', $key)->first();
               if ( $exis_pre ) {
                    $save_q['questions'] = $pregunta;
                    $exis_pre->update( $save_q );
                    array_push($question_id, $key);
               }else {
                    $save_q['survey_id'] = $survey->id;
                    $save_q['questions'] = $pregunta;
                    $question = Question::create($save_q);
                    array_push($question_id, $question->id);
               }
          }

          Question::whereNotIn('id',$question_id)->where('survey_id', $survey->id)->delete();

          foreach (request('respueta') as $key => $respueta) {

               $preg_id = array();
               foreach ($respueta as $key2 => $rsp) {
                    $exis_res = Answer::where('id', $key2)->where('question_id', $key)->first();
                    if ($exis_res) {
                         $save_r['answer'] = $rsp;
                         $exis_res->update( $save_r );
                         array_push($preg_id, $key2);
                    }else {
                         if (isset($question_id[$key-1])) {
                              $save_r['question_id'] = $question_id[$key-1];
                         }else {
                              $save_r['question_id'] = $key;
                         }
                         $save_r['answer'] = $rsp;
                         $answer = Answer::create($save_r);
                         array_push($preg_id, $answer->id);
                    }
               }

               $da = Answer::whereNotIn('id',$preg_id)->where('question_id', $key)->delete();

          }
          return redirect()->route('admin.survey.index')->with('success', 'Encuesta actualizada');

     }

     /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function destroy(Survey $survey)
     {
          $survey->delete();
          return back()->with('success', 'Administrador eliminado');
     }


     public function export_survey(Survey $survey)
     {
          $users = SurveyUser::where('survey_id', $survey->id)->get();

          $objPHPExcel = new PHPExcel();
          $letras = array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","U","V","W","X","Y","Z");
          $objPHPExcel->getProperties()->setCreator('SUMA WEB DISENO')
          ->setTitle('Encuestas');

          $objPHPExcel->setActiveSheetIndex(0)
          ->setCellValue('A1', 'Nombre Completo')
          ->setCellValue('B1', 'Télefono')
          ->setCellValue('C1', 'Correo')
          ->setCellValue('D1', 'Título')
          ->setCellValue('E1', 'Estudio')
          ->setCellValue('F1', 'Descripción');
          $h = 6;
          foreach ($survey->question as $key => $question) {
               $objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras[$h].'1', $question->questions);
               $h++;
          }
          $i = 2;
          foreach ($users as $key => $user) {
               if($user->user){
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$i, $user->user->name.' '.$user->user->last_name_paternal.''.$user->user->last_name_maternal);
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$i, $user->user->phone);
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$i, $user->user->email);
               }
               $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$i, $survey->title);
               $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.$i, $survey->study->name);
               $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$i, $survey->description);
               $respuetas = AnswerUser::where('user_id', $user->user_id)->where('survey_id', $survey->id)->orderBy('id', 'asc')->get();
               $h = 6;
               foreach ($respuetas as $key => $respueta) {
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras[$h].$i, $respueta->answer_id == 0 ? $respueta->open_answer :  $respueta->answer->answer);
                    $h++;
               }
               $i++;
          }


          header('Content-Type: application/vnd.ms-excel;charset=utf-8');
          header('Content-Disposition: attachment;filename="'.$survey->title.'.xls"');
          header('Cache-Control: max-age=1');
          $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
          $objWriter->save('php://output');
          exit;
     }

}
