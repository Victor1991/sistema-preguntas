<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateUserRequest;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Roles;
use App\Events\UserWasCreated;
use App\Events\UserWasActivated;

class CapturistController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index()
     {
          $users = User::role('Capturista')->get();
           return view('admin.capturist.index', compact('users'));
     }

     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function create()
     {
         $permissions = Permission::pluck('name', 'id');
         $leader = new User;
         return view('admin.capturist.create', compact('leader', 'permissions'));
     }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
     {
          $data = $request->validate([
             'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users'
        ]);

        $data['password'] = str_random(8);
        $data['status'] = request('status') == 1 ? 1 : 0 ;

        $user = User::create($data);

        $user->assignRole('Capturista');

        if ($request->filled('permissions')) {
             $user->givePermissionTo($request->permissions);
        }

        UserWasCreated::dispatch($user, $data['password']);

        return redirect()->route('admin.capturist.index')->with('success', 'Capturista registrado');

     }

     /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function show(User $capturist)
     {
         return view('admin.capturist.show', compact('capturist'));
     }

     /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function edit(User $capturist)
     {
          $permissions = Permission::pluck('name', 'id');
          return view('admin.capturist.edit', compact('capturist', 'permissions'));
     }

     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function update(Request $request, User $capturist)
     {
         $data = $request->validate([
              'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users,email,'.$capturist->id,
         ]);

         if($request->filled('password')) {
            $data =  $request->validate([
                   'password' => 'confirmed', 'min:7',
              ]);
         }

         $data['status'] = request('status') == 1 ? 1 : 0 ;


         $capturist->update( $data );

         return redirect()->route('admin.capturist.index')->with('success', 'Capturista actualizado');
     }

     /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function destroy(User $capturist)
     {
          $capturist->delete();
          return back()->with('success', 'Capturist@ eliminado');
     }

     // public function activar_capturistas(Request $request)
     public function activar_capturistas(Request $request)
     {
          $tiempo  = request('tiempo');
          $ip  = request('ip');
          foreach ( request('user') as  $usr) {
               $user = User::where('id', $usr)->first();
               $user->status = 1;
               $user->ip = request('ip');
               $user->time_active = request('tiempo');
               $user->activation_date = date("Y-m-d H:i:s");
               $user->save();

     
               UserWasActivated::dispatch($user, $this->calcular_fecha(date("Y-m-d H:i:s"), request('tiempo').':00' ));

          }
          return back()->with('success', 'Capturist@s activado(s) correctamente');

     }

     public function calcular_fecha($fecha_activacion, $tiempo)
     {
          $timpo_array = explode(":", $tiempo);
          $str = '+'.$timpo_array[0].' hour +'.$timpo_array[1].' minutes +'.$timpo_array[2].' seconds';
          return $cenvertedTime = date('Y-m-d H:i:s', strtotime($str, strtotime($fecha_activacion)) );
     }
}
