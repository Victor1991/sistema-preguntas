<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Brand;
use App\Category;


class BrandController extends Controller
{
     public function index($category_id){
          $brands = Brand::where('category_id', $category_id)->get();
          $category = Category::where('id', $category_id)->first();
          return view('admin.brand.index', compact('brands', 'category'));
     }

     public function create($category_id)
     {
         $brand = new Brand;
         return view('admin.brand.create', compact('brand', 'category_id'));
     }

     public function store(Request $request, $category_id)
     {
          $data = $request->validate([
             'name' => 'required|string|max:255',
          ]);

          $data['status'] = request('status') == 1 ? 1 : 0 ;
          $data['category_id'] = $category_id ;

          $categoty = Brand::create($data);

          return redirect()->route('brand.index', ['category_id' => $category_id])->with('success', 'Categoria registrada');

     }

     public function edit( $category_id,  Brand $brand)
     {
          return view('admin.brand.edit', compact('brand', 'category_id'));
     }

     public function update( $category_id, Request $request, Brand $brand )
     {
          $data = $request->validate([
              'name' => 'required|string|max:255',
          ]);

          $data['status'] = request('status') == 1 ? 1 : 0 ;
          $data['category_id'] = $category_id ;

          $brand->update( $data );
          return redirect()->route('brand.index', ['category_id' => $category_id])->with('success', 'Categoria actualizada');
     }

}
