<?php

namespace App\Http\Controllers\Admin;

use App\Estado;
use App\Municipio;
use App\Localidad;
use App\User;
use App\Category;
use App\Search;
use Auth;


use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;


class SearchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $searchs = Search::all();
         foreach ($searchs as $key => $search) {
           $searchs[$key]->created_by = User::where('id', $search->user_id )->first();
         }
         return view('admin.search.index',  compact('searchs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         $estados = Estado::all();
         $edadmin = 10;
         $edadmax = 80;

         $min = User::role('Candidato')->orderBy('date_of_birth', 'DESC')->first();
         if ($min) {
            $edadmin = Carbon::now()->diffInYears($min['date_of_birth']);
         }

         $max = User::role('Candidato')->whereNotNull('date_of_birth')->orderBy('date_of_birth', 'ASC')->first();
         if ($max) {
            $edadmax = Carbon::now()->diffInYears($max['date_of_birth']);
         }

         $categorys = Category::whereNull('category_id')->get();

         foreach ($categorys as $key => $category) {
            $categorys[$key]->category = Category::where('category_id', $category->id )->get();
         }

         return view('admin.search.create',  compact('estados', 'edadmin', 'edadmax', 'categorys'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => 'required|string|max:255',
       ]);



        $data['name'] = request('name');
        $data['range'] = request('range1').';'.request('range2');
        $data['housewife'] = request('housewife');
        $data['out_of_home'] = request('out_of_home');
        $data['estado_id'] = request('estado_id') ? json_encode(request('estado_id')) : json_encode(array());
        $data['municipio_id'] = request('municipio_id') ? json_encode(request('municipio_id')) : json_encode(array());
        $data['localidad_id'] = request('localidad_id') ? json_encode(request('localidad_id')) : json_encode(array());
        $data['cp'] = request('cp');
        $data['last_participation'] = request('last_participation');
        $data['cat32_new'] = request('cat32_new') ? json_encode(request('cat32_new')) : json_encode(array());
        $data['cat33_new'] = request('cat33_new') ? json_encode(request('cat33_new')) : json_encode(array());
        $data['cat34_new'] = request('cat34_new') ? json_encode(request('cat34_new')) : json_encode(array());
        $data['cat32_old'] = request('cat32_old') ? json_encode(request('cat32_old')) : json_encode(array());
        $data['cat33_old'] = request('cat33_old') ? json_encode(request('cat33_old')) : json_encode(array());
        $data['cat34_old'] = request('cat34_old') ? json_encode(request('cat34_old')) : json_encode(array());
        $data['categorys_id'] = request('cateogira_id') ? json_encode(request('cateogira_id')) : json_encode(array());
        $user = auth()->user();


        $data['user_id'] = $user->id;


        Search::create($data);
        return redirect()->route('admin.search.index')->with('success', 'Busqueda registrada');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Search $search)
    {
         $estados = Estado::all();
         $edadmin = 10;
         $edadmax = 80;

         $min = User::role('Candidato')->orderBy('date_of_birth', 'DESC')->first();
         if ($min) {
            $edadmin = Carbon::now()->diffInYears($min['date_of_birth']);
         }

         $max = User::role('Candidato')->whereNotNull('date_of_birth')->orderBy('date_of_birth', 'ASC')->first();
         if ($max) {
            $edadmax = Carbon::now()->diffInYears($max['date_of_birth']);
         }

         $categorys = Category::whereNull('category_id')->get();


         foreach ($categorys as $key => $category) {
            $categorys[$key]->category = Category::where('category_id', $category->id )->get();
         }

         $municiopios = array();
         if ($search->estado_id) {
              $municiopios = Municipio::whereIn('estado_id', json_decode($search->estado_id))->get();
         }



         $localidades = array();
         if ($search->municipio_id) {
              $localidades =  Localidad::whereIn('municipio_id', json_decode($search->municipio_id))->get();
         }

          return view('admin.search.edit',  compact('estados', 'municiopios', 'localidades', 'edadmin', 'edadmax', 'categorys', 'search'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Search $search)
    {


         $data['range'] = request('range');
         $data['housewife'] = request('housewife');
         $data['out_of_home'] = request('out_of_home');
         $data['estado_id'] = request('estado_id') ? json_encode(request('estado_id')) : json_encode(array());
         $data['municipio_id'] = request('municipio_id') ? json_encode(request('municipio_id')) : json_encode(array());
         $data['localidad_id'] = request('localidad_id') ? json_encode(request('localidad_id')) : json_encode(array());
         $data['cp'] = request('cp');
         $data['last_participation'] = request('last_participation');
         $data['cat32_new'] = request('cat32_new') ? json_encode(request('cat32_new')) : json_encode(array());
         $data['cat33_new'] = request('cat33_new') ? json_encode(request('cat33_new')) : json_encode(array());
         $data['cat34_new'] = request('cat34_new') ? json_encode(request('cat34_new')) : json_encode(array());
         $data['cat32_old'] = request('cat32_old') ? json_encode(request('cat32_old')) : json_encode(array());
         $data['cat33_old'] = request('cat33_old') ? json_encode(request('cat33_old')) : json_encode(array());
         $data['cat34_old'] = request('cat34_old') ? json_encode(request('cat34_old')) : json_encode(array());

         // dump($data);
         // die;
         $search->update( $data );

          return redirect()->route('admin.search.index')->with('success', 'Busqueda registrada');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Search $search)
    {
         $search->delete();
         return back()->with('success', 'Cantidat@ eliminado');
    }
}
