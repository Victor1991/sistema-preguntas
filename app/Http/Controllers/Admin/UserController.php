<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateUserRequest;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Roles;
use App\Events\UserWasCreated;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
     {
          $users = User::role('Administrador')->get();
          return view('admin.users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         $user = new User;
         return view('admin.users.create', compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $data = $request->validate([
             'name' => 'required|string|max:255',
             'email' => 'required|string|email|max:255|unique:users'
        ]);

        $data['status'] = request('status') == 1 ? 1 : 0 ;
        $data['last_name_paternal'] = request('last_name_paternal');
        $data['last_name_maternal'] = request('last_name_maternal');
        $data['password'] = str_random(8);

        $user = User::create($data);

        $user->assignRole('Administrador');

        if ($request->filled('permissions')) {
             $user->givePermissionTo($request->permissions);
        }

        UserWasCreated::dispatch($user, $data['password']);

        return redirect()->route('admin.user.index')->with('success', 'Administrador registrado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return view('admin.users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
         return view('admin.users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
         $data = $request->validate([
             'name' => 'required|string|max:255',
             'email' => 'required|string|email|max:255|unique:users,email,'.$user->id,
        ]);

        if($request->filled('password')) {
           $data =  $request->validate([
                  'password' => 'confirmed', 'min:7',
             ]);
        }

        $data['last_name_paternal'] = request('last_name_paternal');
        $data['last_name_maternal'] = request('last_name_maternal');
        $data['status'] = request('status') == 1 ? 1 : 0 ;

        $user->update( $data );

        return redirect()->route('admin.user.index')->with('success', 'Administrador actualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
          $user->delete();
          return back()->with('success', 'Administrador eliminado');

    }
}
