<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateUserRequest;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Roles;
use App\Events\UserWasCreated;

class LeaderController extends Controller
{
     /**
      * Display a listing of the resource.
      *
      * @return \Illuminate\Http\Response
      */
     public function index()
      {
           $users = User::role('Jefe_capturista')->get();
           return view('admin.leader.index', compact('users'));
     }

     /**
      * Show the form for creating a new resource.
      *
      * @return \Illuminate\Http\Response
      */
     public function create()
     {
          $permissions = Permission::pluck('name', 'id');
          $leader = new User;
          return view('admin.leader.create', compact('leader', 'permissions'));
     }

     /**
      * Store a newly created resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @return \Illuminate\Http\Response
      */
     public function store(Request $request)
     {
          $data = $request->validate([
               'name' => 'required|string|max:255',
              'email' => 'required|string|email|max:255|unique:users'
          ]);

          $data['password'] = str_random(8);
          $data['status'] = request('status') == 1 ? 1 : 0 ;

          $user = User::create($data);

          $user->assignRole('Jefe_capturista');

          if ($request->filled('permissions')) {
               $user->givePermissionTo($request->permissions);
          }

          UserWasCreated::dispatch($user, $data['password']);

          return redirect()->route('admin.leader.index')->with('success', 'Jefe capturista registrado');

     }

     /**
      * Display the specified resource.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function show(User $leader)
     {
         return view('admin.leader.show', compact('leader'));
     }

     /**
      * Show the form for editing the specified resource.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function edit(User $leader)
     {
          $permissions = Permission::pluck('name', 'id');
          return view('admin.leader.edit', compact('leader', 'permissions'));
     }

     /**
      * Update the specified resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function update(UpdateUserRequest $request, User $leader)
     {

          $data = $request->validated();
          $data['status'] = request('status') == 1 ? 1 : 0 ;

          $leader->update( $data );

          return back()->with('success', 'Jefe capturista actualizado');
     }

     /**
      * Remove the specified resource from storage.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function destroy(User $leader)
     {
          $leader->delete();
         return back()->with('success', 'Jefe capturista eliminado');
     }
}
