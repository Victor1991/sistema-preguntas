<?php

namespace App\Http\Controllers\Admin;

use App\Municipio;
use App\Localidad;
use Response;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DireccionesController extends Controller
{
     public function municipios(Request $request)
    {
          $estado_id = $request->get('estado_id');
          if (!is_array($estado_id)) {
               $data = Municipio::where('estado_id', $estado_id)->get();
          }else {
               $data = Municipio::whereIn('estado_id', $estado_id)->get();
          }

          return Response::json($data);


    }

    public function localidades(Request $request)
    {
         $municipio_id = $request->get('municipio_id');

         $estado_id = $request->get('estado_id');
         if (!is_array($estado_id)) {
              $data = Localidad::where('municipio_id', $municipio_id)->orderBy('nombre','ASC')->get();
         }else{
              $data = Localidad::whereIn('municipio_id', $municipio_id)->orderBy('nombre','ASC')->get();
         }
        return Response::json($data);
    }


   public function buscador()
   {
        echo "1";
   }
}
