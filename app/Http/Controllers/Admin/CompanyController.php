<?php

namespace App\Http\Controllers\Admin;

use App\Company;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $companys = Company::where('removed', 0)->get();
         return view('admin.company.index', compact('companys'));


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         $company = new Company;
         return view('admin.company.create', compact('company'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

         $data = $request->validate([
             'name' => 'required|string|max:255'
        ]);

        $data['name'] = request('name');
        $data['direction'] = request('direction');

        $company = Company::create($data);

        return redirect()->route('admin.company.index')->with('success', 'Cliente registrado');


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company)
    {
         return view('admin.company.edit', compact('company'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Company $company)
    {
         $data = $request->validate([
             'name' => 'required|string|max:255'
        ]);

        $data['name'] = request('name');
        $data['direction'] = request('direction');

       $company->update( $data );

        return redirect()->route('admin.company.index')->with('success', 'Cliente editado');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company)
    {
         $data['removed'] = 1;
         $company->update( $data );
         return back()->with('success', 'Cliente eliminado');
    }
}
