<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AdminController extends Controller{

     public function index(){
          $admin = User::role('Administrador')->count();
          $leader = User::role('Jefe_capturista')->count();
          $capturist = User::role('Capturista')->count();
          $candidate = User::role('Candidato')->count();
          return view('admin.dashboard', compact('admin', 'leader', 'capturist', 'candidate'));
     }

}
