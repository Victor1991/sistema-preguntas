<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;


class CategoryController extends Controller
{
    public function index(){
         $categorys = Category::whereNull('category_id')->orderBy('order', 'ASC')->get();

         foreach ($categorys as $key => $category) {
              $categorys[$key]->category = Category::where('category_id', $category->id )->get();
         }

         return view('admin.category.index', compact('categorys'));
    }

    public function create($category_id)
    {
        $category = new Category;
        return view('admin.category.create', compact('category', 'category_id'));
    }

    public function store(Request $request, $category_id)
    {
         $data = $request->validate([
            'name' => 'required|string|max:255',
         ]);

         $data['status'] = request('status') == 1 ? 1 : 0 ;
         $data['category_id'] = $category_id ;

         $categoty = Category::create($data);

         return redirect()->route('admin.category.index')->with('success', 'Categoria registrada');

    }

    public function edit( $category_id,  Category $category)
    {
         return view('admin.category.edit', compact('category', 'category_id'));
    }

    public function update( $category_id, Request $request, Category $category )
    {
         $data = $request->validate([
             'name' => 'required|string|max:255',
         ]);

         $data['status'] = request('status') == 1 ? 1 : 0 ;
         $data['category_id'] = $category_id ;

         $category->update( $data );

         return redirect()->route('admin.category.index')->with('success', 'Categoria actualizada');
    }


}
