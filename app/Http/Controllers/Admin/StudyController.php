<?php

namespace App\Http\Controllers\Admin;

use App\Study;
use App\Company;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StudyController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index()
     {
          $studys = Study::where('removed', 0)->get();
          return view('admin.study.index', compact('studys'));

     }

     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function create()
     {
          $study = new Study;
          $companys = Company::where('removed', 0)->get();
          return view('admin.study.create', compact('study','companys'));
     }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
     {
          $data = $request->validate([
               'name' => 'required|string|max:255'
          ]);

          $data['name'] = request('name');
          $data['company_id'] = request('company_id');
          $data['study_date'] = request('study_date');
          $data['number_participants'] = request('number_participants');

          $study = Study::create($data);

          return redirect()->route('admin.study.index')->with('success', 'Estudio registrado');

     }

     /**
     * Display the specified resource.
     *
     * @param  \App\Study  $study
     * @return \Illuminate\Http\Response
     */
     public function show(Study $study)
     {

     }

     /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Study  $study
     * @return \Illuminate\Http\Response
     */
     public function edit(Study $study)
     {
          $companys = Company::where('removed', 0)->get();
          return view('admin.study.edit', compact('study', 'companys'));

     }

     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Study  $study
     * @return \Illuminate\Http\Response
     */
     public function update(Request $request, Study $study)
     {
          $data = $request->validate([
               'name' => 'required|string|max:255'
          ]);

          $data['name'] = request('name');
          $data['company_id'] = request('company_id');
          $data['study_date'] = request('study_date');
          $data['number_participants'] = request('number_participants');

          $study->update( $data );

          return redirect()->route('admin.study.index')->with('success', 'Estudio editado');
     }

     /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Study  $study
     * @return \Illuminate\Http\Response
     */
     public function destroy(Study $study)
     {
          $data['removed'] = 1;
          $study->update( $data );
          return back()->with('success', 'Estudio eliminado');
     }
}
