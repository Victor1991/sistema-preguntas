<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;


class LoginController extends Controller
{
     /*
     |--------------------------------------------------------------------------
     | Login Controller
     |--------------------------------------------------------------------------
     |
     | This controller handles authenticating users for the application and
     | redirecting them to your home screen. The controller uses a trait
     | to conveniently provide its functionality to your applications.
     |
     */

     use AuthenticatesUsers;

     /**
     * Where to redirect users after login.
     *
     * @var string
     */
     protected $redirectTo = '/admin';

     /**
     * Create a new controller instance.
     *
     * @return void
     */
     public function __construct()
     {
          $this->middleware('guest')->except('logout');
     }


     protected function authenticated(Request $request, $user)
     {
          if( $user->status == 0 ){
               Auth::logout();
               return redirect()->route('login')->with('warning', 'El usuario esta desactivado');
          }
          // }else{
          //      $es_capturista = $user->hasRole('Capturista');
          //      if ($es_capturista) {
          //           if($user->ip != request('ip')){
          //                Auth::logout();
          //                return redirect()->route('login')->with('warning', 'El usuario no puede acceder en esta red');
          //           }
          //      }
          // }
     }

}
