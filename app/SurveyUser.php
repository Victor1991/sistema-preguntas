<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SurveyUser extends Model
{
     protected $fillable = [
          'survey_id', 'user_id'
     ];

     public function user()
     {
          return $this->hasOne(User::class, 'id', 'user_id');
     }
}
