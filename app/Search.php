<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Search extends Model
{
     protected $table = 'searchs';

     protected $guarded = array();

     public function created_by()
     {
          // return $this->hasOne( User::class, 'user_id', 'id' );
          return $this->hasOne( User::class, 'id', 'user_id' );

     }
}
