<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AnswerUser extends Model
{
     protected $fillable = [
          'question_id', 'survey_id', 'user_id', 'answer_id', 'survey_users_id', 'open_answer'
     ];

     public function answer()
     {
          return $this->hasOne(Answer::class, 'id', 'answer_id')->orderBy('id', 'desc') ;
     }

     
}
