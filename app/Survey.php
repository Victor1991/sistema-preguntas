<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Survey extends Model
{
     protected $fillable = [
         'study_id', 'title', 'description', 'finish'
    ];

    public function study()
    {
         return $this->hasOne( Study::class, 'id', 'study_id');
    }

    public function question()
    {
         return $this->hasMany( Question::class)->orderBy('id', 'asc') ;;
    }

}
