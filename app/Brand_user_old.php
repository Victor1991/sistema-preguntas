<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand_user_old extends Model
{
     protected $fillable = [
        'brand_id', 'user_id', 'user_modif'
    ];

    public $timestamps = true;

    public function brand()
    {
         return $this->hasOne(Brand::class, 'id', 'brand_id');
    }
}
