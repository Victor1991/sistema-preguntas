<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudyUser extends Model
{
     protected $table = 'study_user';

     public $timestamps = true;

     protected $fillable = [
         'user_id', 'study_id', 'assigned_by'
    ];

}
