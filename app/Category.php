<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
     protected $fillable = [
         'name', 'status', 'category_id'
     ];

     public function brands_all()
     {
          return $this->hasMany(Brand::class);
     }

     public function brands()
     {
          return $this->hasMany(Brand::class)->where('status', '1');
     }

     public function category()
     {
          return $this->hasMany( Category::class );
     }

}
