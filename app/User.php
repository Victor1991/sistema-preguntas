<?php

namespace App;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'status', 'last_name_paternal', 'last_name_maternal',
        'date_of_birth', 'phone', 'cell_phone', 'ine_number', 'housewife', 'out_of_home',
        'facebook', 'address', 'commentary', 'photo', 'frecuencia_participacion', 'photo_personal',
        'photo_limpieza','photo_lavado' ,'estado_id','municipio_id','localidad_id','calle',
        'codigo_postal', 'complete_information', 'seen_by_boss', 'user_id', 'boss_message'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function setPasswordAttribute($password)
    {
         $this->attributes['password'] = bcrypt($password);
    }

    public function brands()
    {
         return $this->belongsToMany(Brand::class)->orderBy('brand_id')->withTimestamps();
    }

    public function brands_old()
    {
         return $this->hasMany(Brand_user_old::class)->orderBy('brand_id');
    }


    public function studys()
    {
         return $this->belongsToMany(Study::class)->withPivot('id');;
    }

     public function get_estado()
     {
          return $this->hasOne(Estado::class, 'id', 'estado_id');
     }

     public function get_municipio()
     {
          return $this->hasOne(Municipio::class, 'id', 'municipio_id');
     }

     public function get_localidad()
     {
          return $this->hasOne(Localidad::class, 'id', 'localidad_id');
     }

     public function surveys()
     {
          return $this->belongsToMany(Survey::class, 'survey_users');
     }

     public function answer()
     {
          return $this->belongsToMany(Answer::class, 'answer_users');
     }
}
