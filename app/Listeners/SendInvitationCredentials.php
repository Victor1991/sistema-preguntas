<?php

namespace App\Listeners;

use App\Events\UserWasActivated;
use App\Mail\ActivationCapturist;

use Illuminate\Support\Facades\Mail;


use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendInvitationCredentials
{

     /**
     * Handle the event.
     *
     * @param  UserWasCreated  $event
     * @return void
     */
     public function handle(UserWasActivated $event)
     {
          Mail::to($event->user)->queue(
               new ActivationCapturist($event->user, $event->caducidad)
          );
     }

}
