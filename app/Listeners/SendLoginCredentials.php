<?php

namespace App\Listeners;

use App\Events\UserWasCreated;
use App\Mail\LoginCredencials;

use Illuminate\Support\Facades\Mail;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendLoginCredentials
{

     /**
     * Handle the event.
     *
     * @param  UserWasCreated  $event
     * @return void
     */
     public function handle(UserWasCreated $event)
     {
          Mail::to($event->user)->queue(
               new LoginCredencials($event->user, $event->password)
          );
     }
}
