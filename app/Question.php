<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
     protected $fillable = [
          'survey_id', 'questions','catalog_question_id'
     ];

     public function answers()
     {
          return $this->hasMany( Answer::class)->orderBy('id', 'asc') ;
     }


     public function open_answer($question_id, $user_id)
     {
          return $this->hasOne( AnswerUser::class )->where('question_id', $question_id)->where('user_id', $user_id)->first();
     }
}
