<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Study extends Model
{
     public $timestamps = true;

     protected $fillable = [
          'name', 'company_id', 'study_date', 'removed', 'number_participants'
     ];

     public function company()
     {
          return $this->belongsTo( Company::class );
     }
}
